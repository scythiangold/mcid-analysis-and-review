Attribute VB_Name = "WeightedDLModified"
Option Explicit

Public Function WeightedDL(ByVal Source As String, ByVal Target As String, ByVal DeleteCost As Double, ByVal InsertCost As Double, ByVal ReplaceCost As Double, ByVal SwapCost As Double) As Double

    Dim DeleteDistance As Double
    Dim InsertDistance As Double
    Dim MatchDistance As Double
    Dim SwapDistance As Double
    Dim PreSwapCost As Double
    Dim UnspecifiedReplaceCost As Double
    Dim ReplaceDistance(1 To 26, 1 To 26) As Double
    Dim Table() As Double
    Dim Counter As Integer
    Dim CounterAgain As Integer
    Dim CounterNext As Integer
    Dim CounterSwap As Integer
    Dim CounterAgainSwap As Integer
    Dim CandidateSwapIndex As Integer
    Dim MaxSourceLetterMatchIndex As Integer
    Dim SourceIndexByCharacter() As Variant
    
    UnspecifiedReplaceCost = ReplaceCost
    
    For Counter = 1 To 26
        For CounterAgain = 1 To 26
            ReplaceDistance(Counter, CounterAgain) = 0
        Next
    Next
    
    ReplaceDistance(2, 22) = 0.5
    ReplaceDistance(22, 2) = 0.5
    ReplaceDistance(4, 15) = 0.5
    ReplaceDistance(15, 4) = 0.5
    ReplaceDistance(8, 12) = 0.5
    ReplaceDistance(12, 8) = 0.5
    ReplaceDistance(8, 14) = 0.5
    ReplaceDistance(14, 8) = 0.5
    ReplaceDistance(9, 12) = 0.5
    ReplaceDistance(12, 9) = 0.5
    ReplaceDistance(13, 14) = 0.5
    ReplaceDistance(14, 13) = 0.5
    ReplaceDistance(14, 18) = 0.5
    ReplaceDistance(18, 14) = 0.5
    ReplaceDistance(20, 25) = 0.5
    ReplaceDistance(25, 20) = 0.5
    ReplaceDistance(21, 22) = 0.5
    ReplaceDistance(22, 21) = 0.5
    ReplaceDistance(22, 23) = 0.5
    ReplaceDistance(23, 22) = 0.5

    If Len(Source) = 0 _
    Then
        WeightedDL = Len(Target) * InsertCost
        Exit Function
    End If

    If Len(Target) = 0 _
    Then
        WeightedDL = Len(Source) * DeleteCost
        Exit Function
    End If

    ReDim Table(Len(Source), Len(Target))
    ReDim SourceIndexByCharacter(0 To 1, 0 To Len(Source) - 1)

    If Left(Source, 1) <> Left(Target, 1) _
    Then
        ReplaceCost = CheckCustomReplaceCost(Left(Source, 1), Left(Target, 1), UnspecifiedReplaceCost, ReplaceDistance)
        Table(0, 0) = MinOf(ReplaceCost, (DeleteCost + InsertCost))
    End If

    SourceIndexByCharacter(0, 0) = Left(Source, 1)
    SourceIndexByCharacter(1, 0) = 0

    For Counter = 1 To Len(Source) - 1

        DeleteDistance = Table(Counter - 1, 0) + DeleteCost
        InsertDistance = ((Counter + 1) * DeleteCost) + InsertCost

        If Mid(Source, Counter + 1, 1) = Left(Target, 1) _
        Then
            MatchDistance = (Counter * DeleteCost) + 0
        Else
            ReplaceCost = CheckCustomReplaceCost(Mid(Source, Counter + 1, 1), Left(Target, 1), UnspecifiedReplaceCost, ReplaceDistance)
            MatchDistance = (Counter * DeleteCost) + ReplaceCost
        End If

        Table(Counter, 0) = MinOf(MinOf(DeleteDistance, InsertDistance), MatchDistance)
    Next

    For CounterAgain = 1 To Len(Target) - 1

        DeleteDistance = Table(0, CounterAgain - 1) + InsertCost
        InsertDistance = ((CounterAgain + 1) * InsertCost) + DeleteCost

        If Left(Source, 1) = Mid(Target, CounterAgain + 1, 1) _
        Then
            MatchDistance = (CounterAgain * InsertCost) + 0
        Else
            ReplaceCost = CheckCustomReplaceCost(Left(Source, 1), Mid(Target, CounterAgain + 1, 1), UnspecifiedReplaceCost, ReplaceDistance)
            MatchDistance = (CounterAgain * InsertCost) + ReplaceCost
        End If

        Table(0, CounterAgain) = MinOf(MinOf(DeleteDistance, InsertDistance), MatchDistance)
    Next

    For Counter = 1 To Len(Source) - 1

        If Mid(Source, Counter + 1, 1) = Left(Target, 1) _
        Then
            MaxSourceLetterMatchIndex = 0
        Else
            MaxSourceLetterMatchIndex = -1
        End If

        For CounterAgain = 1 To Len(Target) - 1
            CandidateSwapIndex = -1

            For CounterNext = 0 To UBound(SourceIndexByCharacter, 2)
                If SourceIndexByCharacter(0, CounterNext) = Mid(Target, CounterAgain + 1, 1) _
                Then CandidateSwapIndex = SourceIndexByCharacter(1, CounterNext)
            Next

            CounterAgainSwap = MaxSourceLetterMatchIndex
            DeleteDistance = Table(Counter - 1, CounterAgain) + DeleteCost
            InsertDistance = Table(Counter, CounterAgain - 1) + InsertCost
            MatchDistance = Table(Counter - 1, CounterAgain - 1)

            If Mid(Source, Counter + 1, 1) <> Mid(Target, CounterAgain + 1, 1) _
            Then
                ReplaceCost = CheckCustomReplaceCost(Mid(Source, Counter + 1, 1), Mid(Target, CounterAgain + 1, 1), UnspecifiedReplaceCost, ReplaceDistance)
                MatchDistance = MatchDistance + ReplaceCost
            Else
                MaxSourceLetterMatchIndex = CounterAgain
            End If

            If CandidateSwapIndex <> -1 And CounterAgainSwap <> -1 _
            Then
                CounterSwap = CandidateSwapIndex

                If CounterSwap = 0 And CounterAgainSwap = 0 _
                Then
                    PreSwapCost = 0
                Else
                    PreSwapCost = Table(MaxOf(0, CounterSwap - 1), MaxOf(0, CounterAgainSwap - 1))
                End If

                SwapDistance = PreSwapCost + ((Counter - CounterSwap - 1) * DeleteCost) + ((CounterAgain - CounterAgainSwap - 1) * InsertCost) + SwapCost

            Else
                SwapDistance = 500
            End If

            Table(Counter, CounterAgain) = MinOf(MinOf(MinOf(DeleteDistance, InsertDistance), MatchDistance), SwapDistance)
'            Table(Counter, CounterAgain) = MaxOf(MaxOf(MaxOf(DeleteDistance, InsertDistance), MatchDistance), SwapDistance)

        Next

        SourceIndexByCharacter(0, Counter) = Mid(Source, Counter + 1, 1)
        SourceIndexByCharacter(1, Counter) = Counter
    Next

    WeightedDL = Table(Len(Source) - 1, Len(Target) - 1)

End Function
 
Public Function MinOf(ByVal Value1 As Double, ByVal Value2 As Double) As Double
 
    If Value1 > Value2 Then
        MinOf = Value2
    Else
        MinOf = Value1
    End If
 
End Function
 
Public Function MaxOf(ByVal Value1 As Double, ByVal Value2 As Double) As Double
 
    If Value1 < Value2 Then
        MaxOf = Value2
    Else
        MaxOf = Value1
    End If
 
End Function

Function ConvertLetterToNumber(ByVal Source As String) As String

    Dim Counter As Integer
    Dim Result As String
    
    For Counter = 1 To Len(Source)
        Select Case Asc(Mid(Source, Counter, 1))
            Case 65 To 90:
                Result = Result & Asc(Mid(Source, Counter, 1)) - 64
            Case Else
                Result = Result & Mid(Source, Counter, 1)
        End Select
    Next
    ConvertLetterToNumber = Result

End Function

Function IsLetter(strValue As String) As Boolean

    Dim intPos As Integer
    For intPos = 1 To Len(strValue)
        Select Case Asc(Mid(strValue, intPos, 1))
            Case 65 To 90, 97 To 122
                IsLetter = True
            Case Else
                IsLetter = False
                Exit For
        End Select
    Next
    
End Function

Function CheckCustomReplaceCost(SourceChar As String, TargetChar As String, UnspecifiedReplaceCost As Double, ReplaceDistance() As Double) As Double

    Dim ReplaceCost As Double

    If IsLetter(SourceChar) = True And IsLetter(TargetChar) = True _
    Then
        If ReplaceDistance(ConvertLetterToNumber(SourceChar), ConvertLetterToNumber(TargetChar)) <> 0 _
        Then
            ReplaceCost = ReplaceDistance(ConvertLetterToNumber(SourceChar), ConvertLetterToNumber(TargetChar))
        Else
            ReplaceCost = UnspecifiedReplaceCost
        End If
    Else
        ReplaceCost = UnspecifiedReplaceCost
    End If
    CheckCustomReplaceCost = ReplaceCost
    
End Function
