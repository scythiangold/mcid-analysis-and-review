Attribute VB_Name = "Miscellaneous"
Option Explicit
Option Private Module

Function AlphaNumericOnly(ByVal strSource As String) As Variant

    Dim i As Integer
    Dim strResult As String
    
    For i = 1 To Len(strSource)
        Select Case Asc(Mid(strSource, i, 1))
            Case 32, 48 To 57, 65 To 90, 97 To 122:
                strResult = strResult & Mid(strSource, i, 1)
        End Select
    Next
    AlphaNumericOnly = CVar(strResult)
    
End Function

Function AlphaNumericOnlySpecial(ByVal strSource As String) As Variant

    Dim i As Integer
    Dim strResult As String
    
    For i = 1 To Len(strSource)
        Select Case Asc(Mid(strSource, i, 1))
            Case 32, 39, 48 To 57, 65 To 90, 97 To 122:
                strResult = strResult & Mid(strSource, i, 1)
        End Select
    Next
    AlphaNumericOnlySpecial = CVar(strResult)
    
End Function

Function AlphabeticOnly(ByVal strSource As String) As Variant

    Dim i As Integer
    Dim strResult As String
    
    For i = 1 To Len(strSource)
        Select Case Asc(Mid(strSource, i, 1))
            Case 32, 65 To 90, 97 To 122:
                strResult = strResult & Mid(strSource, i, 1)
        End Select
    Next
    AlphabeticOnly = CVar(strResult)
    
End Function

Function FPPOOnly(ByVal strSource As String) As Variant

    Dim i As Integer
    Dim strResult As String
    
    For i = 1 To Len(strSource)
        Select Case Asc(Mid(strSource, i, 1))
            Case 43, 70, 79, 80:
                strResult = strResult & Mid(strSource, i, 1)
        End Select
    Next
    FPPOOnly = CVar(strResult)
    
End Function

Sub clearDebugConsole()

    Dim Counter As Integer
    
    For Counter = 0 To 100
        Debug.Print vbNullString
    Next
    
End Sub

Public Function IsBlank(ByRef rngToCheck As Range) As Boolean

    Dim Cell As Range

    IsBlank = True
    For Each Cell In rngToCheck
        If Cell.Value2 <> vbNullString _
        Then
            IsBlank = False
            Exit Function
        End If
    Next Cell
    
End Function

Function IsInArrayExactCount(Array1 As Variant, Array2 As Variant, Array3 As Variant, Array4 As Variant) As Boolean

    Dim ArrayCounter As Integer, ArrayCounterAgain As Integer
    
    IsInArrayExactCount = False
    If IsEmpty(UBound(Array1)) = True _
    Or IsEmpty(UBound(Array2)) = True _
    Or UBound(Array1) <> UBound(Array3) _
    Or UBound(Array2) <> UBound(Array4) _
    Then Exit Function
    For ArrayCounter = LBound(Array1) To UBound(Array1)
        For ArrayCounterAgain = LBound(Array2) To UBound(Array2)
            If Array1(ArrayCounter) = Array2(ArrayCounterAgain) _
            Then
                If Array3(ArrayCounter) = "I" _
                Or Array4(ArrayCounterAgain) = "I" _
                Then
                    IsInArrayExactCount = True
                    Exit Function
                End If
            End If
        Next
    Next
    
End Function

Function IsInArrayCompleteSpecialCombined(Array1 As Variant, Array2 As Variant, Array3 As Variant) As Boolean

    Dim ArrayCounter As Integer, ArrayCounterAgain As Integer, ArrayCounterNext As Integer
    Dim FNsMIsCombined() As String
    Dim FirstNamesMCID() As String
    Dim MidInitialsMCID() As String
    
    ReDim FNsMIsCombined(0 To 0)
    ReDim FirstNamesMCID(0 To 0)
    ReDim MidInitialsMCID(0 To 0)
    ReDim FNsMIsCombinedMCID(0 To 0)
    
    IsInArrayCompleteSpecialCombined = False
    For ArrayCounter = LBound(Array1) To UBound(Array1)
        ReDim Preserve FNsMIsCombined(LBound(FNsMIsCombined) To UBound(FNsMIsCombined) + 1)
        FNsMIsCombined(UBound(FNsMIsCombined)) = Array1(ArrayCounter)
    Next
    For ArrayCounter = LBound(Array2) To UBound(Array2)
        ReDim Preserve FNsMIsCombined(LBound(FNsMIsCombined) To UBound(FNsMIsCombined) + 1)
        FNsMIsCombined(UBound(FNsMIsCombined)) = Array2(ArrayCounter)
    Next
    For ArrayCounterNext = LBound(Array3, 2) To UBound(Array3, 2)
        If InStr(Array3(1, ArrayCounterNext), " ") <> 0 _
        Then
            FirstNamesMCID = Split(Array3(0, ArrayCounterNext))
        Else
            ReDim FirstNamesMCID(0 To 0)
            FirstNamesMCID(0) = Array3(0, ArrayCounterNext)
        End If
        If InStr(Array3(3, ArrayCounterNext), " ") <> 0 _
        Then
            MidInitialsMCID = Split(Array3(2, ArrayCounterNext))
        Else
            ReDim MidInitialsMCID(0 To 0)
            MidInitialsMCID(0) = Array3(2, ArrayCounterNext)
        End If
        For ArrayCounter = LBound(FirstNamesMCID) To UBound(FirstNamesMCID)
            ReDim Preserve FNsMIsCombinedMCID(LBound(FNsMIsCombinedMCID) To UBound(FNsMIsCombinedMCID) + 1)
            FNsMIsCombinedMCID(UBound(FNsMIsCombinedMCID)) = FirstNamesMCID(ArrayCounter)
        Next
        For ArrayCounter = LBound(MidInitialsMCID) To UBound(MidInitialsMCID)
            ReDim Preserve FNsMIsCombinedMCID(LBound(FNsMIsCombinedMCID) To UBound(FNsMIsCombinedMCID) + 1)
            FNsMIsCombinedMCID(UBound(FNsMIsCombinedMCID)) = MidInitialsMCID(ArrayCounter)
        Next
        If IsInEachArray(FNsMIsCombined, FNsMIsCombinedMCID, "Left", , True) = True _
        Then
            IsInArrayCompleteSpecialCombined = True
            Exit Function
        End If
    Next
    
End Function

Function IsInArrayLengthSpecial(StringLength As Integer, Array1 As Variant, CurrentName As Variant) As Boolean

    Dim ArrayCounter As Integer
    
    IsInArrayLengthSpecial = False
    For ArrayCounter = LBound(Array1) To UBound(Array1)
        If Len(Array1(ArrayCounter)) >= StringLength _
        And Array1(ArrayCounter) <> CurrentName Then
            IsInArrayLengthSpecial = True
            Exit Function
        End If
    Next
    
End Function
