Attribute VB_Name = "MCIDAnalysisandReview"
Option Explicit

Public FNDiffers As Boolean, LNDiffers As Boolean, MIDiffers As Boolean, DOBDiffers As Boolean
Public FNDiffersMCID As Boolean, LNDiffersMCID As Boolean, MIDiffersMCID As Boolean, DOBDiffersMCID As Boolean
Public FNMatch As Boolean, LNMatch As Boolean, MIMatch As Boolean, DOBMatch As Boolean
Public AmbiguousFirst As Boolean, AmbiguousLast As Boolean, AmbiguousBirth As Boolean
Public AmbiguousFN As Boolean, AmbiguousLN As Boolean, AmbiguousSSN As Boolean, AmbiguousDOB As Boolean
Public AmbiguousFNMCID As Boolean, AmbiguousLNMCID As Boolean, AmbiguousDOBMCID As Boolean
Public HardAmbiguousFN As Boolean, HardAmbiguousLN As Boolean, AmbiguousExactFN As Boolean, AmbiguousExactLN As Boolean
Public AmbiguousFNs() As Boolean, AmbiguousLNs() As Boolean
Public AmbiguousExactFNs() As Boolean, AmbiguousExactLNs() As Boolean
Public MarriageRule As Boolean
Public BirthDate As Date
Public MostCommonDate As Date
Public AmbiguousDOBs() As Date
Public PersonKey As Double
Public PersonNumber As Integer, CurrentRow As Integer
Public Counter As Integer, PersonCounter As Integer
Public BirthDateMonth As String, BirthDateDay As String, BirthDateYear As String
Public ReviewCol As Integer, CommentsCol As Integer, FPSequence1Col As Integer, BackupFPSequence1Col As Integer
Public MatchPatternCol As Integer, FPSequence2Col As Integer
Public LastNameCounter As Integer, MidInitialCounter As Integer
Public TaskIDCol As Integer, MCIDCol As Integer
Public SourceKeyCol As Integer, NameStatusCol As Integer, GenderCol As Integer
Public FirstNameCol As Integer, LastNameCol As Integer, MidInitialCol As Integer, BirthDateCol As Integer
Public SubscriberIDCol As Integer, SSNCol As Integer, HCIDCol As Integer
Public IsFNAnalystCol As Integer, IsFNReviewerCol As Integer, MatchPatternReviewerCol As Integer
Public IsPOCol As Integer, MCIDPairFlagCol As Integer, MultipleMCIDFlagCol As Integer, FNSequenceCol As Integer
Public FirstName As Variant, LastName As Variant, MidInitial As Variant
Public FilePath As String
Public Gender As String, GenderMCID As String
Public BirthDateText As Variant, FirstNameExact As Variant, LastNameExact As Variant
Public SourceKey As Variant, MCID As Variant, NameStatus As Variant
Public SubscriberID As Variant, SSN As Variant, HCID As Variant
Public FirstNames() As Variant, LastNames() As Variant, MidInitials() As Variant
Public ExactNames() As Variant, ExactFirstNames() As Variant, ExactLastNames() As Variant, ExactMidInitials() As Variant
Public HardAmbiguousNames1() As Variant, HardAmbiguousNames2() As Variant
Public SoftAmbiguousNames1() As Variant, SoftAmbiguousNames2() As Variant
Public ShortNames() As Variant, DiminutiveShortNames() As Variant, ChineseFamilyNames() As Variant
Public AmbiguousExclusions() As Variant
Public People() As New Person
'Set People() = New Person
Public SubscriberIDs() As New PersonSpecial
'Set SubscriberIDs() = PersonSpecial
Public SubscriberIDsSpecial() As New PersonSpecial
'Set SubscriberIDsSpecial() = New PersonSpecial
Public SSNs() As New PersonSpecial
'Set SSNs() = PersonSpecial
Public SSNsSpecial() As New PersonSpecial
'Set SSNsSpecial() = New PersonSpecial
Public Hypocorisms As Scripting.Dictionary, PeopleKeys As Scripting.Dictionary
Public FalseNegativeReview As Boolean, FalseNegativeAnalysis As Boolean
Public TaskIsFP As Boolean, TaskIsPO As Boolean
Public ExactDates() As Variant
Public ExactNamesSpecial() As Variant
Public MCIDIsFP As Boolean, MCIDIsPO As Boolean
Public FNExactMatch As Boolean, LNExactMatch As Boolean, MIExactMatch As Boolean, DOBExactMatch As Boolean
Public SoftAmbiguousFN() As Boolean, SoftAmbiguousLN() As Boolean
Public TwinsRule As Boolean, RequiresManualCheck As Boolean, ReviewFPSequence As Boolean, EmailComments As Boolean
Public TaskStartRow As Integer, TaskEndRow As Integer, StartRow As Integer, EndRow As Integer
Public FNLNSwapped As Boolean, TwinsGenderDiffers As Boolean, FNNickname As Boolean, FNTypo As Boolean
Public LNTypo As Boolean, LNMarriage As Boolean, DOBTypo As Boolean, MIMissing As Boolean, SSNDiffers As Boolean
Public CurrentValidMCIDs() As Variant
Public ExpiredMCIDs() As Variant
Public ExpiredMCIDQuery As New StringBuilder
Public ExpiredMCID As Boolean
Public Review As String
Public TaskReview As String
Public Comments As String
Public FPReviewHighPersonNumber As Integer
Public AmbiguousDOBTask As Boolean
Public SSNMatch As Boolean
Public SSNMissing As Boolean
Public ExpiredMCIDCheck As Boolean
Public ExpiredMCIDCheckManualOverride As Boolean
Public ExpiredMCIDsCount As Integer

Sub MCIDAnalysisAndReview()

    Dim PersonFound As Boolean, ReviewFPSequence As Boolean
    Dim ResizeCols As Boolean
    Dim RowCounter As Integer, ColCounter As Integer, FinalRow As Integer, InsertCounter As Integer
    Dim StartTime As Long, SecondsElapsed As Long, MinutesElapsed As Long, RemSecondsElapsed As Long
    Const InitialRow = 2
    
    Dim LastRowAnalyst As Integer, LastRowReviewer As Integer
    Dim PrintCounter As Integer
    Dim MCIDs() As String
    Dim QueryText As String
    Dim ExpiredMCIDQuery As New StringBuilder
    Dim SQLConnection As New ADODB.Connection
    Dim SQLCommand As New ADODB.Command
    Dim SQLRecordset As New ADODB.Recordset

    Application.EnableEvents = True
    Application.EnableCancelKey = xlInterrupt
    Application.ScreenUpdating = False

    Set PeopleKeys = New Scripting.Dictionary
    Set Hypocorisms = New Scripting.Dictionary
    Hypocorisms.CompareMode = vbTextCompare
    
    StartTime = Timer
    FPSequence1Col = 0
    InsertCounter = 0
    ResizeCols = False
    EmailComments = False
    FalseNegativeAnalysis = False
    FalseNegativeReview = False
    ExpiredMCIDCheck = False
    ExpiredMCIDCheckManualOverride = True
    ExpiredMCIDsCount = 0
    
    For ColCounter = 1 To 78
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEW")) <> 0 _
        And Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEWER")) = 0 _
        Then ReviewCol = ColCounter
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "COMMENT")) <> 0 _
        Then CommentsCol = ColCounter
        If (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        And FPSequence1Col = 0 _
        Then
            FPSequence1Col = ColCounter
        ElseIf FPSequence1Col <> 0 _
        And (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        Then
            FPSequence2Col = ColCounter
        End If
        If (InStr(Cells(1, ColCounter).Value2, "MATCH") <> 0 And InStr(Cells(1, ColCounter).Value2, "PATTERN") <> 0 _
        And InStr(Cells(1, ColCounter).Value2, "REVIEWER") = 0) _
        Then MatchPatternCol = ColCounter
        If Cells(1, ColCounter).Value2 = "TASK_ID" Then TaskIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MCID" Then MCIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SRC_KEY" Or Cells(1, ColCounter).Value2 = "MEMIDNUM" _
        Then SourceKeyCol = ColCounter
        If Cells(1, ColCounter).Value2 = "NAME_STATUS" Or Cells(1, ColCounter).Value2 = "RECSTAT" Then NameStatusCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "FIRST") <> 0 And InStr(Cells(1, ColCounter).Value2, "DENSE") = 0 _
        Then FirstNameCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "LAST") <> 0 Then LastNameCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "MIDDLE") <> 0 Or InStr(Cells(1, ColCounter).Value2, "INITIAL") <> 0 _
        Then MidInitialCol = ColCounter
        If Cells(1, ColCounter).Value2 = "GENDER" Or Cells(1, ColCounter).Value2 = "SEX" Then GenderCol = ColCounter
        If Cells(1, ColCounter).Value2 = "DOB" Then BirthDateCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SUBSCRBID" Or Cells(1, ColCounter).Value2 = "SBSCRBR_ID" Or Cells(1, ColCounter).Value2 = "SUBSCRIBER_ID" _
        Then SubscriberIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SSN" Or Cells(1, ColCounter).Value2 = "TAXID" Then SSNCol = ColCounter
        If Cells(1, ColCounter).Value2 = "HCID" Then HCIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_FN_ANALYST" Or Cells(1, ColCounter).Value2 = "IS_FN" Then IsFNAnalystCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_PO" Then IsPOCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MCID_PAIR_FLAG" Then MCIDPairFlagCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MULTIPLE_MCID_FLAG" Then MultipleMCIDFlagCol = ColCounter
    Next
    
    LastRowAnalyst = Cells(Rows.Count, MCIDCol).End(xlUp).Row
    LastRowReviewer = LastRowAnalyst
'    LastRowReviewer = Cells(Rows.Count, 7).End(xlUp).Row
    
    If Cells(InitialRow - 1, 5).Value2 = "IS_FN_ANALYST" And IsBlank(Range(Cells(InitialRow, 5), Cells(LastRowAnalyst, 5))) = False _
    And Cells(InitialRow - 1, 7).Value2 = "IS_FN_REVIEWER" And IsBlank(Range(Cells(InitialRow, 7), Cells(LastRowReviewer, 7))) = False _
    Then
        FalseNegativeAnalysis = False
        FalseNegativeReview = False
    ElseIf Cells(InitialRow - 1, 5).Value2 = "IS_FN_ANALYST" And IsBlank(Range(Cells(InitialRow, 5), Cells(LastRowAnalyst, 5))) = True _
    And Cells(InitialRow - 1, 7).Value2 = "IS_FN_REVIEWER" And IsBlank(Range(Cells(InitialRow, 7), Cells(LastRowReviewer, 7))) = True _
    Then
        FalseNegativeAnalysis = True
        FalseNegativeReview = False
    ElseIf Cells(InitialRow - 1, 5).Value2 = "IS_FN_ANALYST" And IsBlank(Range(Cells(InitialRow, 5), Cells(LastRowAnalyst, 5))) = False _
    And Cells(InitialRow - 1, 7).Value2 = "IS_FN_REVIEWER" And IsBlank(Range(Cells(InitialRow, 7), Cells(LastRowReviewer, 7))) = True _
    Then
        FalseNegativeAnalysis = False
        FalseNegativeReview = True
    ElseIf Cells(InitialRow - 1, 8).Value2 = "IS_FN" _
    Then
        FalseNegativeAnalysis = True
        FalseNegativeReview = False
    Else
        FalseNegativeAnalysis = False
        FalseNegativeReview = False
    End If
    If FalseNegativeAnalysis = True Then ExpiredMCIDCheck = True
'    ResizeCols = Sheet1.CheckBox1.Value2
    
    For ColCounter = 1 To 78
        If FalseNegativeReview = True _
        Or FalseNegativeAnalysis = True _
        Then
            If FalseNegativeReview = True _
            Then
                If Cells(1, ColCounter).Value2 = "IS_FN_REVIEWER" Then IsFNReviewerCol = ColCounter
                If Cells(1, ColCounter).Value2 = "MATCHING_PATTERN_REVIEWER" Then MatchPatternReviewerCol = ColCounter
            End If
            If Cells(1, ColCounter).Value2 = "FN_SEQUENCE" Or Cells(1, ColCounter).Value2 = "FN SEQUENCE" Then FNSequenceCol = ColCounter
        End If
    Next
    
    BackupFPSequence1Col = FPSequence1Col
    
    If ReviewCol = 0 _
    Then
        Columns("A:A").Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
        ReviewCol = 1
        Cells(1, ReviewCol).Value2 = "REVIEW"
        If CommentsCol <> 0 Then CommentsCol = CommentsCol + 1
        If FPSequence1Col <> 0 Then FPSequence1Col = FPSequence1Col + 1
        InsertCounter = InsertCounter + 1
    End If
    If CommentsCol = 0 _
    Then
        Columns("B:B").Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
        CommentsCol = 2
        Cells(1, CommentsCol).Value2 = "COMMENTS"
        If FPSequence1Col <> 0 Then FPSequence1Col = FPSequence1Col + 1
        InsertCounter = InsertCounter + 1
    End If
'    If FPSequence2Col = 0 _
'    Then
'        If Cells(1, 3).Value2 = vbNullString _
'        Then
'            Cells(1, 3).Value2 = "FP SEQUENCE"
'            FPSequence2Col = FPSequence1Col
'            FPSequence1Col = 3
'        Else
'            FPSequence2Col = FPSequence1Col - InsertCounter
'            Columns("C:C").Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
'            FPSequence1Col = 3
'            Cells(1, FPSequence1Col).Value2 = "FP SEQUENCE"
'            InsertCounter = InsertCounter + 1
'        End If
'    End If
    If MatchPatternCol = 0 Then MatchPatternCol = ReviewCol
    If FPSequence2Col = 0 Then FPSequence2Col = FPSequence1Col
    If TaskIDCol = 0 Then TaskIDCol = MCIDCol

    For Counter = 1 To InsertCounter
        MatchPatternCol = MatchPatternCol + 1
        FPSequence2Col = FPSequence2Col + 1
        TaskIDCol = TaskIDCol + 1
        MCIDCol = MCIDCol + 1
        SourceKeyCol = SourceKeyCol + 1
        NameStatusCol = NameStatusCol + 1
        FirstNameCol = FirstNameCol + 1
        LastNameCol = LastNameCol + 1
        MidInitialCol = MidInitialCol + 1
        GenderCol = GenderCol + 1
        BirthDateCol = BirthDateCol + 1
        SSNCol = SSNCol + 1
    Next
    
    FilePath = Workbooks("MCID Analysis and Review.xlsm").Sheets("Sheet1").Range("B3").Value2
    Close #1
    Close #2
    Close #3
    Call AddItems
    Open FilePath & "Review Comments.txt" For Output As #1
    Open FilePath & "Debugging Log.txt" For Output As #2
    Print #1, "I agree with the Data Steward except for the following records:"
    Print #1,
    Debug.Print "I agree with the Data Steward except for the following records:" & Chr(10)
    
'   Set start row position
    FinalRow = InitialRow
    RowCounter = InitialRow
    TaskStartRow = InitialRow
    ReDim MCIDs(0 To 0)
    
'    Do While Cells(RowCounter, MCIDCol).Value2 <> vbNullString
'        If ExpiredMCIDCheck = True _
'        Then
'            If Cells(RowCounter, MCIDCol).Value2 <> Cells(RowCounter - 1, MCIDCol).Value2 _
'            Then
'                If MCIDs(UBound(MCIDs)) <> vbNullString _
'                Then ReDim Preserve MCIDs(LBound(MCIDs) To UBound(MCIDs) + 1)
'                MCIDs(UBound(MCIDs)) = Cells(RowCounter, MCIDCol).Value2
'            End If
'        End If
'        RowCounter = RowCounter + 1
'    Loop
'    FinalRow = RowCounter
    
    If ExpiredMCIDCheck = True And ExpiredMCIDCheckManualOverride = False _
    Then
        ExpiredMCIDQuery.Append ("select distinct MCID from MDM_PPLTN_XWALK where not MDM_PPLTN_XWALK.XWALK_TRMNTN_DT > current_date and MCID in (")
        For Counter = LBound(MCIDs) To UBound(MCIDs)
            ExpiredMCIDQuery.Append (MCIDs(Counter))
            If Counter < UBound(MCIDs) _
            Then ExpiredMCIDQuery.Append (", ")
        Next Counter
        ExpiredMCIDQuery.Append (")")
        QueryText = ExpiredMCIDQuery.ToString
        Debug.Print QueryText

'       sConnectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sPath & sFileName_Orig & ";Extended Properties=""Excel 12.0 Xml;HDR=YES"";"

        Set SQLConnection = CreateObject("ADODB.Connection")
        SQLConnection.Properties("Prompt") = 1
        SQLConnection.ConnectionTimeout = 60
        SQLConnection.CommandTimeout = 60
        SQLConnection.CursorLocation = adUseClient
'        SQLConnection.Open "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0 Xml;HDR=YES"
        SQLConnection.Open "Authentication=LDAP"

        Set SQLRecordset = CreateObject("ADODB.RecordSet")
        If SQLRecordset.State = 1 Then SQLRecordset.Close
        SQLRecordset.CursorLocation = adUseClient
        SQLRecordset.Open QueryText, SQLConnection

'        If SQLRecordset.State = 1 Then QueryRows = SQLRecordset.RecordCount
'        If QueryRows <> 0 Then
'        ExpiredMCIDs = SQLRecordset.GetRows(adGetRowsRest, adBookmarkFirst)

        ReDim ExpiredMCIDs(0 To 0)
        
        For PrintCounter = 0 To SQLRecordset.Fields.Count - 1
            Debug.Print SQLRecordset.Fields(PrintCounter).Name,
        Next
        SQLRecordset.MoveFirst
        Do Until SQLRecordset.EOF
            Debug.Print
            For PrintCounter = 0 To SQLRecordset.Fields.Count - 1
                If ExpiredMCIDs(LBound(ExpiredMCIDs)) <> vbNullString _
                Then ReDim Preserve ExpiredMCIDs(LBound(ExpiredMCIDs) To UBound(ExpiredMCIDs) + 1)
                ExpiredMCIDs(UBound(ExpiredMCIDs)) = Trim(SQLRecordset.Fields(PrintCounter).Value)
                Debug.Print SQLRecordset.Fields(PrintCounter).Value,
            Next
            SQLRecordset.MoveNext
        Loop
        
        For PrintCounter = LBound(ExpiredMCIDs) To UBound(ExpiredMCIDs)
            Debug.Print ExpiredMCIDs(PrintCounter)
        Next PrintCounter
    End If
                
    Do
    
        TaskEndRow = TaskStartRow
    
        Do While Cells(TaskEndRow, TaskIDCol).Value2 = Cells(TaskEndRow + 1, TaskIDCol).Value2 _
        And Cells(TaskEndRow, TaskIDCol).Value2 <> vbNullString
            TaskEndRow = TaskEndRow + 1
        Loop
        
        StartRow = TaskStartRow
        TaskIsFP = False
        TaskIsPO = False
        
        Do
        
            EndRow = StartRow
            
            Do While Cells(EndRow, MCIDCol).Value2 = Cells(EndRow + 1, MCIDCol).Value2 _
            And Cells(EndRow, TaskIDCol).Value2 = Cells(EndRow + 1, TaskIDCol).Value2 _
            And Cells(EndRow, MCIDCol).Value2 <> vbNullString
                EndRow = EndRow + 1
            Loop
            
            Call FalsePositiveReview(StartRow, EndRow, TaskStartRow, TaskEndRow, False)
            
'           Exit loop if end of task reached
            If EndRow = TaskEndRow _
            Or Cells(EndRow, MCIDCol).Value2 = vbNullString _
            Then Exit Do
            
            StartRow = EndRow + 1
            
        Loop
        
        If (FalseNegativeAnalysis = True Or FalseNegativeReview = True) And TaskIsFP = False _
        Then Call FalseNegativeComments(TaskIsFP, TaskIsPO, TaskStartRow, TaskEndRow)
            
        TaskStartRow = TaskEndRow + 1
        
'       Exit loop if end of file reached
        If Cells(TaskStartRow, TaskIDCol).Value2 = vbNullString _
        Then Exit Do
        
    Loop
    
    With Application.ActiveWorkbook.ActiveSheet

        ActiveWindow.Zoom = 100
        ActiveSheet.Select
'        If ResizeCols = True _
'        Then
'            .Columns("A:AA").Hidden = True
'            .Range(.Cells(2, ReviewCol), .Range(.Cells(2, ReviewCol), .Cells(FinalRow, ReviewCol))).Columns.AutoFit
'            .Columns(CommentsCol).AutoFit
'            .Range(.Cells(2, FPSequence1Col), .Range(.Cells(2, FPSequence1Col), .Cells(FinalRow, FPSequence1Col))).Columns.AutoFit
'            .Columns(MatchPatternCol).ColumnWidth = 5.86
'            .Range(.Cells(2, FPSequence2Col), .Range(.Cells(2, FPSequence2Col), .Cells(FinalRow, FPSequence2Col))).Columns.AutoFit
'            .Columns(TaskIDCol).AutoFit
'            .Columns(MCIDCol).AutoFit
'            .Columns(SourceKeyCol).AutoFit
'            .Range(.Cells(2, NameStatusCol), .Range(.Cells(2, NameStatusCol), .Cells(FinalRow, NameStatusCol))).Columns.AutoFit
'            .Columns(FirstNameCol).AutoFit
'            .Columns(LastNameCol).AutoFit
'            .Range(.Cells(2, MidInitialCol), .Range(.Cells(2, MidInitialCol), .Cells(FinalRow, MidInitialCol))).Columns.AutoFit
'            .Range(.Cells(2, GenderCol), .Range(.Cells(2, GenderCol), .Cells(FinalRow, GenderCol))).Columns.AutoFit
'            .Columns(BirthDateCol).AutoFit
'            .Columns(SSNCol).AutoFit
'            .Columns("AA:AS").AutoFit
'        End If
'
'        For ColCounter = 25 To 45
'            For CurrentRow = 2 To FinalRow
'                If IsNumeric(.Cells(CurrentRow, ColCounter).Value2) = True _
'                Then .Cells(CurrentRow, ColCounter).Errors(3).Ignore = True
'            Next
'        Next
        
        ActiveSheet.AutoFilterMode = False
        With ActiveWindow
            If .FreezePanes Then .FreezePanes = False
            .SplitColumn = 0
            .SplitRow = 1
            .FreezePanes = True
        End With
        ActiveSheet.Cells(1, 1).Select
        
    End With
    
    Close #1
    Close #2
    Application.ScreenUpdating = True
    
    SecondsElapsed = Round(Timer - StartTime, 2)
    RemSecondsElapsed = SecondsElapsed Mod 60
    MinutesElapsed = (SecondsElapsed - RemSecondsElapsed) / 60
    If FalseNegativeAnalysis = True And ExpiredMCIDsCount < 4 _
    Then
        MsgBox "Update Expired MCIDs list"
    Else
        MsgBox "Done"
    End If
    MsgBox "This code ran successfully in " & MinutesElapsed & " minutes, " & RemSecondsElapsed & " seconds", vbInformation
        
End Sub

Sub FalsePositiveReview(ByVal StartRow As Integer, ByVal EndRow As Integer, ByVal TaskStartRow As Integer, ByVal TaskEndRow As Integer, ByVal FalseNegativeCheck As Boolean)

    Dim PersonFound As Boolean, DiffMIFound As Boolean, DiffLastName As Boolean, DuplicateMIFound As Boolean
    Dim NameCounter As Integer, NameCounterAgain As Integer, DeleteCounter As Integer, ExactNameCounter As Integer
    Dim NameFrequency As Integer
    Dim KeyCounter As Double, HighestKey As Double
    Dim AnalystReview As String
    Dim ReviewerReview As String

'   Set variables
    Counter = 1
    PersonKey = 1.1
    PersonNumber = 0
    PersonCounter = 0
    KeyCounter = 1
    MCID = vbNullString
    MCIDIsFP = False
    MCIDIsPO = False
    AmbiguousFNMCID = False
    FNDiffersMCID = False
    AmbiguousLNMCID = False
    LNDiffersMCID = False
    MIDiffersMCID = False
    AmbiguousDOBMCID = False
    DOBDiffersMCID = False
    DuplicateMIFound = False
    PeopleKeys.RemoveAll
    PeopleKeys.CompareMode = vbTextCompare
    MIExactMatch = True
    MarriageRule = True
    TwinsRule = False
    AmbiguousSSN = False
    AmbiguousFirst = False
    AmbiguousLast = False
    AmbiguousBirth = False
    HardAmbiguousFN = False
    HardAmbiguousLN = False
    LastNameCounter = 1
    AmbiguousExactFN = False
    AmbiguousExactLN = False
    FNMatch = False
    FNLNSwapped = False
    FNTypo = False
    FNNickname = False
    LNMatch = False
    LNTypo = False
    LNMarriage = False
    DOBMatch = False
    DOBTypo = False
    SSNMatch = True
    SSNMissing = False
    TwinsGenderDiffers = False
    ExpiredMCID = False
    
    If FalseNegativeCheck = True _
    Then
        FPSequence1Col = FNSequenceCol
    ElseIf FalseNegativeCheck = False _
    Then
        FPSequence1Col = BackupFPSequence1Col
    End If
    
    CurrentRow = StartRow
    MCID = Cells(StartRow, MCIDCol).Value2

'   Redimension person array
    ReDim People(0 To 1)
    
    GenderMCID = GenderCount(TaskStartRow, TaskEndRow)
    
'   Remove non-alphanumeric characters from relevant record fields
    Call ExactNamesCount(TaskStartRow, TaskEndRow)
    Call ExactNamesCountSpecial(TaskStartRow, TaskEndRow)
    Call ExactDatesCount(StartRow, EndRow)
    Call ScrapeRecord
    
    If (InStr(FirstName, "TEST") <> 0 And InStr(LastName, "TEST") <> 0) _
    Or (InStr(FirstName, "TEST") <> 0 And InStr(LastName, "SAMPLE") <> 0) _
    Then
        Cells(CurrentRow, CommentsCol).Value2 = "test data"
        Cells(CurrentRow, FPSequence1Col).Value2 = "0"
    End If
    If FalseNegativeAnalysis = True _
    Then
        If IsInArray(CStr(Cells(CurrentRow, MCIDCol).Value2), CurrentValidMCIDs, "Exact") = False _
        Then ExpiredMCID = True
    ElseIf FalseNegativeReview = True _
    Then
        If UCase(Cells(CurrentRow, MatchPatternCol).Value2) = "EXPIRED MCID" _
        Then ExpiredMCID = True
    End If
        
    AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
    
    Call PreCheck1
    
    For NameCounterAgain = 0 To UBound(ExactNames, 2)

        ExactFirstNames = SplitName(ExactNames(0, NameCounterAgain))
        ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
        AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)

        If IsInEachArray(FirstNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousFNs, AmbiguousExactFNs) = True _
        And UBound(ExactFirstNames) > UBound(FirstNames) _
        Then
            For NameCounter = 0 To UBound(MidInitials)
                If IsInArray(MidInitials(NameCounter), ExactFirstNames, "Left", , , , AmbiguousExactFNs, FirstNames) = True _
                And (UBound(ExactFirstNames) + 1) > (UBound(FirstNames) + 1) + (UBound(MidInitials) + 1) _
                Then
                    MidInitials(NameCounter) = vbNullString
                    If UBound(MidInitials) > 0 _
                    Then
                        For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                            MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                        Next
                        ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                    End If
                    Exit For
                End If
            Next
        End If
    Next

    For NameCounterAgain = 0 To UBound(ExactNames, 2)

        ExactLastNames = SplitName(ExactNames(1, NameCounterAgain))
        ReDim AmbiguousExactLNs(0 To UBound(ExactLastNames))
        AmbiguousExactLN = AmbiguousLNCheck(ExactLastNames, AmbiguousExactLNs)

        For NameCounter = 0 To UBound(MidInitials)
            If IsInArray(MidInitials(NameCounter), ExactLastNames, "Left", , , , AmbiguousExactLNs, LastNames) = True _
            And UBound(ExactLastNames) > 0 _
            Then
                MidInitials(NameCounter) = vbNullString
                If UBound(MidInitials) > 0 _
                And UBound(ExactLastNames) > 0 _
                Then
                    For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                        MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                    Next
                    ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                End If
                Exit For
            End If
        Next

    Next
    
    Call PreCheck3
    
'   Add first person in task to array
    Call RedimensionArray
    
    Cells(CurrentRow, FPSequence1Col).Value2 = PersonNumber
    
    Do
        
'       Set variables
        DiffMIFound = False
        AmbiguousExactFN = False
        AmbiguousExactLN = False

        CurrentRow = CurrentRow + 1
        
'       Remove non-alphanumeric characters from relevant record fields
        Call ScrapeRecord
        
        If (InStr(FirstName, "TEST") <> 0 And InStr(LastName, "TEST") <> 0) _
        Or (InStr(FirstName, "TEST") <> 0 And InStr(LastName, "SAMPLE") <> 0) _
        Then
            Cells(CommentsCol).Value2 = "test data"
            Cells(FPSequence1Col).Value2 = "0"
        End If
        If FalseNegativeAnalysis = True _
        Then
            If IsInArray(CStr(Cells(CurrentRow, MCIDCol).Value2), CurrentValidMCIDs, "Exact") = False _
            Then ExpiredMCID = True
        ElseIf FalseNegativeReview = True _
        Then
            If UCase(Cells(CurrentRow, MatchPatternCol).Value2) = "EXPIRED MCID" _
            Then ExpiredMCID = True
        End If
        
        AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
        AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
        
        Call PreCheck1
        
        For NameCounterAgain = 0 To UBound(ExactNames, 2)
    
            ExactFirstNames = SplitName(ExactNames(0, NameCounterAgain))
            ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
            AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
    
            If IsInEachArray(FirstNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousFNs, AmbiguousExactFNs) = True _
            And UBound(ExactFirstNames) > UBound(FirstNames) _
            Then
                For NameCounter = 0 To UBound(MidInitials)
                    If IsInArray(MidInitials(NameCounter), ExactFirstNames, "Left", , , , AmbiguousExactFNs, FirstNames) = True _
                    And IsInArray(MidInitials(NameCounter), FirstNames, "Left", , , , AmbiguousFNs, ExactFirstNames) = False _
                    And (UBound(ExactFirstNames) + 1) > (UBound(FirstNames) + 1) + (UBound(MidInitials) + 1) _
                    Then
                        MidInitials(NameCounter) = vbNullString
                        If UBound(MidInitials) > 0 _
                        Then
                            For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                                MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                            Next
                            ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                        End If
                        Exit For
                    End If
                Next
    
            End If
        Next
    
        For NameCounterAgain = 0 To UBound(ExactNames, 2)
    
            ExactLastNames = SplitName(ExactNames(1, NameCounterAgain))
            ReDim AmbiguousExactLNs(0 To UBound(ExactLastNames))
            AmbiguousExactLN = AmbiguousLNCheck(ExactLastNames, AmbiguousExactLNs)
    
            For NameCounter = 0 To UBound(MidInitials)
                If IsInArray(MidInitials(NameCounter), ExactLastNames, "Left", , , , AmbiguousExactLNs, LastNames) = True _
                And UBound(ExactLastNames) > 0 _
                Then
                    MidInitials(NameCounter) = vbNullString
                    If UBound(MidInitials) > 0 _
                    And UBound(ExactLastNames) > 0 _
                    Then
                        For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                            MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                        Next
                        ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                    End If
                    Exit For
                End If
            Next
            
        Next
        
        Call PreCheck2
        
        AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
        AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
            
    '   Compare current record against all people in person array
        For Counter = 1 To UBound(People)
            
'           Set variables
            FNDiffers = False
            LNDiffers = False
            MIDiffers = False
            DOBDiffers = False
            PersonFound = False
            AmbiguousFirst = False
            AmbiguousLast = False
            AmbiguousBirth = False
            HardAmbiguousFN = False
            HardAmbiguousLN = False
            FNExactMatch = False
            LNExactMatch = False
            DOBExactMatch = False
            DuplicateMIFound = False
            DiffLastName = True
            
            If StartRow = EndRow Then Exit Do
            
            If Date - BirthDate < 9496 _
            And AmbiguousDOB = False _
            Then TwinsRule = True _
            
            AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
            AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
            FNDiffers = FirstNameCheck(FirstNames, AmbiguousFNs, Counter, FalseNegativeCheck)
            LNDiffers = LastNameCheck(LastNames, AmbiguousLNs, Counter)
            
            Call PreCheck3

            If IsInEachArray(MidInitials, People(Counter).MidInitial, "Left", , True, "LeftFoundInRight") = False _
            And IsInEachArray(MidInitials, People(Counter).MidInitial, "Left", , True, "RightFoundInLeft") = False _
            Then
                MIDiffers = True
            Else
                If IsInEachArray(MidInitials, People(Counter).FirstName, "Left", , True, "LeftFoundInRight") = True _
                Or IsInEachArray(FirstNames, People(Counter).MidInitial, "Left") = True _
                Then
                    For NameCounter = LBound(ExactNames, 2) To UBound(ExactNames, 2)
                        ExactFirstNames = SplitName(ExactNames(0, NameCounter))
                        ExactMidInitials = SplitName(ExactNames(2, NameCounter))
                        If IsInEachArray(People(Counter).FirstName, ExactFirstNames, "Exact", , True, "RightFoundInLeft") = True _
                        Or IsInEachArray(FirstNames, ExactFirstNames, "Exact", , True, "LeftEqualsRight") = True _
                        Then
                            If IsInEachArray(MidInitials, ExactMidInitials, "Left", , True, "LeftEqualsRight") = False _
                            And IsInArray(vbNullString, ExactMidInitials, "Exact") = False _
                            Then DuplicateMIFound = True
                        End If
                    Next
                End If
            End If
            
            If MIDiffers = True Then
                MIDiffersMCID = True
                MIExactMatch = False
            End If
            
            If (Date - BirthDate < 6574 _
            And AmbiguousDOB = False) _
            Or GenderMCID = "M" _
            Then MarriageRule = False _

            DOBDiffers = BirthDateCheck(BirthDate, Counter)
            
            If FNDiffers = False _
            And LNDiffers = False _
            And MIDiffers = True _
            And DOBDiffers = False _
            And (AmbiguousFirst = False _
            Or (AmbiguousFirst = True _
            And IsInArray(SourceKey, People(Counter).SourceKey, "Exact") = True)) _
            And (AmbiguousLast = False _
            Or (AmbiguousLast = True _
            And IsInArray(SourceKey, People(Counter).SourceKey, "Exact") = True)) _
            And AmbiguousBirth = False _
            And ExpiredMCID = People(Counter).ExpiredMCID _
            And MCID = People(Counter).MCID _
            Then
        
                DiffMIFound = True
                HighestKey = People(Counter).PersonKey
            
            ElseIf FNDiffers = False _
            And LNDiffers = False _
            And MIDiffers = False _
            And DOBDiffers = False _
            And (AmbiguousFirst = False _
            Or (AmbiguousFirst = True _
            And IsInArray(SourceKey, People(Counter).SourceKey, "Exact") = True)) _
            And (AmbiguousLast = False _
            Or (AmbiguousLast = True _
            And IsInArray(SourceKey, People(Counter).SourceKey, "Exact") = True)) _
            And AmbiguousBirth = False _
            And ExpiredMCID = People(Counter).ExpiredMCID _
            And MCID = People(Counter).MCID _
            Then
                
                PersonFound = True
                PersonNumber = People(Counter).PersonNumber
                If NameStatus = "A" Then People(Counter).ActiveRecords = True
                PeopleKeys(People(Counter).PersonKey) = People(Counter).ActiveRecords
                
                People(Counter).SourceKey = SourceKey
                People(Counter).NameStatus = NameStatus
                People(Counter).FirstNameFull = FirstName
                
                If FNExactMatch = False Then
                    For NameCounter = 0 To UBound(FirstNames)
                        People(Counter).FirstName = FirstNames(NameCounter)
                        People(Counter).AmbiguousFN = AmbiguousFNs(NameCounter)
                    Next
                End If
                
                People(Counter).LastNameFull = LastName
                
                If LNExactMatch = False Then
                    For NameCounter = 0 To UBound(LastNames)
                        People(Counter).LastName = LastNames(NameCounter)
                        People(Counter).AmbiguousLN = AmbiguousLNs(NameCounter)
                    Next
                End If
                
                For NameCounter = 0 To UBound(MidInitials)
                    People(Counter).MidInitial = MidInitials(NameCounter)
                Next
                
                People(Counter).BirthDate = CStr(BirthDate)
                People(Counter).AmbiguousDOB = AmbiguousDOB
                People(Counter).Gender = Gender
                People(Counter).SubscriberID = SubscriberID
                People(Counter).SSN = SSN
                People(Counter).AmbiguousSSN = AmbiguousSSN
                People(Counter).HCID = HCID
                People(Counter).HardAmbiguousFN = HardAmbiguousFN
                People(Counter).HardAmbiguousLN = HardAmbiguousLN
                People(Counter).AmbiguousFNFull = AmbiguousFN
                People(Counter).AmbiguousLNFull = AmbiguousLN
                Exit For
            
            End If
            
            If AmbiguousFirst = True Then AmbiguousFNMCID = True
            If FNDiffers = True Then FNDiffersMCID = True
            If AmbiguousLast = True Then AmbiguousLNMCID = True
            If LNDiffers = True Then LNDiffersMCID = True
            If AmbiguousBirth = True Then AmbiguousDOBMCID = True
            If DOBDiffers = True Then DOBDiffersMCID = True
            If LNDiffers = False Then DiffLastName = False
        
        Next
        
        If PersonFound = False _
        Then
            If DiffMIFound = True _
            Then
                PersonKey = HighestKey + 0.1
            ElseIf DiffMIFound = False _
            Then
                KeyCounter = KeyCounter + 1
                PersonKey = KeyCounter + 0.1
                If DiffLastName = True _
                And AmbiguousLN = False _
                Then LastNameCounter = LastNameCounter + 1
            End If
            Call RedimensionArray
        End If
    
        Cells(CurrentRow, FPSequence1Col).Value2 = PersonNumber
'       Exit loop if end of task reached
        If CurrentRow = EndRow Then Exit Do
    
    Loop
    If FalseNegativeCheck = False _
    Then
        If LastNameCounter > 4 Then MarriageRule = False
        Call RecordPeople
    End If
'    If FalseNegativeCheck = True Then Call RecordPeople
    If MIDiffersMCID = True Then MIDiffersMCID = MidInitialCheck(MidInitials, StartRow, EndRow)
    If UBound(People) > 1 Then Call ReCheck(FalseNegativeCheck)
    
    If UBound(People) > 1 Then MCIDIsPO = POCheck(StartRow, EndRow)
    If FalseNegativeCheck = False _
    Then If MCIDIsPO = True Then TaskIsPO = True
    ReviewFPSequence = FPSequenceCheck(StartRow, EndRow)
    If UBound(People) > 1 Then MCIDIsFP = FPCheck(StartRow, EndRow, FalseNegativeCheck)
    If FalseNegativeCheck = False _
    Then If MCIDIsFP = True Then TaskIsFP = True
    AnalystReview = FindAnalystReview(StartRow, EndRow, AnalystReview)
    ReviewerReview = FindAnalystReview(StartRow, EndRow, ReviewerReview)
    Call ReviewComments(AmbiguousFNMCID, FNDiffersMCID, AmbiguousLNMCID, LNDiffersMCID, MIDiffersMCID, DOBDiffersMCID, FalseNegativeCheck, AnalystReview)
    If FalseNegativeCheck = False Then Call FlaggedMCIDComments(StartRow, EndRow, ReviewFPSequence, AnalystReview, ReviewerReview)

End Sub

Function HypocorismCheck(ByVal Name1 As Variant, ByVal Name2 As Variant) As Boolean

    Dim Hypocorism() As String
    Dim FullName() As String
    Dim HypocorismCounter As Integer
    Dim FullNameCounter As Integer
    
    ReDim Hypocorism(0 To 0) As String
    ReDim FullName(0 To 0) As String
    
'    If Len(Name1) < Len(Name2) Then
'        Hypocorism(0) = Name1
'        FullName = Name2
'    ElseIf Len(Name2) < Len(Name1) Then
'        Hypocorism(0) = Name2
'        FullName = Name1
'    ElseIf Len(Name1) = Len(Name2) Then
'        If Right(Name1, 1) = "E" And Right(Name2, 1) <> "E" Then
'            Hypocorism(0) = Name1
'            FullName = Name2
'        ElseIf Right(Name2, 1) = "E" And Right(Name1, 1) <> "E" Then
'            Hypocorism(0) = Name2
'            FullName = Name1
'        Else
'            Exit Function
'        End If
'    End If

    Hypocorism(0) = Name1
    FullName(0) = Name2

    Hypocorism = NameTransform(Hypocorism())
    FullName = NameTransform(FullName())
    
    For HypocorismCounter = 0 To UBound(Hypocorism)
        For FullNameCounter = 0 To UBound(FullName)
            If Len(FullName(FullNameCounter)) < 3 Then
                If Left(Hypocorism(HypocorismCounter), 2) = FullName(FullNameCounter) Then HypocorismCheck = True
            ElseIf Len(Hypocorism(HypocorismCounter)) < 3 Then
                If Left(FullName(FullNameCounter), 2) = Hypocorism(HypocorismCounter) Then HypocorismCheck = True
            Else
                If InStr(FullName(FullNameCounter), Hypocorism(HypocorismCounter)) <> 0 _
                Or InStr(Hypocorism(HypocorismCounter), FullName(FullNameCounter)) <> 0 _
                Then HypocorismCheck = True
            End If
        Next
    Next
    
End Function

Function NameTransform(ByRef Hypocorism() As String) As String()

    Dim BoundCounter As Integer
    Dim DoubleBound As Integer
    Dim StartBound As Integer
    
    If Len(Hypocorism(0)) = 4 _
    Then
        If Right(Hypocorism(0), 2) = Left(Hypocorism(0), 2) _
        Then
            Hypocorism(0) = Left(Hypocorism(0), 2)
            NameTransform = Hypocorism
            Exit Function
        End If
    End If
    
    Select Case Right(Hypocorism(0), 6)
        Case Is = "SHULYA"
            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 6)
        Case Else
            Select Case Right(Hypocorism(0), 5)
                Case Is = "YASHA"
                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 5)
                Case Is = "YOSHA"
                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 5)
                Case Is = "YOZHA"
                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 5)
                Case Is = "YUSHA"
                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 5)
                Case Else
                    Select Case Right(Hypocorism(0), 4)
                        Case Is = "EIGH"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ELLA"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ELLE"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ELLO"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ETTA"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ETTE"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ETTO"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "ONNE"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "OTTE"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "USYA"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "UZZA"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "UZZO"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Is = "YUTA"
                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 4)
                        Case Else
                            Select Case Right(Hypocorism(0), 3)
                                Case Is = "CHA"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2) & "CIA"
                                Case Is = "CHO"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "DGE"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 3) & "G"
                                Case Is = "INA"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "INE"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "INO"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "ITA"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "ITO"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "RGE"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                Case Is = "SHA"
                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 3)
                                Case Else
                                    Select Case Right(Hypocorism(0), 2)
                                        Case Is = "CE"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2) & "S"
                                        Case Is = "EE"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "EL"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "ET"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "EY"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "GH"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "IE"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "IK"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "IN"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "ON"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "OT"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "OU"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Is = "YA"
                                            Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2)
                                        Case Else
                                            Select Case Right(Hypocorism(0), 1)
                                                Case Is = "A"
                                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                                                Case Is = "E"
                                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                                                Case Is = "I"
                                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                                                Case Is = "O"
                                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                                                Case Is = "Y"
                                                    Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                                            End Select
                                    End Select
                            End Select
                    End Select
            End Select
    End Select
    
'    If "XA" Then "ZA"

    If Len(Hypocorism(0)) > 1 _
    Then
        If Right(Hypocorism(0), 1) = Mid(Hypocorism(0), Len(Hypocorism(0)) - 1, 1) _
        Then Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
        Select Case Hypocorism(0)
            Case Is = "MAG"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "MARG"
            Case Is = "MOL"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "MAR"
            Case Is = "SAL"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "SAR"
            Case Is = "SAD"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "SAR"
            Case Is = "NANC"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "ANN"
            Case Is = "HAR"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "HENR"
            Case Is = "BOB"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "ROB"
            Case Is = "PEG"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "MEG"
            Case Is = "LAR"
                ReDim Preserve Hypocorism(0 To 2)
                Hypocorism(1) = "LAUR"
                Hypocorism(2) = "LAWR"
            Case Is = "TON"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "THON"
            Case Is = "POL"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "PAUL"
            Case Is = "JOHN"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "JON"
            Case Is = "LEN"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(1) = "LEON"
        End Select
        Select Case Right(Hypocorism(0), 2)
            Case Is = "TS"
                ReDim Preserve Hypocorism(0 To 3)
                Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) & "Z"
                Hypocorism(2) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2) & "S"
                Hypocorism(3) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2) & "Z"
            Case Is = "CK"
                ReDim Preserve Hypocorism(0 To 1)
                Hypocorism(0) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1)
                Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) & "K"
            Case Else
                Select Case Right(Hypocorism(0), 1)
                    Case Is = "Z"
                        ReDim Preserve Hypocorism(0 To 1)
                        Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) & "S"
                    Case Is = "S"
                        ReDim Preserve Hypocorism(0 To 1)
                        Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) & "Z"
                    Case Is = "K"
                        ReDim Preserve Hypocorism(0 To 1)
                        Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) + "C"
                    Case Is = "V"
                        ReDim Preserve Hypocorism(0 To 1)
                        Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 2) + "PH"
                    Case Is = "D"
                        ReDim Preserve Hypocorism(0 To 1)
                        Hypocorism(1) = Left(Hypocorism(0), Len(Hypocorism(0)) - 1) + "T"
                End Select
        End Select
    End If
    
    If Len(Hypocorism(0)) > 2 Then
        Select Case Left(Hypocorism(0), 3)
            Case Is = "CHR"
                StartBound = UBound(Hypocorism)
                DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                ReDim Preserve Hypocorism(0 To DoubleBound)
                For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                    Hypocorism(BoundCounter) = "CR" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 3)
                Next
            Case Else
                Select Case Left(Hypocorism(0), 2)
                    Case Is = "CR"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = (UBound(Hypocorism) + 1) * 2 - 1
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "CHR" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 3)
                        Next
                    Case Is = "KA"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = (UBound(Hypocorism) + 1) * 2 - 1
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "CA" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "KO"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "CO" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "CA"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "KA" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "CO"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "KO" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "CHO" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "SH"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "CH" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "GE"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "JE" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Is = "JE"
                        StartBound = UBound(Hypocorism)
                        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                            Hypocorism(BoundCounter) = "GE" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                        Next
                    Case Else
                        Select Case Left(Hypocorism(0), 1)
                            Case Is = "F"
                                StartBound = UBound(Hypocorism)
                                DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
                                ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
                                For BoundCounter = StartBound + 1 To UBound(Hypocorism)
                                    Hypocorism(BoundCounter) = "PH" & Right(Hypocorism(DoubleBound - BoundCounter), Len(Hypocorism(DoubleBound - BoundCounter)) - 2)
                                Next
                        End Select
                End Select
        End Select
    End If
    If InStr(Hypocorism(0), "Y") <> 0 _
    Then
        StartBound = UBound(Hypocorism)
        DoubleBound = ((UBound(Hypocorism) + 1) * 2 - 1)
        ReDim Preserve Hypocorism(LBound(Hypocorism) To DoubleBound)
        For BoundCounter = StartBound + 1 To UBound(Hypocorism)
           Hypocorism(BoundCounter) = Replace(Hypocorism(DoubleBound - BoundCounter), "Y", "I")
        Next
    End If
    
    NameTransform = Hypocorism
    
End Function

Sub ExactNamesCount(ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim ExactFirstName As String
    Dim ExactLastName As String
    Dim ExactMidInitial As String
    Dim NameCounter As Integer
    Dim PrintCounter As Integer
    Dim RowCounter As Integer
'    Dim SubscriberID As String
'    Dim SubscriberIDs() As Variant
    Dim SubscriberIDCounter As Integer
    Dim SSNCounter As Integer
    Dim SSNFound As Boolean
'    Dim HCID As String
'    Dim HCIDs() As String

'    ReDim SubscriberIDs(0 To 1, 0 To 0)
'    For RowCounter = StartRow To EndRow
'        SubscriberID = Cells(RowCounter, SubscriberIDCol).Value2
'        If AmbiguousSSNCheck(SubscriberID) = False _
'        Then
'            If UBound(SubscriberIDs) > 0 _
'            Then
'                For SubscriberIDCounter = 0 To UBound(SubscriberIDs, 2)
'                    If SubscriberID = SubscriberIDs(0, SubscriberIDCounter) _
'                    Then
'                        SubscriberIDs(1, SubscriberIDCounter) = SubscriberIDs(1, SubscriberIDCounter) + 1
'                        Exit For
'                    End If
'                    ReDim SubscriberIDs(0 To 1, 0 To UBound(SubscriberIDs) + 1)
'                    SubscriberIDs(0, UBound(SubscriberIDs)) = SubscriberID
'                    SubscriberIDs(1, UBound(SubscriberIDs)) = 1
'                Next
'            Else
'            SubscriberIDs(0, UBound(SubscriberIDs)) = SubscriberID
'            SubscriberIDs(1, UBound(SubscriberIDs)) = 1
'            End If
'        End If
'    Next
'
'    ReDim HCIDs(0 To 1, 0 To 0)
'    For RowCounter = StartRow To EndRow
'        HCID = Cells(RowCounter, HCIDCol).Value2
'        If AmbiguousSSNCheck(HCID) = False _
'        Then
'            If UBound(HCIDs) > 0 _
'            Then
'                For HCIDCounter = 0 To UBound(HCIDs, 2)
'                    If HCID = HCIDs(0, HCIDCounter) _
'                    Then
'                        HCIDs(1, HCIDCounter) = HCIDs(1, HCIDCounter) + 1
'                        Exit For
'                    End If
'                    ReDim HCIDs(0 To 1, 0 To UBound(HCIDs) + 1)
'                    HCIDs(0, UBound(HCIDs)) = HCID
'                    HCIDs(1, UBound(HCIDs)) = 1
'                Next
'            Else
'            HCIDs(0, UBound(HCIDs)) = HCID
'            HCIDs(1, UBound(HCIDs)) = 1
'            End If
'        End If
'    Next
'
'    ReDim HCIDs(0 To 1, 0 To 0)
'    For RowCounter = StartRow To EndRow
'        HCID = Cells(RowCounter, HCIDCol).Value2
'        If AmbiguousSSNCheck(HCID) = False _
'        Then
'            If UBound(HCIDs) > 0 _
'            Then
'                For HCIDCounter = 0 To UBound(HCIDs, 2)
'                    If HCID = HCIDs(0, HCIDCounter) _
'                    Then
'                        HCIDs(1, HCIDCounter) = HCIDs(1, HCIDCounter) + 1
'                        Exit For
'                    End If
'                    ReDim HCIDs(0 To 1, 0 To UBound(HCIDs) + 1)
'                    HCIDs(0, UBound(HCIDs)) = HCID
'                    HCIDs(1, UBound(HCIDs)) = 1
'                Next
'            Else
'            HCIDs(0, UBound(HCIDs)) = HCID
'            HCIDs(1, UBound(HCIDs)) = 1
'            End If
'        End If
'    Next
    
'    For RowCounter = StartRow To EndRow
'        HCIDs.SSN = Cells(RowCounter, SubscriberCol).Value2
'        AmbiguousHCID = AmbiguousSSNCheck(Cells(RowCounter, SuscriberIDCol).Value2)
'        HCIDs.AmbiguousSSN = AmbiguousHCID
'    Next

    ReDim ExactNames(0 To 6, 0 To 0)
    ExactFirstName = Cells(StartRow, FirstNameCol).Value2
    ExactLastName = Cells(StartRow, LastNameCol).Value2
    ExactMidInitial = Cells(StartRow, MidInitialCol).Value2
    ExactNames(0, 0) = FormatName(ExactFirstName)
    ExactNames(1, 0) = FormatName(ExactLastName)
    ExactNames(2, 0) = FormatName(ExactMidInitial)
    ExactNames(3, 0) = "1"
    ExactNames(4, 0) = "0"
    ExactNames(5, 0) = "0"
    If Cells(StartRow, GenderCol).Value2 = "F" Then ExactNames(4, 0) = "1"
    If Cells(StartRow, GenderCol).Value2 = "M" Then ExactNames(5, 0) = "1"
    ReDim SSNs(0 To 0)
    AmbiguousSSN = AmbiguousSSNCheck(Cells(StartRow, SSNCol).Value2)
    If AmbiguousSSN = False _
    Then
        SSNs(0).SSN = Cells(StartRow, SSNCol).Value2
        SSNs(0).AmbiguousSSN = AmbiguousSSN
        SSNs(0).SSNCount = 1
    End If
    ReDim SubscriberIDs(0 To 0)
    AmbiguousSSN = AmbiguousSSNCheck(Cells(StartRow, SubscriberIDCol).Value2)
    If AmbiguousSSN = False _
    Then
        SubscriberIDs(0).SSN = Cells(StartRow, SubscriberIDCol).Value2
        SubscriberIDs(0).AmbiguousSSN = AmbiguousSSN
    End If
    For RowCounter = StartRow + 1 To EndRow
        ExactFirstName = Cells(RowCounter, FirstNameCol).Value2
        ExactLastName = Cells(RowCounter, LastNameCol).Value2
        ExactMidInitial = FormatName(Cells(RowCounter, MidInitialCol).Value2)
        For NameCounter = 0 To UBound(ExactNames, 2)
            If ExactNames(0, NameCounter) = FormatName(ExactFirstName) _
            And ExactNames(1, NameCounter) = FormatName(ExactLastName) _
            And ExactNames(2, NameCounter) = FormatName(ExactMidInitial) _
            Then
                ExactNames(3, NameCounter) = CStr(CInt(ExactNames(3, NameCounter)) + 1)
                If Cells(RowCounter, GenderCol).Value2 = "F" Then ExactNames(4, NameCounter) = CStr(CInt(ExactNames(4, NameCounter)) + 1)
                If Cells(RowCounter, GenderCol).Value2 = "M" Then ExactNames(5, NameCounter) = CStr(CInt(ExactNames(5, NameCounter)) + 1)
                SSNFound = False
                If SSNs(NameCounter).CheckArrayInitialized = True _
                Then
                    For SSNCounter = 0 To UBound(SSNs(NameCounter).SSN)
                        If Cells(RowCounter, SSNCol).Value2 = SSNs(NameCounter).SSN(SSNCounter) _
                        Then
                            SSNFound = True
                            Exit For
                        End If
                    Next
                End If
                If SSNFound = True _
                Then
                    SSNs(NameCounter).IncreaseSSNCount SSNCounter
                ElseIf SSNFound = False _
                Then
                    AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SSNCol).Value2)
                    If AmbiguousSSN = False _
                    Then
                        SSNs(NameCounter).SSN = Cells(RowCounter, SSNCol).Value2
                        SSNs(NameCounter).AmbiguousSSN = AmbiguousSSN
                        SSNs(NameCounter).SSNCount = 1
                    End If
                End If
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SubscriberIDCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SubscriberIDs(NameCounter).SSN = Cells(RowCounter, SubscriberIDCol).Value2
                    SubscriberIDs(NameCounter).AmbiguousSSN = AmbiguousSSN
                End If
                Exit For
            End If
            If NameCounter = UBound(ExactNames, 2) _
            Then
                ReDim Preserve ExactNames(0 To 6, 0 To UBound(ExactNames, 2) + 1)
                ExactNames(0, UBound(ExactNames, 2)) = FormatName(ExactFirstName)
                ExactNames(1, UBound(ExactNames, 2)) = FormatName(ExactLastName)
                ExactNames(2, UBound(ExactNames, 2)) = FormatName(ExactMidInitial)
                ExactNames(3, UBound(ExactNames, 2)) = "1"
                ExactNames(4, UBound(ExactNames, 2)) = "0"
                ExactNames(5, UBound(ExactNames, 2)) = "0"
                If Cells(RowCounter, GenderCol).Value2 = "F" Then ExactNames(4, UBound(ExactNames, 2)) = "1"
                If Cells(RowCounter, GenderCol).Value2 = "M" Then ExactNames(5, UBound(ExactNames, 2)) = "1"
                ReDim Preserve SSNs(0 To UBound(SSNs) + 1)
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SSNCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SSNs(UBound(SSNs)).SSN = Cells(RowCounter, SSNCol).Value2
                    SSNs(UBound(SSNs)).AmbiguousSSN = AmbiguousSSN
                    SSNs(UBound(SSNs)).SSNCount = 1
                End If
                ReDim Preserve SubscriberIDs(0 To UBound(SubscriberIDs) + 1)
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SubscriberIDCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SubscriberIDs(UBound(SubscriberIDs)).SSN = Cells(RowCounter, SubscriberIDCol).Value2
                    SubscriberIDs(UBound(SubscriberIDs)).AmbiguousSSN = AmbiguousSSN
                End If
            End If
        Next
    Next
    
'    For NameCounter = 0 To UBound(ExactNames, 2)
'        Debug.Print ExactNames(0, NameCounter) & " " & _
'        ExactNames(1, NameCounter) & " " & _
'        ExactNames(2, NameCounter) & " " & _
'        ExactNames(3, NameCounter) & " " & _
'        ExactNames(4, NameCounter) & " " & _
'        ExactNames(5, NameCounter) & " "
'        If SSNs(NameCounter).CheckArrayInitialized = True _
'        Then
'            For SSNCounter = 0 To UBound(SSNs(NameCounter).SSN)
'                Debug.Print SSNs(NameCounter).SSN(SSNCounter) & " " & _
'                SSNs(NameCounter).AmbiguousSSN(SSNCounter) & " " & _
'                SSNs(NameCounter).SSNCount(SSNCounter) & " "
'            Next
'        End If
'    Debug.Print
'    Next
'    Debug.Print
                    
End Sub

Sub ExactNamesCountSpecial(ByVal TaskStartRow As Integer, ByVal TaskEndRow As Integer)

    Dim ExactFirstName As String
    Dim ExactLastName As String
    Dim ExactMidInitial As String
    Dim NameCounter As Integer
    Dim RowCounter As Integer
    Dim SSNCounter As Integer
    Dim AmbiguousSSN As Boolean
    Dim SSNFound As Boolean
    
    ReDim ExactNamesSpecial(0 To 5, 0 To 0)
    ExactFirstName = Cells(TaskStartRow, FirstNameCol).Value2
    ExactLastName = Cells(TaskStartRow, LastNameCol).Value2
    ExactNamesSpecial(0, 0) = FormatName(ExactFirstName)
    ExactNamesSpecial(1, 0) = FormatName(ExactLastName)
    ExactNamesSpecial(3, 0) = "1"
    ExactNamesSpecial(4, 0) = "0"
    ExactNamesSpecial(5, 0) = "0"
    If Cells(TaskStartRow, GenderCol).Value2 = "F" Then ExactNamesSpecial(4, 0) = "1"
    If Cells(TaskStartRow, GenderCol).Value2 = "M" Then ExactNamesSpecial(5, 0) = "1"
    ReDim SSNsSpecial(0 To 0)
    AmbiguousSSN = AmbiguousSSNCheck(Cells(TaskStartRow, SSNCol).Value2)
    If AmbiguousSSN = False _
    Then
        SSNsSpecial(0).SSN = Cells(TaskStartRow, SSNCol).Value2
        SSNsSpecial(0).AmbiguousSSN = AmbiguousSSN
        SSNsSpecial(0).SSNCount = 1
    End If
    ReDim SubscriberIDsSpecial(0 To 0)
    AmbiguousSSN = AmbiguousSSNCheck(Cells(TaskStartRow, SubscriberIDCol).Value2)
    If AmbiguousSSN = False _
    Then
        SubscriberIDsSpecial(0).SSN = Cells(TaskStartRow, SubscriberIDCol).Value2
        SubscriberIDsSpecial(0).AmbiguousSSN = AmbiguousSSN
    End If
    For RowCounter = TaskStartRow + 1 To TaskEndRow
        ExactFirstName = Cells(RowCounter, FirstNameCol).Value2
        ExactLastName = Cells(RowCounter, LastNameCol).Value2
        For NameCounter = 0 To UBound(ExactNamesSpecial, 2)
            If ExactNamesSpecial(0, NameCounter) = FormatName(ExactFirstName) _
            And ExactNamesSpecial(1, NameCounter) = FormatName(ExactLastName) _
            Then
                ExactNamesSpecial(3, NameCounter) = CStr(CInt(ExactNamesSpecial(3, NameCounter)) + 1)
                If Cells(RowCounter, GenderCol).Value2 = "F" Then ExactNamesSpecial(4, NameCounter) = CStr(CInt(ExactNamesSpecial(4, NameCounter)) + 1)
                If Cells(RowCounter, GenderCol).Value2 = "M" Then ExactNamesSpecial(5, NameCounter) = CStr(CInt(ExactNamesSpecial(5, NameCounter)) + 1)
                SSNFound = False
                If SSNsSpecial(NameCounter).CheckArrayInitialized = True _
                Then
                    For SSNCounter = 0 To UBound(SSNsSpecial(NameCounter).SSN)
                        If Cells(RowCounter, SSNCol).Value2 = SSNsSpecial(NameCounter).SSN(SSNCounter) _
                        Then
                            SSNFound = True
                            Exit For
                        End If
                    Next
                End If
                If SSNFound = True _
                Then
                    SSNsSpecial(NameCounter).IncreaseSSNCount SSNCounter
                ElseIf SSNFound = False _
                Then
                    AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SSNCol).Value2)
                    If AmbiguousSSN = False _
                    Then
                        SSNsSpecial(NameCounter).SSN = Cells(RowCounter, SSNCol).Value2
                        SSNsSpecial(NameCounter).AmbiguousSSN = AmbiguousSSN
                        SSNsSpecial(NameCounter).SSNCount = 1
                    End If
                End If
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SubscriberIDCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SubscriberIDsSpecial(NameCounter).SSN = Cells(RowCounter, SubscriberIDCol).Value2
                    SubscriberIDsSpecial(NameCounter).AmbiguousSSN = AmbiguousSSN
                End If
                Exit For
            End If
            If NameCounter = UBound(ExactNamesSpecial, 2) _
            Then
                ReDim Preserve ExactNamesSpecial(0 To 5, 0 To UBound(ExactNamesSpecial, 2) + 1)
                ExactNamesSpecial(0, UBound(ExactNamesSpecial, 2)) = FormatName(ExactFirstName)
                ExactNamesSpecial(1, UBound(ExactNamesSpecial, 2)) = FormatName(ExactLastName)
                ExactNamesSpecial(3, UBound(ExactNamesSpecial, 2)) = "1"
                ExactNamesSpecial(4, UBound(ExactNamesSpecial, 2)) = "0"
                ExactNamesSpecial(5, UBound(ExactNamesSpecial, 2)) = "0"
                If Cells(RowCounter, GenderCol).Value2 = "F" Then ExactNamesSpecial(4, UBound(ExactNamesSpecial, 2)) = "1"
                If Cells(RowCounter, GenderCol).Value2 = "M" Then ExactNamesSpecial(5, UBound(ExactNamesSpecial, 2)) = "1"
                ReDim Preserve SSNsSpecial(0 To UBound(SSNsSpecial) + 1)
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SSNCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SSNsSpecial(UBound(SSNsSpecial)).SSN = Cells(RowCounter, SSNCol).Value2
                    SSNsSpecial(UBound(SSNsSpecial)).AmbiguousSSN = AmbiguousSSN
                    SSNsSpecial(UBound(SSNsSpecial)).SSNCount = 1
                End If
                ReDim Preserve SubscriberIDsSpecial(0 To UBound(SubscriberIDsSpecial) + 1)
                AmbiguousSSN = AmbiguousSSNCheck(Cells(RowCounter, SubscriberIDCol).Value2)
                If AmbiguousSSN = False _
                Then
                    SubscriberIDsSpecial(UBound(SubscriberIDsSpecial)).SSN = Cells(RowCounter, SubscriberIDCol).Value2
                    SubscriberIDsSpecial(UBound(SubscriberIDsSpecial)).AmbiguousSSN = AmbiguousSSN
                End If
            End If
        Next
    Next
    
'    For NameCounter = 0 To UBound(ExactNamesSpecial, 2)
'        Debug.Print ExactNamesSpecial(0, NameCounter) & " " & _
'        ExactNamesSpecial(1, NameCounter) & " " & _
'        ExactNamesSpecial(3, NameCounter) & " " & _
'        ExactNamesSpecial(4, NameCounter) & " " & _
'        ExactNamesSpecial(5, NameCounter) & " "
'        If SSNsSpecial(NameCounter).CheckArrayInitialized = True _
'        Then
'            For SSNCounter = 0 To UBound(SSNsSpecial(NameCounter).SSN)
'                Debug.Print SSNsSpecial(NameCounter).SSN(SSNCounter) & " " & _
'                SSNsSpecial(NameCounter).AmbiguousSSN(SSNCounter) & " " & _
'                SSNsSpecial(NameCounter).SSNCount(SSNCounter) & " "
'            Next
'        End If
'        Debug.Print
'        If SubscriberIDsSpecial(NameCounter).CheckArrayInitialized = True _
'        Then
'            For SSNCounter = 0 To UBound(SubscriberIDsSpecial(NameCounter).SSN)
'                Debug.Print SubscriberIDsSpecial(NameCounter).SSN(SSNCounter) & " " & _
'                SubscriberIDsSpecial(NameCounter).AmbiguousSSN(SSNCounter)
'            Next
'        End If
'    Debug.Print
'    Next
'    Debug.Print

End Sub

Sub ExactDatesCount(ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim HighestTie As Boolean
    Dim ExactBirthDate As Date
    Dim NameCounter As Integer
    Dim RowCounter As Integer
    Dim HighCounter As Integer
    
    ReDim ExactDates(0 To 1, 0 To 0)
    ExactBirthDate = Cells(StartRow, BirthDateCol).Value2
    ExactDates(0, 0) = ExactBirthDate
    ExactDates(1, 0) = 1
    For RowCounter = StartRow + 1 To EndRow
        If IsDate(Cells(RowCounter, BirthDateCol).Value2) = True _
        Then
            ExactBirthDate = Cells(RowCounter, BirthDateCol).Value2
            For NameCounter = 0 To UBound(ExactDates, 2)
                If ExactBirthDate = CDate(ExactDates(0, NameCounter)) _
                Then
                    ExactDates(1, NameCounter) = CInt(ExactDates(1, NameCounter)) + 1
                    Exit For
                End If
            Next
            If NameCounter - 1 = UBound(ExactDates, 2) _
            Then
                ReDim Preserve ExactDates(0 To 1, 0 To UBound(ExactDates, 2) + 1)
                ExactDates(0, UBound(ExactDates, 2)) = ExactBirthDate
                ExactDates(1, UBound(ExactDates, 2)) = 1
            End If
        End If
    Next
    HighCounter = 0
    For NameCounter = 0 To UBound(ExactDates, 2)
        If ExactDates(1, NameCounter) > HighCounter _
        Then
            HighCounter = ExactDates(1, NameCounter)
            MostCommonDate = ExactDates(0, NameCounter)
            HighestTie = False
        ElseIf ExactDates(1, NameCounter) = HighCounter _
        Then
            HighestTie = True
            MostCommonDate = Empty
        End If
    Next

'    For NameCounter = 0 To UBound(ExactDates, 2)
'        Debug.Print ExactDates(0, NameCounter)
'        Debug.Print ExactDates(1, NameCounter)
'    Next

End Sub

Function AmbiguousFNCheck(ByRef FirstNames() As Variant, ByRef AmbiguousFNs() As Boolean) As Boolean

    Dim NameCounter As Integer

    AmbiguousFNCheck = False
    HardAmbiguousFN = False
    ReDim AmbiguousFNs(0 To UBound(FirstNames)) As Boolean
    ReDim SoftAmbiguousFN(0 To UBound(FirstNames)) As Boolean
    
    For NameCounter = 0 To UBound(AmbiguousFNs)
        AmbiguousFNs(NameCounter) = False
    Next
    
    For NameCounter = 0 To UBound(FirstNames)
        If (IsInArray(FirstNames(NameCounter), HardAmbiguousNames1, "Inside", "RightInLeft") = True _
        And IsInArray(FirstNames(NameCounter), AmbiguousExclusions, "Inside", "RightInLeft") = False) _
        Or IsInArray(FirstNames(NameCounter), HardAmbiguousNames2, "Exact") = True _
        Or InStr(FirstName, "ESTOF") <> 0 _
        Or InStr(FirstName, "FINDME") <> 0 _
        Or InStr(FirstName, "DONOTUSE") <> 0 _
        Or InStr(FirstName, "NONAMEGIVE") <> 0 _
        Or Replace(FirstNameExact, " ", "") + LastName = "DONOTUSE" _
        Or Replace(FirstNameExact, " ", "") + LastName = "NONAMEGIVE" _
        Or Replace(FirstNameExact, " ", "") + LastName = "NONAMEGIVEN" _
        Or Left(FirstNameExact, 2) = "G-" _
        Or Left(FirstNameExact, 2) = "B-" _
        Or Left(FirstNames(NameCounter), 2) = "BB" _
        Or Left(FirstNames(NameCounter), 2) = "BG" _
        Or Left(FirstNames(NameCounter), 2) = "NB" _
        Or Left(FirstNames(NameCounter), 2) = "TW" _
        Or Left(FirstNames(NameCounter), 2) = "NP" _
        Or Left(FirstNames(NameCounter), 2) = "FC" _
        Or Left(FirstNames(NameCounter), 2) = "BM" _
        Or Left(FirstNames(NameCounter), 2) = "BF" _
        Or Left(FirstNames(NameCounter), 2) = "NN" _
        Then
            AmbiguousFNCheck = True
            HardAmbiguousFN = True
            Exit For
        End If
        If Len(FirstNames(NameCounter)) < 3 _
        Then
            If IsInArray(FirstNames(NameCounter), ShortNames, "Exact") = False _
            And IsInArray(FirstNames(NameCounter), DiminutiveShortNames, "Exact") = False _
            Then AmbiguousFNs(NameCounter) = True
        ElseIf IsInArray(FirstNames(NameCounter), SoftAmbiguousNames1, "Inside", "RightInLeft") = True _
        And IsInArray(FirstNames(NameCounter), AmbiguousExclusions, "Inside", "RightInLeft") = False _
        Then
            AmbiguousFNs(NameCounter) = True
        ElseIf IsInArray(FirstNames(NameCounter), SoftAmbiguousNames2, "Exact") = True _
        Then
            AmbiguousFNs(NameCounter) = True
        End If
    Next
    
    If AmbiguousFNCheck = True _
    Then
        For NameCounter = 1 To UBound(AmbiguousFNs)
            AmbiguousFNs(NameCounter) = True
        Next
    End If
    
    If IsInArray(False, AmbiguousFNs) = False _
    Then AmbiguousFNCheck = True

End Function

Function GenderCheck(ByVal PersonCounter As Integer) As Boolean

    Dim GenderPercent As Double
    Dim GenderPercentCompare As Double
    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    
    GenderPercent = -1
    GenderPercentCompare = -1

    For NameCounter = 0 To UBound(ExactNamesSpecial, 2)
        If Replace(ExactNamesSpecial(0, NameCounter), " ", "") = FirstName _
        And Replace(ExactNamesSpecial(1, NameCounter), " ", "") = LastName _
        Then
            If CInt(ExactNamesSpecial(4, NameCounter)) = 0 _
            And CInt(ExactNamesSpecial(5, NameCounter)) = 0 _
            Then
                GenderPercent = -1
            ElseIf CInt(ExactNamesSpecial(5, NameCounter)) = 0 _
            Then
                GenderPercent = 0
            Else
                GenderPercent = CInt(ExactNamesSpecial(5, NameCounter)) / (CInt(ExactNamesSpecial(4, NameCounter)) + CInt(ExactNamesSpecial(5, NameCounter)))
            End If
            Exit For
        End If
    Next

    For NameCounterAgain = 0 To UBound(ExactNamesSpecial, 2)
        If IsInArray(Replace(ExactNamesSpecial(0, NameCounterAgain), " ", ""), People(PersonCounter).FirstNameFull, "Exact") = True _
        And IsInArray(Replace(ExactNamesSpecial(1, NameCounterAgain), " ", ""), People(PersonCounter).LastNameFull, "Exact") = True _
        Then
            If CInt(ExactNamesSpecial(4, NameCounterAgain)) = 0 _
            And CInt(ExactNamesSpecial(5, NameCounterAgain)) = 0 _
            Then
                GenderPercentCompare = -1
            ElseIf CInt(ExactNamesSpecial(5, NameCounterAgain)) = 0 _
            Then
                GenderPercentCompare = 0
            Else
                GenderPercentCompare = CInt(ExactNamesSpecial(5, NameCounterAgain)) / (CInt(ExactNamesSpecial(4, NameCounterAgain)) + CInt(ExactNamesSpecial(5, NameCounterAgain)))
            End If
            Exit For
        End If
    Next

    If ((Round(GenderPercent, 3) >= 0.667 And Round(GenderPercent, 3) <= 1) _
    And (Round(GenderPercentCompare, 3) <= 0.333 And Round(GenderPercentCompare, 3) >= 0)) _
    Or ((Round(GenderPercent, 3) <= 0.333 And Round(GenderPercent, 3) >= 0) _
    And (Round(GenderPercentCompare, 3) >= 0.667 And Round(GenderPercentCompare, 3) <= 1)) _
    Then
        If Replace(ExactNamesSpecial(0, NameCounter), " ", "") <> Replace(ExactNamesSpecial(0, NameCounterAgain), " ", "") _
        And FNExactMatch = False _
        Then
            TwinsGenderDiffers = True
            GenderCheck = TwinsGenderDiffers
            Exit Function
        End If
    End If

End Function

Function SSNCheck(ByVal PersonCounter As Integer, ByVal FalseNegativeCheck As Boolean) As Boolean

    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim NameCounterNext As Integer
    Dim HighCounter As Integer
    Dim SSNCounter As Integer
    Dim FirstPrimarySSN As String
    Dim SecondPrimarySSN As String
    Dim SSNSubscriberMatch As Boolean
    
    For NameCounterAgain = 0 To UBound(ExactNamesSpecial, 2)
        If FirstName = Replace(ExactNamesSpecial(0, NameCounterAgain), " ", "") _
        And LastName = Replace(ExactNamesSpecial(1, NameCounterAgain), " ", "") _
        Then
            For NameCounterNext = 0 To UBound(ExactNamesSpecial, 2)
                If IsInArray(Replace(ExactNamesSpecial(0, NameCounterNext), " ", ""), People(PersonCounter).FirstNameFull, "Exact") = True _
                And IsInArray(Replace(ExactNamesSpecial(1, NameCounterNext), " ", ""), People(PersonCounter).LastNameFull, "Exact") = True _
                Then
                    If NameCounterAgain <> NameCounterNext _
                    Then
                        If SSNsSpecial(NameCounterAgain).CheckArrayInitialized = True _
                        And SSNsSpecial(NameCounterNext).CheckArrayInitialized = True _
                        Then
                            HighCounter = 0
                            For SSNCounter = 0 To UBound(SSNsSpecial(NameCounterAgain).SSN)
                                If SSNsSpecial(NameCounterAgain).SSNCount(SSNCounter) > HighCounter _
                                Then
                                    FirstPrimarySSN = SSNsSpecial(NameCounterAgain).SSN(SSNCounter)
                                    HighCounter = SSNsSpecial(NameCounterAgain).SSNCount(SSNCounter)
                                End If
                            Next
                            HighCounter = 0
                            For SSNCounter = 0 To UBound(SSNsSpecial(NameCounterNext).SSN)
                                If SSNsSpecial(NameCounterNext).SSNCount(SSNCounter) > HighCounter _
                                Then
                                    SecondPrimarySSN = SSNsSpecial(NameCounterNext).SSN(SSNCounter)
                                    HighCounter = SSNsSpecial(NameCounterNext).SSNCount(SSNCounter)
                                End If
                            Next
                            If IsInArray(FirstPrimarySSN, SSNsSpecial(NameCounterNext).SSN, "Exact") = False _
                            And IsInArray(SecondPrimarySSN, SSNsSpecial(NameCounterAgain).SSN, "Exact") = False _
                            Then
                                If CLng(BirthDate) < 40718 _
                                Then
                                    If CInt(Right(FirstPrimarySSN, 1)) - CInt(Right(SecondPrimarySSN, 1)) < 3 _
                                    Or CInt(Right(SecondPrimarySSN, 1)) - CInt(Right(FirstPrimarySSN, 1)) < 3 _
                                    Or WeightedDL(FirstPrimarySSN, SecondPrimarySSN, 2, 2, 1, 1) > 1 _
                                    Then
                                        If SubscriberIDsSpecial(NameCounterNext).CheckArrayInitialized = True _
                                        Then
                                            SSNSubscriberMatch = False
                                            For SSNCounter = 0 To UBound(SubscriberIDsSpecial(NameCounterNext).SSN)
                                                If FirstPrimarySSN = SubscriberIDsSpecial(NameCounterNext).SSN(SSNCounter) _
                                                Then
                                                    SSNSubscriberMatch = True
                                                    Exit For
                                                End If
                                            Next
                                            If SSNSubscriberMatch = False _
                                            Then
                                                If MCID <> People(PersonCounter).MCID _
                                                Then SSNMatch = False
                                                SSNCheck = True
                                                Exit Function
                                            End If
                                        ElseIf SubscriberIDsSpecial(NameCounterAgain).CheckArrayInitialized = True _
                                        Then
                                            SSNSubscriberMatch = False
                                            For SSNCounter = 0 To UBound(SubscriberIDsSpecial(NameCounterAgain).SSN)
                                                If SecondPrimarySSN = SubscriberIDsSpecial(NameCounterAgain).SSN(SSNCounter) _
                                                Then
                                                    SSNSubscriberMatch = True
                                                    Exit For
                                                End If
                                            Next
                                            If SSNSubscriberMatch = False _
                                            Then
                                                If MCID <> People(PersonCounter).MCID _
                                                Then SSNMatch = False
                                                SSNCheck = True
                                                Exit Function
                                            End If
                                        Else
                                            If MCID <> People(PersonCounter).MCID _
                                            And FirstPrimarySSN <> vbNullString _
                                            And SecondPrimarySSN <> vbNullString _
                                            Then SSNMatch = True
                                            Exit Function
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            If MCID <> People(PersonCounter).MCID Then SSNMissing = True
                        End If
                    End If
                End If
            Next
        End If
    Next

End Function

Function AmbiguousSSNCheck(ByVal ExactSSN As Variant) As Boolean

    AmbiguousSSNCheck = False

    If ExactSSN = vbNullString _
    Or ExactSSN = "NA" _
    Or ExactSSN = "000000000" _
    Or ExactSSN = "999999999" _
    Or ExactSSN = "?" _
    Or ExactSSN = "0" _
    Then AmbiguousSSNCheck = True
    
    If Len(ExactSSN) < 7 _
    Or Left(ExactSSN, 3) = "666" _
    Or (Len(ExactSSN) = 9 _
    And (Left(ExactSSN, 1) = "9" _
    Or Mid(ExactSSN, 4, 2) = "00" _
    Or Mid(ExactSSN, 6, 4) = "0000")) _
    Or (Len(ExactSSN) = 8 _
    And (Mid(ExactSSN, 3, 2) = "00" _
    Or Mid(ExactSSN, 5, 4) = "0000")) _
    Or IsNumeric(ExactSSN) = False _
    Then AmbiguousSSNCheck = True

End Function

Function FirstNameCheck(ByRef FirstNames() As Variant, ByRef AmbiguousFNs() As Boolean, ByVal PersonCounter As Integer, ByVal FalseNegativeCheck As Boolean) As Boolean

    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim NameCompareCounter As Integer
    Dim NameCompareCounterAgain As Integer
    Dim MetaphoneCounter As Integer
    Dim MetaphoneCompareCounter As Integer
    Dim DeleteCounter As Integer
    Dim EditDistance As Double
    Dim HypocorismEntry() As String
    Dim MetaphoneKey As String
    Dim MetaphoneKeys() As String
    Dim MetaphoneCompareKey As String
    Dim MetaphoneCompareKeys() As String
    Dim AverageNameLength As Double
    Dim MetaphoneGender As Double
    Dim MetaphoneCompareGender As Double
    Dim GenderDiffers As Boolean
    Dim SSNDiffers As Boolean

    FirstNameCheck = True
    FNExactMatch = False
    If IsInArray(FirstName, People(PersonCounter).FirstNameFull, "Exact") = True _
    Then FNExactMatch = True

    If (AmbiguousFN = True Or People(PersonCounter).AmbiguousFNFull = True) _
    Then
        AmbiguousFirst = True
        If (IsInEachArray(FirstNames, People(PersonCounter).FirstName, "LeftInside", "Either", True, "LeftEqualsRight", , , , , 2) = True _
        Or IsInArray(FirstName, People(PersonCounter).FirstNameFull, "Exact") = True) _
        And (AmbiguousFN = True And People(PersonCounter).AmbiguousFNFull = True) _
        Then
            FirstNameCheck = False
            If AmbiguousFN = True And People(PersonCounter).AmbiguousFNFull = True _
            And IsInArray(SourceKey, People(PersonCounter).SourceKey, "Exact") = True _
            Then AmbiguousFirst = False
        End If
        SSNDiffers = SSNCheck(PersonCounter, FalseNegativeCheck)
        Exit Function
    End If
    
    If FNExactMatch = True _
    Then
        FirstNameCheck = False
        RequiresManualCheck = False
        If MCID <> People(PersonCounter).MCID Then FNMatch = True
        Exit Function
    End If
    
    If UBound(ExactNamesSpecial, 2) > 0 _
    Then
        GenderDiffers = GenderCheck(PersonCounter)
        If GenderDiffers = True Then Exit Function
        SSNDiffers = SSNCheck(PersonCounter, FalseNegativeCheck)
        If SSNDiffers = True Then Exit Function
    End If

    For NameCounterAgain = 0 To UBound(ExactNames, 2)

        ExactFirstNames = SplitName(ExactNames(0, NameCounterAgain))
        ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
        AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
        ExactMidInitials = SplitName(ExactNames(2, NameCounterAgain))

        If IsInEachArray(FirstNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousFNs, AmbiguousExactFNs) = True _
        And IsInEachArray(People(PersonCounter).FirstName, ExactFirstNames, "LeftInside", "Either", , , People(PersonCounter).AmbiguousFN, AmbiguousExactFNs) = True _
        Then

            FirstNameCheck = False
            If MCID <> People(PersonCounter).MCID Then FNMatch = True
            
            If (UBound(ExactFirstNames) + 1) >= (UBound(FirstNames) + 1) + (UBound(MidInitials) + 1) _
            Or MidInitials(0) = vbNullString _
            Then
                For NameCounter = 0 To UBound(MidInitials)
                    If IsInArray(MidInitials(NameCounter), ExactFirstNames, "Left", , , , AmbiguousExactFNs) = True _
                    And FirstName <> Replace(ExactNames(0, NameCounterAgain), " ", "") _
                    And (UBound(MidInitials) > 0 _
                    Or MidInitials(0) <> vbNullString) _
                    Then
                        MidInitials(NameCounter) = vbNullString
                        If UBound(MidInitials) > 0 _
                        Then
                            For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                                MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                            Next
                            ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                        End If
                        Exit Function
                    End If
                Next
                Exit Function
            Else
                For NameCounter = 0 To UBound(FirstNames)
                    If IsInArray(FirstNames(NameCounter), ExactMidInitials, "Left") = True _
                    And FirstName <> Replace(ExactNames(0, NameCounterAgain), " ", "") _
                    And UBound(FirstNames) > 0 _
                    Then
                        If MidInitials(0) = vbNullString _
                        Then
                            MidInitials(0) = Left(FirstNames(NameCounter), 1)
                        Else
                            ReDim Preserve MidInitials(0 To UBound(MidInitials) + 1)
                            MidInitials(UBound(MidInitials)) = Left(FirstNames(NameCounter), 1)
                            For DeleteCounter = NameCounter To UBound(FirstNames) - 1
                                FirstNames(DeleteCounter) = FirstNames(DeleteCounter + 1)
                            Next
                            ReDim Preserve FirstNames(UBound(FirstNames) - 1)
                            ReDim Preserve AmbiguousFNs(UBound(AmbiguousFNs) - 1)
                        End If
                        Exit Function
                    End If
                Next
                Exit Function
            End If
        End If
    Next

'   Compare first name(s) in current record against first name(s) in person in array
    If IsInEachArray(FirstNames, People(PersonCounter).FirstName, "Inside", , , , AmbiguousFNs, People(PersonCounter).AmbiguousFN) = True _
    Then
        FirstNameCheck = False
        If MCID <> People(PersonCounter).MCID Then FNMatch = True
        For NameCounter = 0 To UBound(MidInitials)
            If IsInArray(MidInitials(NameCounter), People(PersonCounter).FirstName, "Left") = True _
            And IsInArray(MidInitials(NameCounter), FirstNames, "Left") = False _
            And UBound(People(PersonCounter).FirstName) > 0 _
            Then
                MidInitials(NameCounter) = vbNullString
                If UBound(MidInitials) > 0 _
                Then
                    For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                        MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                    Next
                    ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                End If
                Exit Function
            End If
        Next
        Exit Function
    End If
    
'   If first name not found then run weighted Damerau-Levenshtein algorithm
    For NameCounter = 0 To UBound(FirstNames)
        For NameCompareCounter = 0 To UBound(People(PersonCounter).FirstName)
            If AmbiguousFNs(NameCounter) = False And People(PersonCounter).AmbiguousFN(NameCompareCounter) = False Then
                EditDistance = WeightedDL(CStr(FirstNames(NameCounter)), CStr(People(PersonCounter).FirstName(NameCompareCounter)), 1, 0.5, 1.5, 0.5)
                AverageNameLength = (Len(FirstNames(NameCounter)) + Len(People(PersonCounter).FirstName(NameCompareCounter))) / 2
                If TwinsRule = False _
                Then
                    If EditDistance <= 2 _
                    Then
                        FirstNameCheck = False
                        If MCID <> People(PersonCounter).MCID Then FNTypo = True
                        Exit Function
                    End If
                ElseIf TwinsRule = True _
                Then
                    If EditDistance <= 1 _
                    Then
                        FirstNameCheck = False
                        If MCID <> People(PersonCounter).MCID Then FNTypo = True
                        Exit Function
                    End If
                End If
            End If
        Next
    Next

    For NameCounter = 0 To UBound(FirstNames)
        MetaphoneKey = DoubleMetaphone(FirstNames(NameCounter), 10)
        MetaphoneKey = Application.WorksheetFunction.Trim(MetaphoneKey)
        MetaphoneKeys = Split(MetaphoneKey)
        For NameCompareCounter = 0 To UBound(People(PersonCounter).FirstName)
            AverageNameLength = (Len(FirstNames(NameCounter)) + Len(People(PersonCounter).FirstName(NameCompareCounter))) / 2
            If EditDistance <= AverageNameLength / 3 _
            Then
                MetaphoneCompareKey = DoubleMetaphone(CStr(People(PersonCounter).FirstName(NameCompareCounter)), 10)
                MetaphoneCompareKey = Application.WorksheetFunction.Trim(MetaphoneCompareKey)
                MetaphoneCompareKeys = Split(MetaphoneCompareKey)
                For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
                    For MetaphoneCompareCounter = 0 To UBound(MetaphoneCompareKeys)
                        If MetaphoneKeys(MetaphoneCounter) = MetaphoneCompareKeys(MetaphoneCompareCounter) _
                        Then
                            FirstNameCheck = False
                            If MCID <> People(PersonCounter).MCID Then FNTypo = True
                            Exit Function
                        End If
                    Next
                Next
            End If
        Next
    Next

    For NameCounter = 0 To UBound(FirstNames)
        For NameCompareCounter = 0 To UBound(People(PersonCounter).FirstName)
            If AmbiguousFNs(NameCounter) = False And People(PersonCounter).AmbiguousFN(NameCompareCounter) = False _
            Then
                If HypocorismCheck(FirstNames(NameCounter), CStr(People(PersonCounter).FirstName(NameCompareCounter))) = True _
                Then
                    FirstNameCheck = False
                    If MCID <> People(PersonCounter).MCID Then FNNickname = True
                    Exit Function

'                Else
'                    ArrayPlace = IsInArray2DString(FirstNames(NameCounter), NicknameDict, 1)
'                    If ArrayPlace <> Null Then
'                        If WeightedDL(NicknameDict(ArrayPlace, 2), CStr(People(PersonCounter).FirstName(NameCompareCounter))) <= 1 _
'                        Then
'                            FirstNameCheck = False
'                            Exit Function
'                        End If
'                    End If

                End If
            End If
        Next
    Next

    For NameCounter = 0 To UBound(FirstNames)
        If Hypocorisms.Exists(FirstNames(NameCounter)) = True _
        Then
            HypocorismEntry() = Split(UCase(Hypocorisms(FirstNames(NameCounter))), ", ")
            If IsInEachArray(People(PersonCounter).FirstName, HypocorismEntry, "Exact") = True _
            Then
                FirstNameCheck = False
                If MCID <> People(PersonCounter).MCID Then FNNickname = True
                Exit Function
            End If
        End If
    Next

    For NameCounter = 0 To UBound(People(PersonCounter).FirstName)
        If Hypocorisms.Exists(People(PersonCounter).FirstName(NameCounter)) = True _
        Then
            HypocorismEntry() = Split(UCase(Hypocorisms(People(PersonCounter).FirstName(NameCounter))), ", ")
            If IsInEachArray(FirstNames, HypocorismEntry, "Exact") = True _
            Then
                FirstNameCheck = False
                If MCID <> People(PersonCounter).MCID Then FNNickname = True
                Exit Function
            End If
        End If
    Next

'   Grand finale
    For NameCounter = 0 To UBound(FirstNames)
        If Hypocorisms.Exists(FirstNames(NameCounter)) = True _
        Then
            HypocorismEntry() = Split(UCase(Hypocorisms(FirstNames(NameCounter))), ", ")
            For NameCompareCounter = 0 To UBound(People(PersonCounter).FirstName)
                For NameCompareCounterAgain = 0 To UBound(HypocorismEntry)
                    EditDistance = WeightedDL(HypocorismEntry(NameCompareCounterAgain), CStr(People(PersonCounter).FirstName(NameCompareCounter)), 1, 0.5, 1.5, 0.5)
                    If EditDistance <= 1 Then
                        FirstNameCheck = False
                        If MCID <> People(PersonCounter).MCID Then FNNickname = True
                        Exit Function
                    End If
                Next
            Next
        End If
    Next
        
''       First Name Check 1
'        For NameCounter = LBound(FirstNames) To UBound(FirstNames)
'            For NameCounterAgain = LBound(ExactNames, 2) To UBound(ExactNames, 2)
'                If IsInArrayStringRev(ExactNames(1, NameCounterAgain), FirstNames) = True _
'                Then
'                    ExactFirstNames = SplitName(ExactNames(1, NameCounterAgain))
'                    ReDim AmbiguousExactFNs(LBound(ExactFirstNames) To UBound(ExactFirstNames))
'                    AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
'                    If IsInArrayLeftInStringAmbi(FirstNames(NameCounter), ExactFirstNames, AmbiguousFNs(NameCounter), AmbiguousExactFNs) = True _
'                    And IsInArrayLeftInArrayAmbi(People(Counter).FirstName, ExactFirstNames, People(Counter).AmbiguousFN, AmbiguousExactFNs) = True _
'                    Then
'                        FirstName1Found(NameCounter) = True
'                        Exit For
'                    End If
'                End If
'                If FirstName1Found(NameCounter) = True Then Exit For
'            Next
'            PhoneticMatch = False
'            For NameCompareCounter = 0 To UBound(People(Counter).FirstName)
'                If AmbiguousFNs(NameCounter) = False And People(Counter).AmbiguousFN(NameCompareCounter) = False _
'                Then
'                    If HypocorismCheck(FirstNames(NameCounter), CStr(People(Counter).FirstName(NameCompareCounter))) = True _
'                    Then
'                        FirstName1Found(NameCounter) = True
'                        Exit For
'                    End If
'                    EditDistance = WeightedDL(CStr(FirstNames(NameCounter)), CStr(People(Counter).FirstName(NameCompareCounter)), 1, 0.5, 1.5, 0.5)
'                    If EditDistance <= 1 _
'                    Then
'                        FirstName1Found(NameCounter) = True
'                        Exit For
'                    End If
'                    If Len(CStr(FirstNames(NameCounter))) < Len(CStr(People(Counter).FirstName(NameCompareCounter))) _
'                    Then
'                        EditDistance = WeightedDL(CStr(FirstNames(NameCounter)), Left(CStr(People(Counter).FirstName(NameCompareCounter)), Len(FirstNames(NameCounter))), 1, 0.5, 1.5, 0.5)
'                    ElseIf Len(CStr(FirstNames(NameCounter))) > Len(CStr(People(Counter).FirstName(NameCompareCounter))) _
'                    Then
'                        EditDistance = WeightedDL(CStr(People(Counter).FirstName(NameCompareCounter)), Left(CStr(FirstNames(NameCounter)), Len(CStr(People(Counter).FirstName(NameCompareCounter)))), 1, 0.5, 1.5, 0.5)
'                    End If
'                    If EditDistance <= 1.5 _
'                    Then
'                        FirstName1Found(NameCounter) = True
'                        Exit For
'                    End If
'                    MetaphoneKey = DoubleMetaphone(FirstNames(NameCounter), 10)
'                    MetaphoneKey = Application.WorksheetFunction.Trim(MetaphoneKey)
'                    MetaphoneKeys = Split(MetaphoneKey)
'                    MetaphoneCompareKey = DoubleMetaphone(CStr(People(Counter).FirstName(NameCompareCounter)), 10)
'                    MetaphoneCompareKey = Application.WorksheetFunction.Trim(MetaphoneCompareKey)
'                    MetaphoneCompareKeys = Split(MetaphoneCompareKey)
'                    AverageNameLength = (Len(FirstNames(NameCounter)) + Len(People(Counter).FirstName(NameCompareCounter))) / 2
'                    AverageKeyLength = 0
'                    For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
'                        AverageKeyLength = AverageKeyLength + Len(MetaphoneKeys(MetaphoneCounter))
'                    Next
'                    For MetaphoneCounter = 0 To UBound(MetaphoneCompareKeys)
'                        AverageKeyLength = AverageKeyLength + Len(MetaphoneCompareKeys(MetaphoneCounter))
'                    Next
'                    AverageKeyLength = AverageKeyLength / (UBound(MetaphoneKeys) + 1 + UBound(MetaphoneCompareKeys) + 1)
'                    For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
'                        For MetaphoneCompareCounter = 0 To UBound(MetaphoneCompareKeys)
'                            If (EditDistance <= AverageNameLength / 3 _
'                            Or AverageKeyLength * AverageKeyLength > AverageNameLength * 2) _
'                            And MetaphoneKeys(MetaphoneCounter) = MetaphoneCompareKeys(MetaphoneCompareCounter) _
'                            Then PhoneticMatch = True
'                        Next
'                    Next
'                    If PhoneticMatch = True _
'                    Then
'                        FirstName1Found(NameCounter) = True
'                        Exit For
'                    End If
'                End If
'            Next
'        Next
''       First Name Check 2
'        For NameCounter = 0 To UBound(People(Counter).FirstName)
'            For NameCounterAgain = LBound(ExactNames, 2) To UBound(ExactNames, 2)
'                If IsInArrayStringRev(ExactNames(1, NameCounterAgain), People(Counter).FirstName) = True _
'                Then
'                    ExactFirstNames = SplitName(ExactNames(1, NameCounterAgain))
'                    ReDim AmbiguousExactFNs(LBound(ExactFirstNames) To UBound(ExactFirstNames))
'                    AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
'                    If IsInArrayLeftInStringAmbi(People(Counter).FirstName(NameCounter), ExactFirstNames, People(Counter).AmbiguousFN(NameCounter), AmbiguousExactFNs) = True _
'                    And IsInArrayLeftInArrayAmbi(FirstNames, ExactFirstNames, AmbiguousFNs, AmbiguousExactFNs) = True _
'                    Then
'                        FirstName2Found(NameCounter) = True
'                        Exit For
'                    End If
'                End If
'                If FirstName2Found(NameCounter) = True Then Exit For
'            Next
'            PhoneticMatch = False
'            For NameCompareCounter = LBound(FirstNames) To UBound(FirstNames)
'                If People(Counter).AmbiguousFN(NameCounter) = False And AmbiguousFNs(NameCompareCounter) = False _
'                Then
'                    If HypocorismCheck(CStr(People(Counter).FirstName(NameCounter)), FirstNames(NameCompareCounter)) = True _
'                    Then
'                        FirstName2Found(NameCounter) = True
'                        Exit For
'                    End If
'                    EditDistance = WeightedDL(CStr(People(Counter).FirstName(NameCounter)), CStr(FirstNames(NameCompareCounter)), 1, 0.5, 1.5, 0.5)
'                    If EditDistance <= 1 _
'                    Then
'                        FirstName2Found(NameCounter) = True
'                        Exit For
'                    End If
'                    If Len(CStr(People(Counter).FirstName(NameCounter))) < Len(CStr(FirstNames(NameCompareCounter))) _
'                    Then
'                        EditDistanceAdjusted = WeightedDL(CStr(FirstNames(NameCompareCounter)), Left(CStr(People(Counter).FirstName(NameCounter)), Len(FirstNames(NameCompareCounter))), 1, 0.5, 1.5, 0.5)
'                    ElseIf Len(CStr(People(Counter).FirstName(NameCounter))) > Len(CStr(FirstNames(NameCompareCounter))) _
'                    Then
'                        EditDistanceAdjusted = WeightedDL(CStr(People(Counter).FirstName(NameCounter)), Left(CStr(FirstNames(NameCompareCounter)), Len(CStr(People(Counter).FirstName(NameCounter)))), 1, 0.5, 1.5, 0.5)
'                    End If
'                    If EditDistanceAdjusted <= 1.5 _
'                    Then
'                        FirstName2Found(NameCounter) = True
'                        Exit For
'                    End If
'                    MetaphoneKey = DoubleMetaphone(CStr(People(Counter).FirstName(NameCounter)), 10)
'                    MetaphoneKey = Application.WorksheetFunction.Trim(MetaphoneKey)
'                    MetaphoneKeys = Split(MetaphoneKey)
'                    MetaphoneCompareKey = DoubleMetaphone(FirstNames(NameCompareCounter), 10)
'                    MetaphoneCompareKey = Application.WorksheetFunction.Trim(MetaphoneCompareKey)
'                    MetaphoneCompareKeys = Split(MetaphoneCompareKey)
'                    AverageNameLength = (Len(FirstNames(NameCompareCounter)) + Len(People(Counter).FirstName(NameCounter))) / 2
'                    AverageKeyLength = 0
'                    For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
'                        AverageKeyLength = AverageKeyLength + Len(MetaphoneKeys(MetaphoneCounter))
'                    Next
'                    For MetaphoneCounter = 0 To UBound(MetaphoneCompareKeys)
'                        AverageKeyLength = AverageKeyLength + Len(MetaphoneCompareKeys(MetaphoneCounter))
'                    Next
'                    AverageKeyLength = AverageKeyLength / (UBound(MetaphoneKeys) + 1 + UBound(MetaphoneCompareKeys) + 1)
'                    For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
'                        For MetaphoneCompareCounter = 0 To UBound(MetaphoneCompareKeys)
'
'                            If (EditDistance <= AverageNameLength / 3 _
'                            Or AverageKeyLength * AverageKeyLength > AverageNameLength * 2) _
'                            And MetaphoneKeys(MetaphoneCounter) = MetaphoneCompareKeys(MetaphoneCompareCounter) _
'                            Then PhoneticMatch = True
'
'                        Next
'                    Next
'                    If PhoneticMatch = True _
'                    Then
'                        FirstName2Found(NameCounter) = True
'                        Exit For
'                    End If
'                End If
'            Next
'        Next
'        If IsInArrayFalse(FirstName1Found) = True _
'        And IsInArrayFalse(FirstName2Found) = True _
'        Then
'            FirstNameCheck = True
'        ElseIf IsInArrayFalse(FirstName1Found) = True _
'        Or IsInArrayFalse(FirstName2Found) = True _
'        Then
'            MIDiffers = True
'        End If

End Function

Function AmbiguousLNCheck(ByRef LastNames() As Variant, ByRef AmbiguousLNs() As Boolean) As Boolean

    Dim NameCounter As Integer
    
    AmbiguousLNCheck = False
    HardAmbiguousLN = False
    ReDim AmbiguousLNs(0 To UBound(LastNames)) As Boolean
    ReDim SoftAmbiguousLN(0 To UBound(LastNames)) As Boolean
    For NameCounter = 0 To UBound(AmbiguousLNs)
        AmbiguousLNs(NameCounter) = False
    Next
    If Len(LastName) = 0 _
    Then AmbiguousLNCheck = True
    For NameCounter = 0 To UBound(LastNames)
        If (IsInArray(LastNames(NameCounter), HardAmbiguousNames1, "Inside", "RightInLeft") = True _
        And IsInArray(LastNames(NameCounter), AmbiguousExclusions, "Inside", "RightInLeft") = False) _
        Or IsInArray(LastNames(NameCounter), HardAmbiguousNames2, "Exact") = True _
        Or InStr(LastName, "ESTOF") <> 0 _
        Or InStr(LastName, "FINDME") <> 0 _
        Or InStr(LastName, "DONOTUSE") <> 0 _
        Or InStr(LastName, "NONAMEGIVE") <> 0 _
        Or Replace(FirstNameExact, " ", "") & LastName = "DONOTUSE" _
        Or Replace(FirstNameExact, " ", "") & LastName = "NONAMEGIVE" _
        Or Replace(FirstNameExact, " ", "") & LastName = "NONAMEGIVEN" _
        Or Left(LastNames(NameCounter), 2) = "BB" _
        Or Left(LastNames(NameCounter), 2) = "BG" _
        Or Left(LastNames(NameCounter), 2) = "NB" _
        Or Left(LastNames(NameCounter), 2) = "NP" _
        Or Left(LastNames(NameCounter), 2) = "FC" _
        Or Left(LastNames(NameCounter), 2) = "BM" _
        Or Left(LastNames(NameCounter), 2) = "BF" _
        Or Left(LastNames(NameCounter), 2) = "NN" _
        Then
            AmbiguousLNCheck = True
            HardAmbiguousLN = True
            Exit For
        End If
        If Len(LastNames(NameCounter)) < 2 _
        Then AmbiguousLNs(NameCounter) = True
        If IsInArray(LastNames(NameCounter), SoftAmbiguousNames1, "Inside", "RightInLeft") = True _
        And IsInArray(LastNames(NameCounter), AmbiguousExclusions, "Inside", "RightInLeft") = False _
        Then
            AmbiguousLNs(NameCounter) = True
        ElseIf IsInArray(LastNames(NameCounter), SoftAmbiguousNames2, "Exact") = True _
        Then
            AmbiguousLNs(NameCounter) = True
        End If
    Next
    If AmbiguousLNCheck = True Then
        For NameCounter = 1 To UBound(AmbiguousLNs)
            AmbiguousLNs(NameCounter) = True
        Next
    End If
    If IsInArray(False, AmbiguousLNs) = False _
    Then AmbiguousLNCheck = True
'    If AmbiguousLNCheck = True Then AmbiguousLast = True
    
End Function

Function LastNameCheck(ByRef LastNames() As Variant, ByRef AmbiguousLNs() As Boolean, ByVal PersonCounter As Integer) As Boolean

    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim NameCompareCounter As Integer
    Dim DeleteCounter As Integer
    Dim MetaphoneCounter As Integer
    Dim MetaphoneCompareCounter As Integer
    Dim EditDistance As Double
    Dim MetaphoneKey As String
    Dim MetaphoneKeys() As String
    Dim MetaphoneCompareKey As String
    Dim MetaphoneCompareKeys() As String
    
    LastNameCheck = True
    LNExactMatch = False
    If IsInArray(LastName, People(PersonCounter).LastNameFull, "Exact") = True _
    Then LNExactMatch = True
    
    If (AmbiguousLN = True Or People(PersonCounter).AmbiguousLNFull = True) _
    Then
        AmbiguousLast = True
        If (IsInEachArray(LastNames, People(PersonCounter).LastName, "LeftInside", "Either", True, "LeftEqualsRight", , , , , 1) = True _
        Or IsInArray(LastName, People(PersonCounter).LastNameFull, "Exact") = True) _
        And (AmbiguousLN = True And People(PersonCounter).AmbiguousLNFull = True) _
        Then
            LastNameCheck = False
            If AmbiguousLN = True And People(PersonCounter).AmbiguousLNFull = True _
            And IsInArray(SourceKey, People(PersonCounter).SourceKey, "Exact") = True _
            Then AmbiguousLast = False
        End If
        Exit Function
    End If
    
    If LNExactMatch = True _
    Then
        LastNameCheck = False
        RequiresManualCheck = False
        If MCID <> People(PersonCounter).MCID Then LNMatch = True
        Exit Function
    End If
    
    For NameCounterAgain = 0 To UBound(ExactNames, 2)

        ExactLastNames = SplitName(ExactNames(1, NameCounterAgain))
        ReDim AmbiguousExactLNs(0 To UBound(ExactLastNames))
        AmbiguousExactLN = AmbiguousLNCheck(ExactLastNames, AmbiguousExactLNs)
        
        If IsInEachArray(LastNames, ExactLastNames, "LeftInside", "Either", , , AmbiguousLNs, AmbiguousExactLNs) = True _
        And IsInEachArray(People(PersonCounter).LastName, ExactLastNames, "LeftInside", "Either", , , People(PersonCounter).AmbiguousLN, AmbiguousExactLNs) = True _
        Then
        
            LastNameCheck = False
            If MCID <> People(PersonCounter).MCID Then LNMatch = True
            For NameCounter = 0 To UBound(MidInitials)
                If IsInArray(MidInitials(NameCounter), ExactLastNames, "Left", , , , AmbiguousExactLNs) = True _
                And LastName <> Replace(ExactNames(1, NameCounterAgain), " ", "") _
                And UBound(ExactLastNames) > 0 _
                Then
                    MidInitials(NameCounter) = vbNullString
                    If UBound(MidInitials) > 0 _
                    And UBound(ExactLastNames) > 0 _
                    Then
                        For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                            MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                        Next
                        ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                    End If
                    Exit For
                End If
            Next
            Exit Function
            
        End If
    Next
    
'   Compare first name(s) in current record against first name(s) in person in array
    If IsInEachArray(LastNames, People(PersonCounter).LastName, "Inside", "Either", , , AmbiguousLNs, People(PersonCounter).AmbiguousLN) = True _
    Then
    
        LastNameCheck = False
        If MCID <> People(PersonCounter).MCID Then LNMatch = True
        
        For NameCounter = 0 To UBound(MidInitials)
            If IsInArray(MidInitials(NameCounter), People(PersonCounter).LastName, "Left") = True _
            And UBound(People(PersonCounter).LastName) > 0 _
            Then
                MidInitials(NameCounter) = vbNullString
                If UBound(MidInitials) > 0 _
                Then
                    For DeleteCounter = NameCounter To UBound(MidInitials) - 1
                        MidInitials(DeleteCounter) = MidInitials(DeleteCounter + 1)
                    Next
                    ReDim Preserve MidInitials(UBound(MidInitials) - 1)
                End If
                Exit For
            End If
        Next
        Exit Function

    End If

'   If first name not found then run weighted Damerau-Levenshtein algorithm
    For NameCounter = 0 To UBound(LastNames)
        For NameCompareCounter = 0 To UBound(People(PersonCounter).LastName)
            If AmbiguousLNs(NameCounter) = False And People(PersonCounter).AmbiguousLN(NameCompareCounter) = False Then
                EditDistance = WeightedDL(CStr(LastNames(NameCounter)), CStr(People(PersonCounter).LastName(NameCompareCounter)), 1, 0.5, 1.5, 0.5)
                If EditDistance <= 1.5 _
                And (IsInArray(SSN, People(PersonCounter).SSN, "Exact") = True _
                Or (AmbiguousSSN = True Or IsInArray(True, People(PersonCounter).AmbiguousSSN, "Exact") = True)) _
                Then
                    LastNameCheck = False
                    If MCID <> People(PersonCounter).MCID Then LNTypo = True
                    Exit Function
                End If
            End If
        Next
    Next
    
    For NameCounter = 0 To UBound(LastNames)
        MetaphoneKey = DoubleMetaphone(LastNames(NameCounter), 10)
        MetaphoneKey = Application.WorksheetFunction.Trim(MetaphoneKey)
        MetaphoneKeys = Split(MetaphoneKey)
        For NameCompareCounter = 0 To UBound(People(PersonCounter).LastName)
            MetaphoneCompareKey = DoubleMetaphone(CStr(People(PersonCounter).LastName(NameCompareCounter)), 10)
            MetaphoneCompareKey = Application.WorksheetFunction.Trim(MetaphoneCompareKey)
            MetaphoneCompareKeys = Split(MetaphoneCompareKey)
            For MetaphoneCounter = 0 To UBound(MetaphoneKeys)
                For MetaphoneCompareCounter = 0 To UBound(MetaphoneCompareKeys)
                    If AmbiguousLNs(NameCounter) = False And People(PersonCounter).AmbiguousLN(NameCompareCounter) = False Then
                        If (Len(MetaphoneKeys(MetaphoneCounter)) > 3 And Len(MetaphoneCompareKeys(MetaphoneCompareCounter)) > 3) _
                        Or EditDistance <= 2 Then
                            If MetaphoneKeys(MetaphoneCounter) = MetaphoneCompareKeys(MetaphoneCompareCounter) _
                            Then
                                LastNameCheck = False
                                If MCID <> People(PersonCounter).MCID Then LNTypo = True
                                Exit Function
                            End If
                        End If
                    End If
                Next
            Next
        Next
    Next

End Function

Function AmbiguousDOBCheck(ByVal BirthDate As Date) As Boolean

    AmbiguousDOBCheck = False
    If IsInArray(BirthDate, AmbiguousDOBs) = True _
    Then AmbiguousDOBCheck = True
    
'    If (CStr(Format(BirthDate, "m/d")) = "1/1" _
'    And UBound(ExactDates) > 0) _
'    Or BirthDateYear = "8888" _

End Function

Function BirthDateCheck(ByVal BirthDate As Date, ByVal Counter As Integer) As Boolean

    Dim NameCounter As Integer
    Dim CompareMonth As String
    Dim CompareDay As String
    Dim CompareYear As String
    Dim MostCommonMonth As String
    Dim MostCommonDay As String
    Dim MostCommonYear As String
    Dim BirthDate1Match As Boolean
    Dim BirthDate2Match As Boolean

    BirthDateCheck = True
    DOBExactMatch = False
    BirthDate1Match = False
    BirthDate2Match = False
    If MostCommonDate <> "12:00:00 AM" _
    Then
        If BirthDate = MostCommonDate _
        And IsInArray(MostCommonDate, People(Counter).BirthDate) = True _
        Then
            DOBExactMatch = True
            BirthDateCheck = False
            If MCID <> People(Counter).MCID Then DOBMatch = True
            Exit Function
        End If
    End If
    If IsInArray(BirthDate, People(Counter).BirthDate) = True _
    Then
        DOBExactMatch = True
        BirthDateCheck = False
        If MCID <> People(Counter).MCID Then DOBMatch = True
        Exit Function
    End If
    If AmbiguousDOB = True _
    Or IsInArray(False, People(Counter).AmbiguousDOB) = False _
    Then
        If MCID <> People(PersonCounter).MCID Then AmbiguousDOBTask = True
        If IsInArray(BirthDate, People(Counter).BirthDate) = False _
        Then
            AmbiguousBirth = True
        ElseIf IsInArray(BirthDate, People(Counter).BirthDate) = True _
        Then
            DOBExactMatch = True
            BirthDateCheck = False
            If MCID <> People(PersonCounter).MCID _
            Then
                DOBMatch = True
                AmbiguousDOBTask = False
            End If
        End If
        Exit Function
    End If
    BirthDateMonth = CStr(Format(BirthDate, "mm"))
    BirthDateDay = CStr(Format(BirthDate, "dd"))
    BirthDateYear = CStr(Format(BirthDate, "yyyy"))
    MostCommonMonth = CStr(Format(MostCommonDate, "mm"))
    MostCommonDay = CStr(Format(MostCommonDate, "dd"))
    MostCommonYear = CStr(Format(MostCommonDate, "yyyy"))
    For NameCounter = 0 To UBound(People(Counter).BirthDate)
        CompareMonth = CStr(Format(People(Counter).BirthDate(NameCounter), "mm"))
        CompareDay = CStr(Format(People(Counter).BirthDate(NameCounter), "dd"))
        CompareYear = CStr(Format(People(Counter).BirthDate(NameCounter), "yyyy"))
        If (CStr(Format(BirthDate, "m/d")) = "1/1" _
        Or CStr(Format(People(Counter).BirthDate(NameCounter), "m/d")) = "1/1") _
        And BirthDateYear = CompareYear _
        Then
            AmbiguousBirth = True
            If IsInArray(SubscriberID, People(Counter).SubscriberID, "Exact") = True _
            Or IsInArray(SSN, People(Counter).SSN, "Exact") = True _
            Or IsInArray(SubscriberID, People(Counter).SSN, "Exact") = True _
            Or IsInArray(SSN, People(Counter).SubscriberID, "Exact") = True _
            Or IsInArray(HCID, People(Counter).HCID, "Exact") = True _
            Then
                BirthDateCheck = False
                AmbiguousBirth = False
                If MCID <> People(Counter).MCID Then DOBMatch = True
                Exit Function
            End If
        End If
    Next
    If AmbiguousBirth = True _
    Then Exit Function
    If UBound(ExactDates, 2) > 1 _
    Then
        If WeightedDL(CStr(Format(BirthDate, "mmddyyyy")), CStr(Format(MostCommonDate, "mmddyyyy")), 2, 2, 1, 2) <= 1 _
        Or CStr(Format(BirthDate, "mdyyyy")) = CStr(Format(MostCommonDate, "mdyyyy")) _
        Or (BirthDateMonth = MostCommonMonth _
        And Abs(CInt(BirthDateDay) - CInt(MostCommonDay)) <= 5 _
        And BirthDateYear = MostCommonYear) _
        Or (BirthDateMonth = MostCommonDay _
        And BirthDateDay = MostCommonMonth _
        And BirthDateYear = MostCommonYear) _
        Or (BirthDateMonth = Right(MostCommonMonth, 1) & Left(MostCommonMonth, 1) _
        And BirthDateDay = MostCommonDay _
        And BirthDateYear = MostCommonYear) _
        Or (BirthDateMonth = MostCommonMonth _
        And BirthDateDay = Right(MostCommonDay, 1) & Left(MostCommonDay, 1) _
        And BirthDateYear = MostCommonYear) _
        Or (BirthDateMonth = MostCommonMonth _
        And BirthDateDay = MostCommonDay _
        And BirthDateYear = Left(MostCommonYear, 2) & Right(MostCommonYear, 1) & Mid(MostCommonYear, 3)) _
        Then BirthDate1Match = True
        For NameCounter = 0 To UBound(People(Counter).BirthDate)
            CompareMonth = CStr(Format(People(Counter).BirthDate(NameCounter), "mm"))
            CompareDay = CStr(Format(People(Counter).BirthDate(NameCounter), "dd"))
            CompareYear = CStr(Format(People(Counter).BirthDate(NameCounter), "yyyy"))
            If WeightedDL(CStr(Format(People(Counter).BirthDate(NameCounter), "mmddyyyy")), CStr(Format(MostCommonDate, "mmddyyyy")), 2, 2, 1, 2) <= 1 _
            Or CStr(Format(People(Counter).BirthDate(NameCounter), "mdyyyy")) = CStr(Format(MostCommonDate, "mdyyyy")) _
            Or (CompareMonth = MostCommonMonth _
            And Abs(CInt(CompareDay) - CInt(MostCommonDay)) <= 5 _
            And CompareYear = MostCommonYear) _
            Or (CompareMonth = MostCommonDay _
            And CompareDay = MostCommonMonth _
            And CompareYear = MostCommonYear) _
            Or (CompareMonth = Right(MostCommonMonth, 1) & Left(MostCommonMonth, 1) _
            And CompareDay = MostCommonDay _
            And CompareYear = MostCommonYear) _
            Or (CompareMonth = MostCommonMonth _
            And CompareDay = Right(MostCommonDay, 1) & Left(MostCommonDay, 1) _
            And CompareYear = MostCommonYear) _
            Or (CompareMonth = MostCommonMonth _
            And CompareDay = MostCommonDay _
            And CompareYear = Left(MostCommonYear, 2) & Right(MostCommonYear, 1) & Mid(MostCommonYear, 3, 1)) _
            Then
                BirthDate2Match = True
                Exit For
            End If
        Next
    ElseIf UBound(ExactDates, 2) = 1 _
    Then
        For NameCounter = 0 To UBound(People(Counter).BirthDate)
            CompareMonth = CStr(Format(People(Counter).BirthDate(NameCounter), "mm"))
            CompareDay = CStr(Format(People(Counter).BirthDate(NameCounter), "dd"))
            CompareYear = CStr(Format(People(Counter).BirthDate(NameCounter), "yyyy"))
            If WeightedDL(CStr(Format(BirthDate, "mmddyyyy")), CStr(Format(People(Counter).BirthDate(NameCounter), "mmddyyyy")), 2, 2, 1, 2) <= 1 _
            Or CStr(Format(BirthDate, "mdyyyy")) = CStr(Format(People(Counter).BirthDate(NameCounter), "mdyyyy")) _
            Or (BirthDateMonth = CompareMonth _
            And Abs(CInt(BirthDateDay) - CInt(CompareDay)) <= 5 _
            And BirthDateYear = CompareYear) _
            Or (BirthDateMonth = CompareDay _
            And BirthDateDay = CompareMonth _
            And BirthDateYear = CompareYear) _
            Or (BirthDateMonth = Right(CompareMonth, 1) & Left(CompareMonth, 1) _
            And BirthDateDay = CompareDay _
            And BirthDateYear = CompareYear) _
            Or (BirthDateMonth = CompareMonth _
            And BirthDateDay = Right(CompareDay, 1) & Left(CompareDay, 1) _
            And BirthDateYear = CompareYear) _
            Or (BirthDateMonth = CompareMonth _
            And BirthDateDay = CompareDay _
            And BirthDateYear = Left(CompareYear, 2) & Right(CompareYear, 1) & Mid(CompareYear, 3, 1)) _
            Then
                BirthDate1Match = True
                BirthDate2Match = True
            End If
        Next
    End If
    If BirthDate1Match = True _
    And BirthDate2Match = True _
    Then
        BirthDateCheck = False
        If MCID <> People(Counter).MCID Then DOBTypo = True
    End If

End Function

Function MidInitialCheck(ByRef MidInitials() As Variant, ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim Counter As Integer
    Dim CompareCounter As Integer
    
    MidInitialCheck = True
    MidInitialCounter = 0
    If UBound(People) > 1 _
    Then
        For Counter = 1 To UBound(People)
            For CompareCounter = 1 To UBound(People)
                If IsInEachArray(People(Counter).MidInitial, People(CompareCounter).MidInitial, "Left", , True, "LeftFoundInRight") = False _
                And IsInEachArray(People(Counter).MidInitial, People(CompareCounter).MidInitial, "Left", , True, "RightFoundInLeft") = False _
                And IsInArray(vbNullString, People(Counter).MidInitial, "Exact", , True) = False _
                And IsInArray(vbNullString, People(CompareCounter).MidInitial, "Exact", , True) = False _
                And People(Counter).ExpiredMCID = False And People(CompareCounter).ExpiredMCID = False _
                Then
                    MidInitialCounter = MidInitialCounter + 1
                    Exit For
                End If
            Next
        Next
        If MidInitialCounter < 1 Then MidInitialCheck = False
        For Counter = UBound(People) - 1 To 1 Step -1
            For CompareCounter = UBound(People) To Counter + 1 Step -1
                If CInt(People(Counter).PersonKey) = CInt(People(CompareCounter).PersonKey) _
                And (IsInEachArray(People(Counter).MidInitial, People(CompareCounter).MidInitial, "Left", , True, "LeftFoundInRight") = True _
                Or IsInEachArray(People(Counter).MidInitial, People(CompareCounter).MidInitial, "Left", , True, "RightFoundInLeft") = True _
                Or IsInArray(vbNullString, People(Counter).MidInitial, "Exact", , True) = True _
                Or IsInArray(vbNullString, People(CompareCounter).MidInitial, "Exact", , True) = True) _
                Then
                    If PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.2) = True _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.3) = True _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.4) = True _
                    Then
                        If CDec(People(Counter).PersonKey) - CInt(People(Counter).PersonKey) = 0.1 _
                        And CDec(People(CompareCounter).PersonKey) - CInt(People(Counter).PersonKey) = 0.4 _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.1) = True _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.2) = False _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.3) = False _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.4) = True _
                        Then Call MidInitialCheckLevel(Counter, CompareCounter, StartRow, EndRow)
                    ElseIf PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.2) = True _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.3) = True _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.4) = False _
                    Then
                        If (CDec(People(Counter).PersonKey) - CInt(People(Counter).PersonKey) = 0.1 _
                        And (CDec(People(CompareCounter).PersonKey) - CInt(People(Counter).PersonKey) = 0.2 _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.1) = True _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.2) = True _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.3) = False) _
                        Xor (CDec(People(Counter).PersonKey) - CInt(People(Counter).PersonKey) = 0.2 _
                        And CDec(People(CompareCounter).PersonKey) - CInt(People(Counter).PersonKey) = 0.3 _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.1) = False _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.2) = True _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.3) = True) _
                        Xor (CDec(People(Counter).PersonKey) - CInt(People(Counter).PersonKey) = 0.1 _
                        And CDec(People(CompareCounter).PersonKey) - CInt(People(Counter).PersonKey) = 0.3 _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.1) = True _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.2) = False _
                        And PeopleKeys.Item(CDec(CInt(People(Counter).PersonKey)) + 0.3) = True)) _
                        Then Call MidInitialCheckLevel(Counter, CompareCounter, StartRow, EndRow)
                    ElseIf PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.2) = True _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.3) = False _
                    And PeopleKeys.Exists(CDec(CInt(People(Counter).PersonKey)) + 0.4) = False _
                    Then
                        Call MidInitialCheckLevel(Counter, CompareCounter, StartRow, EndRow)
                    Else
                        If People(Counter).MCID <> People(CompareCounter).MCID Then MIMissing = True
                    End If
                End If
            Next
        Next
    End If
    
End Function

Sub MidInitialCheckLevel(ByVal Counter As Integer, ByVal CompareCounter As Integer, ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim NameCounter As Integer
    Dim HighCounter As Integer
    Dim HighCounterAgain As Integer
    
    HighCounter = 0
    HighCounterAgain = 0
    For NameCounter = 0 To UBound(People(Counter).MidInitial)
        HighCounter = HighCounter + 1
    Next
    For NameCounter = 0 To UBound(People(CompareCounter).MidInitial)
        HighCounterAgain = HighCounterAgain + 1
    Next
    If (LastNameCounter = 1 Or MarriageRule = False) _
    And IsInEachArray(People(Counter).BirthDate, People(CompareCounter).BirthDate) = True _
    And People(Counter).ExpiredMCID = People(CompareCounter).ExpiredMCID _
    And People(Counter).MCID = People(CompareCounter).MCID _
    Then
        If People(Counter).MCID <> People(CompareCounter).MCID Then MIMatch = True
        Call CombinePeople(Counter, CompareCounter, StartRow, EndRow)
        Exit Sub
    End If
    If MarriageRule = True _
    Then
        If MidInitialCounter = 0 _
        And People(Counter).ExpiredMCID = People(CompareCounter).ExpiredMCID _
        And People(Counter).MCID = People(CompareCounter).MCID _
        Then
            If People(Counter).MCID <> People(CompareCounter).MCID Then MIMatch = True
            Call CombinePeople(Counter, CompareCounter, StartRow, EndRow)
            Exit Sub
        ElseIf MidInitialCounter = 1 _
        And (HighCounter > 2 _
        Or IsInArray(vbNullString, People(Counter).MidInitial, "Exact", , True) = True _
        Or IsInArray(vbNullString, People(CompareCounter).MidInitial, "Exact", , True) = True) _
        And (HighCounterAgain > 2 _
        Or IsInArray(vbNullString, People(Counter).MidInitial, "Exact", , True) = True _
        Or IsInArray(vbNullString, People(CompareCounter).MidInitial, "Exact", , True) = True) _
        And People(Counter).ExpiredMCID = People(CompareCounter).ExpiredMCID _
        And People(Counter).MCID = People(CompareCounter).MCID _
        Then
            If People(Counter).MCID <> People(CompareCounter).MCID _
            Then
                MIMatch = True
                LNMarriage = True
            End If
            Call CombinePeople(Counter, CompareCounter, StartRow, EndRow)
            Exit Sub
        ElseIf MidInitialCounter > 1 _
        And HighCounter > 2 _
        And HighCounterAgain > 2 _
        And People(Counter).ExpiredMCID = People(CompareCounter).ExpiredMCID _
        And People(Counter).MCID = People(CompareCounter).MCID _
        Then
            If People(Counter).MCID <> People(CompareCounter).MCID _
            Then
                MIMatch = True
                LNMarriage = True
            End If
            Call CombinePeople(Counter, CompareCounter, StartRow, EndRow)
            Exit Sub
        End If
    End If

End Sub

Sub CombinePeople(ByVal Counter As Integer, ByVal CompareCounter As Integer, ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim MatchCounter As Integer, DeleteCounter As Integer
    
    For MatchCounter = 0 To UBound(People(CompareCounter).SourceKey())
        People(Counter).SourceKey() = People(CompareCounter).SourceKey(MatchCounter)
    Next
'    For MatchCounter = 0 To UBound(People(CompareCounter).MCID())
'        People(Counter).MCID() = People(CompareCounter).MCID(MatchCounter)
'    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).NameStatus())
        People(Counter).NameStatus() = People(CompareCounter).NameStatus(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).FirstNameFull())
        People(Counter).FirstNameFull() = People(CompareCounter).FirstNameFull(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).FirstName())
        People(Counter).FirstName() = People(CompareCounter).FirstName(MatchCounter)
        People(Counter).AmbiguousFN() = People(CompareCounter).AmbiguousFN(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).LastNameFull())
        People(Counter).LastNameFull() = People(CompareCounter).LastNameFull(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).LastName())
        People(Counter).LastName() = People(CompareCounter).LastName(MatchCounter)
        People(Counter).AmbiguousLN() = People(CompareCounter).AmbiguousLN(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).MidInitial())
        People(Counter).MidInitial() = People(CompareCounter).MidInitial(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).BirthDate())
        People(Counter).BirthDate() = People(CompareCounter).BirthDate(MatchCounter)
        People(Counter).AmbiguousDOB() = People(CompareCounter).AmbiguousDOB(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).Gender())
        People(Counter).Gender() = People(CompareCounter).Gender(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).SubscriberID())
        People(Counter).SubscriberID() = People(CompareCounter).SubscriberID(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).SSN())
        People(Counter).SSN() = People(CompareCounter).SSN(MatchCounter)
        People(Counter).AmbiguousSSN() = People(CompareCounter).AmbiguousSSN(MatchCounter)
    Next
    For MatchCounter = 0 To UBound(People(CompareCounter).HCID())
        People(Counter).HCID() = People(CompareCounter).HCID(MatchCounter)
    Next
    
    If People(CompareCounter).ActiveRecords = True Then People(Counter).ActiveRecords = True
    If People(CompareCounter).AmbiguousFNFull = True Then People(Counter).AmbiguousFNFull = True
    If People(CompareCounter).AmbiguousLNFull = True Then People(Counter).AmbiguousLNFull = True
    If People(CompareCounter).HardAmbiguousFN = True Then People(Counter).HardAmbiguousFN = True
    If People(CompareCounter).HardAmbiguousLN = True Then People(Counter).HardAmbiguousLN = True
    
    PeopleKeys.Remove People(CompareCounter).PersonKey
    
    For DeleteCounter = StartRow To EndRow
        If Cells(DeleteCounter, FPSequence1Col).Value2 = People(CompareCounter).PersonNumber _
        Then
            Cells(DeleteCounter, FPSequence1Col).Value2 = People(Counter).PersonNumber
        ElseIf Cells(DeleteCounter, FPSequence1Col).Value2 > People(CompareCounter).PersonNumber _
        Then
            Cells(DeleteCounter, FPSequence1Col).Value2 = Cells(DeleteCounter, FPSequence1Col).Value2 - 1
        End If
    Next
    
    If UBound(People) > CompareCounter Then
        For DeleteCounter = CompareCounter To UBound(People) - 1
            Set People(DeleteCounter) = People(DeleteCounter + 1)
            People(DeleteCounter).PersonNumber = People(DeleteCounter).PersonNumber - 1
        Next
    End If
    ReDim Preserve People(0 To UBound(People) - 1)
    
End Sub

Function FPCheck(ByVal StartRow As Integer, ByVal EndRow As Integer, ByVal FalseNegativeCheck As Boolean)

    Dim Counter As Integer
    Dim RowCounter As Integer
    Dim MatchCounter As Integer

    For Counter = 1 To UBound(People) - 1
        For MatchCounter = Counter + 1 To UBound(People)
            If People(Counter).ActiveRecords = True And People(MatchCounter).ActiveRecords = True _
            And People(Counter).ExpiredMCID = False And People(MatchCounter).ExpiredMCID = False _
            Then MCIDIsFP = True
        Next
    Next
    
    If MCIDIsFP = True _
    Then
        For RowCounter = StartRow To EndRow
            If Cells(RowCounter, FPSequence1Col).Value2 > 0 _
            Then Cells(RowCounter, FPSequence1Col).Value2 = Cells(RowCounter, FPSequence1Col).Value2 + 1
       Next
    ElseIf MCIDIsPO = True _
    Then
        For RowCounter = StartRow To EndRow
            If Cells(RowCounter, FPSequence1Col).Value2 <> "0" _
            Then Cells(RowCounter, FPSequence1Col).Value2 = vbNullString
        Next
    End If
    
    FPCheck = MCIDIsFP

End Function

Function POCheck(ByVal StartRow As Integer, ByVal EndRow As Integer)

    Dim Counter As Integer
    Dim MatchCounter As Integer
    Dim RowCounter As Integer
    
    For Counter = 1 To UBound(People) - 1
        For MatchCounter = Counter + 1 To UBound(People)
            If IsInArrayExactCount(People(Counter).SourceKey, People(MatchCounter).SourceKey, People(Counter).NameStatus, People(MatchCounter).NameStatus) = True _
            And People(Counter).ExpiredMCID = False And People(MatchCounter).ExpiredMCID = False _
            Then MCIDIsPO = True
        Next
    Next

    If MCIDIsPO = True _
    Then
        For RowCounter = StartRow To EndRow
            If Cells(RowCounter, NameStatusCol).Value2 = "I" _
            Then Cells(RowCounter, FPSequence1Col).Value2 = "0"
        Next
    End If

    POCheck = MCIDIsPO
    
End Function

Sub ReviewComments(ByVal AmbiguousFNMCID As Boolean, ByVal FNDiffersMCID As Boolean, ByVal AmbiguousLNMCID As Boolean, ByVal LNDiffersMCID As Boolean, ByVal MIDiffersMCID As Boolean, ByVal DOBDiffersMCID As Boolean, ByVal FalseNegativeCheck As Boolean, ByVal AnalystReview As String)

    Dim RowCounter As Integer
    Dim FPSequenceNum As String
    Dim ReviewBuild As New StringBuilder
    Dim TaskReviewBuild As New StringBuilder
    Dim CommentsBuild As New StringBuilder
    
    ReviewBuild.Class_Initialize
    TaskReviewBuild.Class_Initialize
    CommentsBuild.Class_Initialize
    
    If (FalseNegativeAnalysis = True Or FalseNegativeReview = True) _
    And FalseNegativeCheck = False _
    Then
        For RowCounter = TaskStartRow To TaskEndRow
            If FalseNegativeAnalysis = True And Cells(RowCounter, IsFNAnalystCol).Value2 = vbNullString _
            Then
                Cells(RowCounter, IsFNAnalystCol).Value2 = "N"
                Cells(RowCounter, MatchPatternCol).Value2 = "TWINS DIFFERENT_FN_WITH_SAME_GENDER"
            End If
            Cells(RowCounter, IsPOCol).Value2 = vbNullString
            Cells(RowCounter, MCIDPairFlagCol).Value2 = "0"
            Cells(RowCounter, MultipleMCIDFlagCol).Value2 = vbNullString
        Next RowCounter
    End If
    
    If ExpiredMCID = True _
    Then
        CommentsBuild.Append ("expired MCID")
    Else
        If MCIDIsFP = True Then ReviewBuild.Append ("FP")
        If MCIDIsFP = True And MCIDIsPO = True Then ReviewBuild.Append ("+")
        If MCIDIsPO = True Then ReviewBuild.Append ("PO")
        Review = ReviewBuild.ToString
        If TaskIsFP = True Then TaskReviewBuild.Append ("FP")
        If TaskIsFP = True And TaskIsPO = True Then TaskReviewBuild.Append ("+")
        If TaskIsPO = True Then TaskReviewBuild.Append ("PO")
        TaskReview = TaskReviewBuild.ToString
        If FNDiffersMCID = True Then CommentsBuild.Append ("FN")
        If LNDiffersMCID = True And (MIDiffersMCID = True Or DOBDiffersMCID = True) And CommentsBuild.ToString <> vbNullString _
        Then
            CommentsBuild.Append (", ")
        ElseIf LNDiffersMCID = True And CommentsBuild.ToString <> vbNullString _
        Then
            CommentsBuild.Append (" and ")
        End If
        If LNDiffersMCID = True Then CommentsBuild.Append ("LN")
        If MIDiffersMCID = True And DOBDiffersMCID = True And CommentsBuild.ToString <> vbNullString _
        Then
            CommentsBuild.Append (", ")
        ElseIf MIDiffersMCID = True And CommentsBuild.ToString <> vbNullString _
        Then
            CommentsBuild.Append " and "
        End If
        If MIDiffersMCID = True Then CommentsBuild.Append ("MI")
        If DOBDiffersMCID = True And CommentsBuild.ToString <> vbNullString Then CommentsBuild.Append (" and ")
        If DOBDiffersMCID = True Then CommentsBuild.Append ("DOB")
        If CommentsBuild.ToString <> vbNullString _
        Then
            CommentsBuild.Append (" differs")
        Else
            CommentsBuild.Class_Initialize
        End If
        If AmbiguousFNMCID = True And Review <> vbNullString _
        Then
            If CommentsBuild.ToString = vbNullString _
            Then
                CommentsBuild.Append ("ambiguous FN")
            Else
                CommentsBuild.Append (", ambiguous FN")
            End If
        End If
        If AmbiguousLNMCID = True And Review <> vbNullString _
        Then
            If AmbiguousFNMCID = True _
            Then
                CommentsBuild.Append (" and LN")
            Else
                If CommentsBuild.ToString = vbNullString _
                Then
                    CommentsBuild.Append ("ambiguous LN")
                Else
                    CommentsBuild.Append (", ambiguous LN")
                End If
            End If
        End If
        If AmbiguousDOBMCID = True And Review <> vbNullString _
        Then
            If AmbiguousFNMCID = True Or AmbiguousLNMCID = True _
            Then
                CommentsBuild.Append (" and DOB")
            Else
                If CommentsBuild.ToString = vbNullString _
                Then
                    CommentsBuild.Append ("ambiguous DOB")
                Else
                    CommentsBuild.Append (", ambiguous DOB")
                End If
            End If
        End If
    End If

    Comments = CommentsBuild.ToString
    If StartRow = EndRow And ExpiredMCID = False Then Comments = vbNullString
    If FalseNegativeCheck = True Then Review = vbNullString
    If Review = vbNullString And Comments <> "expired MCID" Then Comments = vbNullString
        
    For RowCounter = StartRow To EndRow
        FPSequenceNum = Cells(RowCounter, FPSequence1Col).Value2
        
        Select Case FPSequenceNum
            Case Is = "0"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(38, 38, 38)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(38, 38, 38)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(38, 38, 38)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(38, 38, 38)
            Case Is = "2"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(47, 117, 81)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(73, 69, 41)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(73, 69, 41)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(73, 69, 41)
            Case Is = "3"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(198, 89, 17)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(22, 54, 92)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(22, 54, 92)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(22, 54, 92)
            Case Is = "4"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(123, 123, 123)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(54, 96, 146)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(54, 96, 146)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(54, 96, 146)
            Case Is = "5"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(191, 143, 0)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(150, 54, 52)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(150, 54, 52)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(150, 54, 52)
            Case Is = "6"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(48, 84, 150)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(118, 147, 60)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(118, 147, 60)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(118, 147, 60)
            Case Is = "7"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(84, 130, 53)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(96, 73, 122)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(96, 73, 122)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(96, 73, 122)
            Case Is = "8"
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(58, 56, 56)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(49, 134, 155)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(49, 134, 155)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(49, 134, 155)
'            Case Is > 8
''                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(58, 56, 56)
'                Cells(RowCounter, ReviewCol).Font.Color = RGB(226, 107, 10)
'                Cells(RowCounter, CommentsCol).Font.Color = RGB(226, 107, 10)
'                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(226, 107, 10)
            Case Is = vbNullString, Is = ""
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(166, 166, 166)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(166, 166, 166)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(166, 166, 166)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(166, 166, 166)
            Case Else
'                Range("A" & CStr(RowCounter) & ":E" & CStr(RowCounter)).Font.Color = RGB(51, 63, 79)
                Cells(RowCounter, ReviewCol).Font.Color = RGB(226, 107, 10)
                Cells(RowCounter, CommentsCol).Font.Color = RGB(226, 107, 10)
                Cells(RowCounter, FPSequence1Col).Font.Color = RGB(226, 107, 10)
        End Select
        
        Cells(RowCounter, ReviewCol).Value2 = Review
        
        If Cells(RowCounter, CommentsCol).Value2 <> "test data" _
        Then
            If FalseNegativeCheck = False Then Cells(RowCounter, CommentsCol).Value2 = Comments
            If Review = vbNullString And FalseNegativeCheck = False _
            Then Cells(RowCounter, FPSequence1Col).Value2 = vbNullString
            If FalseNegativeAnalysis = True And FalseNegativeCheck = False _
            Then
                If MCIDIsFP = True Or MCIDIsPO = True _
                Then
                    Cells(RowCounter, IsFNAnalystCol).Value2 = "N"
                    Cells(RowCounter, MatchPatternCol).Value2 = Review & ", " & "CURRENT " & "= "
                ElseIf ExpiredMCID = True _
                Then
                    Cells(RowCounter, IsFNAnalystCol).Value2 = "N"
                    Cells(RowCounter, MatchPatternCol).Value2 = "EXPIRED MCID"
                End If
            ElseIf FalseNegativeReview = True And FalseNegativeCheck = False _
            Then
                If MCIDIsFP = True Or MCIDIsPO = True _
                Then
                    If Review <> AnalystReview _
                    Then
                        Cells(RowCounter, IsFNReviewerCol).Value2 = "N"
                        Cells(RowCounter, MatchPatternReviewerCol).Value2 = Review & ", " & "CURRENT " & "= "
                    End If
                ElseIf ExpiredMCID = True _
                Then
                    If Review <> AnalystReview _
                    Then
                        Cells(RowCounter, IsFNReviewerCol).Value2 = "N"
                        Cells(RowCounter, MatchPatternReviewerCol).Value2 = "EXPIRED MCID"
                    End If
                End If
            End If
        End If
        
    Next
    
    If FalseNegativeAnalysis = True And FalseNegativeCheck = False _
    Then
        If TaskIsFP = True Or TaskIsPO = True _
        Then
            For RowCounter = TaskStartRow To TaskEndRow
                If Cells(RowCounter, IsFNAnalystCol).Value2 = vbNullString _
                Then
                    Cells(RowCounter, IsFNAnalystCol).Value2 = "N"
                    Cells(RowCounter, MatchPatternCol).Value2 = "TWINS DIFFERENT_FN_WITH_SAME_GENDER"
                End If
                Cells(RowCounter, IsPOCol).Value2 = TaskReview
                Cells(RowCounter, MultipleMCIDFlagCol).Value2 = "FALSE POSITIVE"
                Cells(RowCounter, FPSequence2Col).Font.Size = Cells(RowCounter, FPSequence1Col).Font.Size
                Cells(RowCounter, FPSequence2Col).Font.Color = Cells(RowCounter, FPSequence1Col).Font.Color
                Cells(RowCounter, FPSequence2Col).Value2 = Cells(RowCounter, FPSequence1Col).Value2
            Next RowCounter
        End If
    ElseIf FalseNegativeReview = True And FalseNegativeCheck = False _
    Then
        If TaskIsFP = True Or TaskIsPO = True _
        Then
            For RowCounter = TaskStartRow To TaskEndRow
                If Review <> AnalystReview _
                Then
                    If Cells(RowCounter, IsFNReviewerCol).Value2 = vbNullString _
                    Then
                        Cells(RowCounter, IsFNReviewerCol).Value2 = "N"
                        Cells(RowCounter, MatchPatternReviewerCol).Value2 = "TWINS DIFFERENT_FN_WITH_SAME_GENDER"
                    End If
                End If
                Cells(RowCounter, IsPOCol).Value2 = TaskReview
                Cells(RowCounter, MultipleMCIDFlagCol).Value2 = "FALSE POSITIVE"
                Cells(RowCounter, FPSequence2Col).Font.Size = Cells(RowCounter, FPSequence1Col).Font.Size
                Cells(RowCounter, FPSequence2Col).Font.Color = Cells(RowCounter, FPSequence1Col).Font.Color
                Cells(RowCounter, FPSequence2Col).Value2 = Cells(RowCounter, FPSequence1Col).Value2
            Next RowCounter
        End If
    End If
    
End Sub

Function FPSequenceCheck(ByVal StartRow As Integer, ByVal EndRow As Integer) As Boolean

    Dim Counter As Integer
    Dim FPSequenceDSReview() As String, FPSequenceMyReview() As String
    Dim ReviewFPSequence As Boolean
    
    ReviewFPSequence = False
    
    FPSequenceDSReview() = ResequenceNum(StartRow, EndRow, FPSequence2Col)
    FPSequenceMyReview() = ResequenceNum(StartRow, EndRow, FPSequence1Col)
    
    For Counter = 0 To UBound(FPSequenceDSReview)
        If FPSequenceMyReview(Counter) <> FPSequenceDSReview(Counter) _
        And FPSequenceDSReview(Counter) <> vbNullString _
        Then
            ReviewFPSequence = True
            Exit For
        End If
    Next
    
    FPSequenceCheck = ReviewFPSequence
    
End Function

Function ResequenceNum(ByVal StartRow As Integer, ByVal EndRow As Integer, ByVal FPSequenceCol As Integer) As String()

    Dim Counter As Integer, RowCounter As Integer
    Dim FPSequenceNum As String
    Dim FPSequenceIndex() As String, FPSequenceNumSort() As String

    ReDim FPSequenceIndex(0 To 0) As String
    ReDim FPSequenceNumSort(0 To EndRow - StartRow) As String
    
    For RowCounter = StartRow To EndRow
        FPSequenceNum = Cells(RowCounter, FPSequenceCol).Value2
        If IsInArray(FPSequenceNum, FPSequenceIndex, "Exact") = False _
        And FPSequenceNum <> "0" And FPSequenceNum <> vbNullString _
        Then
            ReDim Preserve FPSequenceIndex(0 To UBound(FPSequenceIndex) + 1)
            FPSequenceIndex(UBound(FPSequenceIndex)) = FPSequenceNum
        End If
    Next
    
    For RowCounter = StartRow To EndRow
        FPSequenceNum = Cells(RowCounter, FPSequenceCol).Value2
        For Counter = 1 To UBound(FPSequenceIndex)
            If FPSequenceNum = "0" _
            Then
                FPSequenceNumSort(RowCounter - StartRow) = "0"
                Exit For
            ElseIf FPSequenceNum = vbNullString _
            Then
                FPSequenceNumSort(RowCounter - StartRow) = vbNullString
                Exit For
            End If
            If FPSequenceNum = FPSequenceIndex(Counter) _
            Then
                If FPSequenceNum <> "0" And FPSequenceNum <> vbNullString _
                Then
                    FPSequenceNumSort(RowCounter - StartRow) = CStr(Counter)
                    Exit For
                End If
            End If
        Next
    Next
    
    If FPSequenceCol = FPSequence2Col _
    Then Call RemoveIndex(StartRow, EndRow, FPSequenceIndex)
    
    ResequenceNum = FPSequenceNumSort

End Function

Sub RemoveIndex(ByVal StartRow As Integer, ByVal EndRow As Integer, ByRef Index() As String)

    Dim Counter As Integer, RowCounter As Integer
    Dim NumberFound As Boolean
    
    For Counter = 1 To UBound(Index) + 1
        NumberFound = False
        For RowCounter = StartRow To EndRow
            If Counter = Cells(RowCounter, FPSequence1Col).Value2 _
            Then
                NumberFound = True
                Exit For
            End If
        Next
        If NumberFound = False _
        Then
            For RowCounter = StartRow To EndRow
                If Cells(RowCounter, FPSequence1Col).Value2 > Counter _
                And Cells(RowCounter, FPSequence1Col).Value2 > 1 _
                Then Cells(RowCounter, FPSequence1Col).Value2 = Cells(RowCounter, FPSequence1Col).Value2 - 1
            Next
        End If
    Next

End Sub

Function FormatName(ByVal FullName As Variant) As Variant

    FullName = UCase(FullName)
    FullName = Replace(FullName, "-", " ")
    FullName = Replace(FullName, ".", " ")
    FullName = AlphaNumericOnlySpecial(CStr(FullName))
    If FullName = "NA" Or FullName = " " Then FullName = vbNullString
    If FullName = "0" Then FullName = "O"
    FullName = Application.WorksheetFunction.Trim(FullName)
    If IsNumeric(FullName) = True _
    Then FullName = AlphabeticOnly(CStr(FullName))
    FormatName = CVar(FullName)

End Function

Function SplitName(ByVal FullName As Variant) As Variant()
    Dim SplitNames() As String
    Dim TempNames() As Variant
    Dim NameCounter As Integer
    FullName = CStr(FullName)
'    For NameCounter = 0 To UBound(ExactNames, 2)
'        If FullName
    If FullName = vbNullString _
    Then
        ReDim SplitNames(0 To 0)
        SplitNames(0) = vbNullString
    Else
        SplitNames = Split(FullName)
    End If
'    ReDim TempNames(0 To UBound(SplitNames) + 1) As Variant
    ReDim TempNames(0 To UBound(SplitNames)) As Variant
    For NameCounter = 0 To UBound(SplitNames)
        TempNames(NameCounter) = CVar(SplitNames(NameCounter))
    Next
'    TempNames(UBound(TempNames)) = Replace(FullName, " ", vbNullString)
    SplitName = TempNames

End Function

Function GenderCount(ByVal StartRow As Integer, ByVal EndRow As Integer) As String

    Dim GenderCounter As Integer
    Dim FemaleCount As Integer
    Dim MaleCount As Integer
    Dim GenderPercent As Double
    
    FemaleCount = 1
    MaleCount = 1

    For GenderCounter = StartRow To EndRow
        Gender = Cells(GenderCounter, GenderCol).Value2
        If Gender = "F" Then FemaleCount = FemaleCount + 1
        If Gender = "M" Then MaleCount = MaleCount + 1
    Next
                    
    GenderPercent = FemaleCount / (FemaleCount + MaleCount)
                
    If Round(GenderPercent, 3) >= 0.667 Then GenderMCID = "F"
    If Round(GenderPercent, 3) < 0.667 Then GenderMCID = "M"
    
    GenderCount = GenderMCID
                
End Function

Sub FlaggedMCIDComments(ByVal StartRow As Integer, ByVal EndRow As Integer, ByVal ReviewFPSequence As Boolean, ByVal AnalystReview As String, ByVal ReviewerReview As String)


    Dim Feedback As String
    Dim Reason As String
    
    If EmailComments = True Then MCID = vbNullString
    
    Feedback = vbNullString
    Reason = vbNullString

    If (ReviewerReview <> vbNullString And ReviewerReview <> Review) _
    Or (ReviewerReview = vbNullString And AnalystReview <> Review) _
    Then
    
        Select Case AnalystReview
            Case Is = "FP"
                Select Case Review
                    Case Is = "FP+PO"
                        Feedback = "Also PO"
                        Reason = " share source key"
                    Case Is = "PO"
                        Feedback = "PO and not FP"
                        Reason = " are not active and share source key"
                    Case Is = vbNullString
                        Feedback = "Not FP"
                        Reason = " because all records belong to same person"
                End Select
            Case Is = "PO"
                Select Case Review
                    Case Is = "FP+PO"
                        Feedback = "Also FP"
                        Reason = " are active"
                    Case Is = "FP"
                        Feedback = "FP and not PO"
                        Reason = " are active and do not share source key"
                    Case Is = "PO"
                        Feedback = "Mark all inactive records"
                        Reason = vbNullString
                    Case Is = vbNullString
                        Feedback = "Not PO"
                        Reason = " because all records belong to same person"
                End Select
            Case Is = "FP+PO"
                Select Case Review
                    Case Is = "FP"
                        Feedback = "Not PO"
                        Reason = " do not share source key"
                    Case Is = "PO"
                        Feedback = "Not FP"
                        Reason = " are not active"
                    Case Is = vbNullString
                        Feedback = "Not FP and not PO"
                        Reason = " because all records belong to same person"
                End Select
            Case Else
                Select Case Review
                    Case Is = "FP"
                        Feedback = "FP"
                        Reason = " are active"
                    Case Is = "PO"
                        Feedback = "PO"
                        Reason = " share source key"
                    Case Is = "FP+PO"
                        Feedback = "FP"
                        Reason = " are active and share source key"
                End Select
        End Select
        
        If AmbiguousFNMCID = True And FNDiffersMCID = False _
        Then Reason = " because records with same ambiguous first name do not share source key"
        If AmbiguousLNMCID = True And LNDiffersMCID = False _
        Then Reason = " because records with same ambiguous last name do not share source key"
'        If AmbiguousLNMCID = True And LNDiffersMCID = False Then Reason = " because all records with ambiguous last name share SSN with records with non-ambiguous last name"
        If StartRow = EndRow Then Reason = " because only one record exists in MCID"
        
        If Reason <> " because all records belong to same person" _
        And Reason <> " because only one record exists in MCID" _
        And Reason <> " because records with same ambiguous first name do not share source key" _
        And Reason <> " because records with same ambiguous last name do not share source key" _
        And Reason <> vbNullString _
        Then
            Feedback = Feedback & " because records with "
            If FNDiffersMCID = True And AmbiguousFNMCID = True _
            Then
                Feedback = Feedback & "ambiguous first name and records with non-ambiguous first name"
            ElseIf FNDiffersMCID = True And AmbiguousFNMCID = False _
            Then
                Feedback = Feedback & "different first names"
            End If
            If LNDiffersMCID = True And AmbiguousLNMCID = True _
            Then
                Feedback = Feedback & "ambiguous last name and records with non-ambiguous last name"
            ElseIf LNDiffersMCID = True And AmbiguousLNMCID = False _
            Then
                Feedback = Feedback & "different last names"
            End If
            If MIDiffersMCID = True Then Feedback = Feedback & "different middle initials"
            If DOBDiffersMCID = True Then Feedback = Feedback & "different dates of birth"
        End If
        
        If Feedback <> vbNullString And Reason <> vbNullString _
        Then
            Print #1, MCID & vbTab & StartRow & " -- " & EndRow & vbTab & vbTab & Feedback & Reason
            Debug.Print MCID & vbTab & StartRow & " -- " & EndRow & vbTab & vbTab & Feedback & Reason
        End If
        
    ElseIf Review = AnalystReview And ReviewFPSequence = True _
    Then
        Print #1, MCID & vbTab & StartRow & " -- " & EndRow & vbTab & vbTab & "Review FP sequence"
        Debug.Print MCID & vbTab & StartRow & " -- " & EndRow & vbTab & vbTab & "Review FP sequence"
    End If

End Sub

Function FindAnalystReview(ByVal StartRow As Integer, ByVal EndRow As Integer, ByVal AnalystReview As String)

    Dim MatchingPatternRaw As String
    Dim MatchingPattern() As String
    
    MatchingPatternRaw = Cells(StartRow, MatchPatternCol).Value2
    If MatchingPatternRaw = "EXPIRED MCID" _
    Or MatchingPatternRaw = "Expired MCID" _
    Then Exit Function

    If Len(MatchingPatternRaw) > 5 _
    Then
        If Mid(MatchingPatternRaw, 3, 1) = "," Or Mid(MatchingPatternRaw, 6, 1) = "," _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, ",")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = "." Or Mid(MatchingPatternRaw, 6, 1) = "." _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, ".")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = "<" Or Mid(MatchingPatternRaw, 6, 1) = "<" _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, "<")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = "'" Or Mid(MatchingPatternRaw, 6, 1) = "'" _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, "'")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = " " Or Mid(MatchingPatternRaw, 6, 1) = " " _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, " ")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = "C" Or Mid(MatchingPatternRaw, 6, 1) = "C" _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, "C")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        ElseIf Mid(MatchingPatternRaw, 3, 1) = "+" And Mid(MatchingPatternRaw, 4, 2) <> "PO" _
        Then
            MatchingPattern() = Split(MatchingPatternRaw, "+")
            AnalystReview = UCase(Replace(MatchingPattern(0), " ", vbNullString))
        Else
            AnalystReview = vbNullString
        End If
    Else
        AnalystReview = vbNullString
    End If
    
    AnalystReview = FPPOOnly(AnalystReview)
    FindAnalystReview = AnalystReview

End Function
