Attribute VB_Name = "Subroutines"
Option Explicit
Option Private Module

Sub PreCheck1()

    Dim SwappedName As String
    Dim SwappedNames() As String
    Dim NamesSwapped As Boolean
    Dim TestCounter As Integer
    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim ExactNameCounter As Integer
    Dim DeleteCounter As Integer
    Dim NameFrequency As Integer
    
    NamesSwapped = False
    NameFrequency = 0
    
    If AmbiguousFN = True _
    Or AmbiguousLN = True _
    Then
        For NameCounter = 0 To UBound(MidInitials)
            ReDim Preserve FirstNames(0 To UBound(FirstNames) + 1)
            FirstNames(UBound(FirstNames)) = MidInitials(NameCounter)
            ReDim Preserve AmbiguousFNs(0 To UBound(AmbiguousFNs) + 1)
            AmbiguousFNs(UBound(AmbiguousFNs)) = False
        Next NameCounter
        FirstName = vbNullString
        For NameCounter = 0 To UBound(FirstNames)
            FirstName = FirstName & FirstNames(NameCounter)
        Next NameCounter
        ReDim MidInitials(0 To 0)
        AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
        If HardAmbiguousFN = True Or HardAmbiguousLN = True Then Exit Sub
    End If
    
    For NameCounter = 0 To UBound(ExactNames, 2)
        If Replace(ExactNames(0, NameCounter), " ", "") = FirstName _
        And Replace(ExactNames(1, NameCounter), " ", "") = LastName _
        Then
            NameFrequency = ExactNames(3, NameCounter)
            Exit For
        End If
    Next
    
'        If FirstName = LastName And IsInArrayString(CStr(FirstName), People(Counter).FirstNameFull) = True _
'        Then LastName = vbNullString
'
'        If FirstName = LastName And IsInArrayString(CStr(LastName), People(Counter).LastNameFull) = True _
'        Then FirstName = vbNullString
    
    For ExactNameCounter = 0 To UBound(ExactNames, 2)
    
        ExactFirstNames = SplitName(ExactNames(0, ExactNameCounter))
        ExactLastNames = SplitName(ExactNames(1, ExactNameCounter))
        ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
        ReDim AmbiguousExactLNs(0 To UBound(ExactLastNames))
        AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
        AmbiguousExactLN = AmbiguousLNCheck(ExactLastNames, AmbiguousExactLNs)

        If IsInEachArray(FirstNames, ExactLastNames, "LeftInside", "Either") = True _
        And IsInEachArray(LastNames, ExactFirstNames, "LeftInside", "Either") = True _
        And ExactNames(3, ExactNameCounter) > NameFrequency _
        And IsInEachArray(FirstNames, LastNames, "LeftInside", "Either") = False _
        And CStr(ExactNames(1, ExactNameCounter)) <> vbNullString _
        Then
        
'            For TestCounter = 0 To UBound(ExactFirstNames)
'                Debug.Print ExactFirstNames(TestCounter)
'            Next
'
'            For TestCounter = 0 To UBound(ExactLastNames)
'                Debug.Print ExactLastNames(TestCounter)
'            Next
'
'            For TestCounter = 0 To UBound(FirstNames)
'                Debug.Print FirstNames(TestCounter)
'            Next
'
'            For TestCounter = 0 To UBound(LastNames)
'                Debug.Print LastNames(TestCounter)
'            Next
        
            ReDim SwappedNames(0 To UBound(FirstNames))
            For NameCounter = 0 To UBound(SwappedNames)
                SwappedNames(NameCounter) = FirstNames(NameCounter)
            Next
            ReDim FirstNames(0 To UBound(LastNames))
            ReDim AmbiguousFNs(0 To UBound(FirstNames))
            For NameCounter = 0 To UBound(FirstNames)
                FirstNames(NameCounter) = LastNames(NameCounter)
            Next
            ReDim LastNames(0 To UBound(SwappedNames))
            ReDim AmbiguousLNs(0 To UBound(LastNames))
            For NameCounter = 0 To UBound(LastNames)
                LastNames(NameCounter) = SwappedNames(NameCounter)
            Next
            SwappedName = FirstName
            FirstName = LastName
            LastName = SwappedName
            If MCID <> People(PersonCounter).MCID Then FNLNSwapped = True
            Exit For
            
        ElseIf IsInEachArray(FirstNames, ExactLastNames, "LeftInside", "Either", , , AmbiguousFNs, AmbiguousExactLNs) = True _
        And IsInEachArray(LastNames, ExactLastNames, "LeftInside", "Either", , , AmbiguousLNs, AmbiguousExactLNs) = True _
        And ExactNames(3, ExactNameCounter) > NameFrequency _
        And CStr(ExactNames(1, ExactNameCounter)) <> vbNullString _
        And CStr(ExactNames(1, ExactNameCounter)) <> LastName _
        Then
                
            For NameCounter = 0 To UBound(FirstNames)
                ReDim Preserve LastNames(UBound(LastNames) + 1)
                LastNames(UBound(LastNames)) = FirstNames(NameCounter)
            Next
            ReDim AmbiguousLNs(0 To UBound(LastNames))
            For NameCounter = 0 To UBound(FirstNames)
                LastName = FirstNames(NameCounter) & LastName
            Next
            FirstName = MidInitials(0)
            MidInitials(0) = vbNullString
            ReDim FirstNames(0 To 0)
            ReDim AmbiguousFNs(0 To 0)
            AmbiguousFNs(0) = True
'                    For NameCounter = 0 To UBound(LastNames)
'                        If IsInArrayExactString(CStr(LastNames(NameCounter)), ExactFirstNames) _
'                        Then
'                            If FirstNames(0) <> vbNullString Then ReDim Preserve FirstNames(UBound(FirstNames) + 1)
'                            FirstNames(UBound(FirstNames)) = LastNames(NameCounter)
'                            FirstName = FirstName & LastNames(NameCounter)
'                            LastNames(NameCounter) = vbNullString
'                        End If
'                    Next
'                    LastName = vbNullString
'                    For NameCounter = 0 To UBound(LastNames)
'                        LastName = LastName & LastNames(NameCounter)
'                    Next
'                    ReDim AmbiguousFNs(0 To UBound(FirstNames))
            Exit For
            
        ElseIf IsInEachArray(LastNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousLNs, AmbiguousExactFNs) = True _
        And IsInEachArray(FirstNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousFNs, AmbiguousExactFNs) = True _
        And ExactNames(3, ExactNameCounter) > NameFrequency _
        And CStr(ExactNames(0, ExactNameCounter)) <> vbNullString _
        And CStr(ExactNames(1, ExactNameCounter)) <> FirstName _
        Then
            For NameCounter = 0 To UBound(LastNames)
                ReDim Preserve FirstNames(UBound(FirstNames) + 1)
                FirstNames(UBound(FirstNames)) = LastNames(NameCounter)
            Next NameCounter
            ReDim AmbiguousFNs(0 To UBound(FirstNames))
            For NameCounter = 0 To UBound(LastNames)
                FirstName = FirstName & LastNames(NameCounter)
            Next NameCounter
            LastName = vbNullString
            ReDim LastNames(0 To 0)
            ReDim AmbiguousLNs(0 To 0)
            AmbiguousLNs(0) = True
'                    For NameCounter = 0 To UBound(LastNames)
'                        If IsInArrayExactString(CStr(LastNames(NameCounter)), ExactFirstNames) _
'                        Then
'                            If FirstNames(0) <> vbNullString Then ReDim Preserve FirstNames(UBound(FirstNames) + 1)
'                            FirstNames(UBound(FirstNames)) = LastNames(NameCounter)
'                            FirstName = FirstName & LastNames(NameCounter)
'                            LastNames(NameCounter) = vbNullString
'                        End If
'                    Next
'                    LastName = vbNullString
'                    For NameCounter = 0 To UBound(LastNames)
'                        LastName = LastName & LastNames(NameCounter)
'                    Next
'                    ReDim AmbiguousFNs(0 To UBound(FirstNames))
            Exit For
        End If
    Next
    
    For ExactNameCounter = 0 To UBound(ExactNames, 2)
    
        ExactFirstNames = SplitName(ExactNames(0, ExactNameCounter))
        ExactLastNames = SplitName(ExactNames(1, ExactNameCounter))
        ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
        ReDim AmbiguousExactLNs(0 To UBound(ExactLastNames))
        AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
        AmbiguousExactLN = AmbiguousLNCheck(ExactLastNames, AmbiguousExactLNs)

        If IsInEachArray(LastNames, ExactLastNames, "LeftInside", "Either", , , AmbiguousLNs, AmbiguousExactLNs) = True _
        And IsInEachArray(LastNames, ExactFirstNames, "LeftInside", "Either", , , AmbiguousLNs, AmbiguousExactFNs) = True _
        And ExactNames(3, ExactNameCounter) > NameFrequency _
        And CStr(ExactNames(1, ExactNameCounter)) <> vbNullString _
        And CStr(ExactNames(1, ExactNameCounter)) <> LastName _
        Then
            For NameCounter = 0 To UBound(LastNames)
                If IsInArray(LastNames(NameCounter), ExactFirstNames, "Exact") _
                Then
                    If FirstNames(0) <> vbNullString Then ReDim Preserve FirstNames(UBound(FirstNames) + 1)
                    FirstNames(UBound(FirstNames)) = LastNames(NameCounter)
                    FirstName = FirstName & LastNames(NameCounter)
                    LastNames(NameCounter) = vbNullString
                End If
            Next NameCounter
            LastName = vbNullString
            For NameCounter = 0 To UBound(LastNames)
                LastName = LastName & LastNames(NameCounter)
            Next NameCounter
            ReDim AmbiguousFNs(0 To UBound(FirstNames))
            Exit For
        End If
    Next
    
    AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
    NamesSwapped = False
            
    If UBound(FirstNames) = 0 _
    Then
    
        For NameCounter = 0 To UBound(MidInitials)
            If Len(MidInitials(NameCounter)) > 2 _
            Or AmbiguousFN = True _
            Then
                For NameCounterAgain = 0 To UBound(FirstNames)
                    If Len(FirstNames(NameCounterAgain)) < 3 _
                    And IsInArray(FirstNames(NameCounterAgain), ShortNames, "Exact") = False _
                    Then
                        For ExactNameCounter = 0 To UBound(ExactNames, 2)
                            ExactFirstNames = SplitName(ExactNames(0, ExactNameCounter))
                            ReDim AmbiguousExactFNs(0 To UBound(ExactFirstNames))
                            AmbiguousExactFN = AmbiguousFNCheck(ExactFirstNames, AmbiguousExactFNs)
                            If IsInArray(FirstNames(NameCounterAgain), ExactFirstNames, "Exact") = True _
                            And ExactNames(3, ExactNameCounter) > NameFrequency _
                            Then
                                SwappedName = FirstNames(NameCounterAgain)
                                FirstNames(NameCounterAgain) = MidInitials(NameCounter)
                                MidInitials(NameCounter) = SwappedName
                                NamesSwapped = True
                                Exit For
                            End If
                        Next ExactNameCounter
                    End If
                Next NameCounterAgain
            End If
        Next NameCounter

        If NamesSwapped = True _
        Then
            SwappedName = FirstName
            FirstName = MidInitial
            MidInitial = SwappedName
            AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
        End If
        
    ElseIf UBound(FirstNames) > 0 _
    Then
    
        For NameCounter = UBound(FirstNames) To 0 Step -1
            If UBound(FirstNames) = 0 Then Exit For
            
'            And UBound(FirstNames) > 0) _

            If (Len(FirstNames(NameCounter)) < 3 _
            And IsNumeric(FirstNames(NameCounter)) = False _
            And AmbiguousFN = False _
            And AmbiguousFNs(NameCounter) = False _
            And IsInArray(3, FirstNames, "Length", "LeftLessThanRight") = True _
            And IsInArray(FirstNames(NameCounter), ShortNames, "Exact") = False) _
            Then
                If MidInitials(0) = vbNullString _
                Then
                    MidInitials(0) = Left(FirstNames(NameCounter), 1)
                Else
                    ReDim Preserve MidInitials(UBound(MidInitials) + 1)
                    MidInitials(UBound(MidInitials)) = Left(FirstNames(NameCounter), 1)
                End If
                For DeleteCounter = NameCounter To UBound(FirstNames) - 1
                    FirstNames(DeleteCounter) = FirstNames(DeleteCounter + 1)
                Next DeleteCounter
                ReDim Preserve FirstNames(UBound(FirstNames) - 1)
                For DeleteCounter = NameCounter To UBound(AmbiguousFNs) - 1
                    AmbiguousFNs(DeleteCounter) = AmbiguousFNs(DeleteCounter + 1)
                Next DeleteCounter
                ReDim Preserve AmbiguousFNs(UBound(AmbiguousFNs) - 1)
                FirstName = vbNullString
                For DeleteCounter = 0 To UBound(FirstNames)
                    FirstName = FirstName & FirstNames(DeleteCounter)
                Next DeleteCounter
            End If
        Next NameCounter
        AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
        
    End If
    
    AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)

End Sub

Sub PreCheck2()

    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim DeleteCounter As Integer
    Dim NonAmbiguousNameFound As Boolean
    Dim ExactNameCounter As Integer

    NonAmbiguousNameFound = False
    If UBound(FirstNames) > 0 And IsEmpty(UBound(People)) = False _
    Then
        For NameCounter = UBound(FirstNames) To 0 Step -1
            If UBound(FirstNames) = 0 Then Exit For
            If IsInArrayCompleteSpecialCombined(FirstNames, MidInitials, ExactNames) = True _
            And IsInArrayLengthSpecial(2, FirstNames, FirstNames(NameCounter)) = True _
            And AmbiguousFN = False _
            And IsNumeric(FirstNames(NameCounter)) = False _
            Then
                For ExactNameCounter = 0 To UBound(ExactNames, 2)
                    If Replace(ExactNames(0, ExactNameCounter), " ", "") = FirstName _
                    And Replace(ExactNames(1, ExactNameCounter), " ", "") = LastName _
                    Then Exit Sub
                Next
                For NameCounterAgain = 0 To NameCounter
                    If AmbiguousFNs(NameCounterAgain) = False _
                    Then
                        NonAmbiguousNameFound = True
                        Exit For
                    End If
                Next NameCounterAgain
                If NonAmbiguousNameFound = True _
                Then
                    If MidInitials(0) = vbNullString _
                    Then
                        MidInitials(0) = Left(FirstNames(NameCounter), 1)
                    Else
                        ReDim Preserve MidInitials(UBound(MidInitials) + 1)
                        MidInitials(UBound(MidInitials)) = Left(FirstNames(NameCounter), 1)
                    End If
                    For DeleteCounter = NameCounter To UBound(FirstNames) - 1
                        FirstNames(DeleteCounter) = FirstNames(DeleteCounter + 1)
                    Next DeleteCounter
                    ReDim Preserve FirstNames(UBound(FirstNames) - 1)
                    For DeleteCounter = NameCounter To UBound(AmbiguousFNs) - 1
                        AmbiguousFNs(DeleteCounter) = AmbiguousFNs(DeleteCounter + 1)
                    Next DeleteCounter
                    ReDim Preserve AmbiguousFNs(UBound(AmbiguousFNs) - 1)
                    FirstName = vbNullString
                    For DeleteCounter = 0 To UBound(FirstNames)
                        FirstName = FirstName & FirstNames(DeleteCounter)
                    Next DeleteCounter
                End If
            End If
        Next NameCounter
    End If
    
    AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)

End Sub

Sub PreCheck3()

    Dim NameCounter As Integer
    Dim NameCounterAgain As Integer
    Dim DeleteCounter As Integer
    
    If UBound(FirstNames) > 0 And IsEmpty(UBound(People)) = False _
    Then
        For NameCounter = UBound(FirstNames) To 0 Step -1
            If UBound(FirstNames) = 0 Then Exit For
            If Len(FirstNames(NameCounter)) = 1 _
            And IsNumeric(FirstNames(NameCounter)) = False _
            And AmbiguousFN = False _
            And UBound(FirstNames) > 0 _
            Then
                If MidInitials(0) = vbNullString _
                Then
                    MidInitials(0) = Left(FirstNames(NameCounter), 1)
                Else
                    ReDim Preserve MidInitials(UBound(MidInitials) + 1)
                    MidInitials(UBound(MidInitials)) = Left(FirstNames(NameCounter), 1)
                End If
                For DeleteCounter = NameCounter To UBound(FirstNames) - 1
                    FirstNames(DeleteCounter) = FirstNames(DeleteCounter + 1)
                Next DeleteCounter
                ReDim Preserve FirstNames(UBound(FirstNames) - 1)
                For DeleteCounter = NameCounter To UBound(AmbiguousFNs) - 1
                    AmbiguousFNs(DeleteCounter) = AmbiguousFNs(DeleteCounter + 1)
                Next DeleteCounter
                ReDim Preserve AmbiguousFNs(UBound(AmbiguousFNs) - 1)
                FirstName = vbNullString
                For DeleteCounter = 0 To UBound(FirstNames)
                    FirstName = FirstName & FirstNames(DeleteCounter)
                Next DeleteCounter
            End If
        Next NameCounter
    End If
    
    AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
    
End Sub

Sub ScrapeRecord()

    Dim FirstNameApostrophe As String
    Dim LastNameApostrophe As String
    Dim NameCounter As Integer

    SourceKey = Cells(CurrentRow, SourceKeyCol).Value2
    MCID = Cells(CurrentRow, MCIDCol).Value2
    NameStatus = Cells(CurrentRow, NameStatusCol).Value2
    FirstName = Replace(Cells(CurrentRow, FirstNameCol).Value2, "&APOS;", "'")
    FirstNameExact = FirstName
    For NameCounter = 0 To UBound(ExactNames, 2)
        If InStr(ExactNames(0, NameCounter), " ") <> 0 _
        Then
            If FirstName = Replace(ExactNames(0, NameCounter), " ", vbNullString) _
            Then FirstName = ExactNames(0, NameCounter)
        End If
    Next
    FirstNameApostrophe = Replace(FirstName, "'", vbNullString, , 1)
    FirstNameApostrophe = Replace(FirstName, " ", vbNullString)
    For NameCounter = 0 To UBound(ExactNames, 2)
        If InStr(ExactNames(0, NameCounter), "'") <> 0 _
        Then
            If FirstNameApostrophe = Replace(ExactNames(0, NameCounter), "'", vbNullString) _
            Then
                FirstName = Replace(FirstName, " ", vbNullString, , 1)
                FirstName = Replace(FirstName, "'", vbNullString)
            End If
        End If
    Next
    FirstName = FormatName(FirstName)
'    If InStr(FirstName, " ") <> 0 _
'    Then
        FirstNames = SplitName(FirstName)
'    Else
'        ReDim FirstNames(0 To 0)
'        FirstNames(0) = FirstName
'    End If
    FirstName = Replace(FirstName, " ", vbNullString)
    LastName = Replace(Cells(CurrentRow, LastNameCol).Value2, "&APOS;", "'")
    LastNameExact = LastName
    For NameCounter = 0 To UBound(ExactNames, 2)
        If InStr(ExactNames(1, NameCounter), " ") <> 0 _
        Then
            If LastName = Replace(ExactNames(1, NameCounter), " ", vbNullString) _
            Then LastName = ExactNames(1, NameCounter)
        End If
    Next
    LastNameApostrophe = Replace(LastName, "'", vbNullString, , 1)
    LastNameApostrophe = Replace(LastName, " ", vbNullString, , 1)
    For NameCounter = 0 To UBound(ExactNames, 2)
        If InStr(ExactNames(1, NameCounter), "'") <> 0 _
        Then
            If LastNameApostrophe = Replace(ExactNames(1, NameCounter), "'", vbNullString) _
            Then
                LastName = Replace(LastName, " ", vbNullString, , 1)
                LastName = Replace(LastName, "'", vbNullString, , 1)
            End If
        End If
    Next
    LastName = FormatName(LastName)
    LastNames = SplitName(LastName)
    LastName = Replace(LastName, " ", vbNullString)
    MidInitial = Cells(CurrentRow, MidInitialCol).Value2
    If MidInitial = "" _
    Or MidInitial = "NA" _
    Or MidInitial = "NOT" _
    Then MidInitial = vbNullString
    MidInitial = FormatName(MidInitial)
    MidInitials = SplitName(MidInitial)
    BirthDateText = Cells(CurrentRow, BirthDateCol).Value2
    If IsNumeric(Cells(CurrentRow, BirthDateCol).Value2) = True _
    Or IsDate(Cells(CurrentRow, BirthDateCol).Value2) = True _
    Then
        BirthDate = Cells(CurrentRow, BirthDateCol).Value2
'        If IsDate(Cells(CurrentRow, BirthDateCol).Value2 - 2) = True _
'        Then BirthDate = CDate(Cells(CurrentRow, BirthDateCol - 2).Value2)
    End If
    If IsDate(BirthDate) = True _
    Then
        BirthDateMonth = CStr(Format(BirthDate, "mm"))
        BirthDateDay = CStr(Format(BirthDate, "dd"))
        BirthDateYear = CStr(Format(BirthDate, "yyyy"))
    End If
    AmbiguousDOB = AmbiguousDOBCheck(BirthDate)
    Gender = Cells(CurrentRow, GenderCol).Value2
    SubscriberID = Cells(CurrentRow, SubscriberIDCol).Value2
    SSN = Cells(CurrentRow, SSNCol).Value2
    AmbiguousSSN = AmbiguousSSNCheck(SSN)
    HCID = Cells(CurrentRow, HCIDCol).Value2
    ReDim AmbiguousFNs(0 To UBound(FirstNames)) As Boolean
    ReDim SoftAmbiguousFN(0 To UBound(FirstNames)) As Boolean
    ReDim AmbiguousLNs(0 To UBound(LastNames)) As Boolean
    ReDim SoftAmbiguousLN(0 To UBound(LastNames)) As Boolean
    
End Sub

Sub RedimensionArray()

    Dim NameCounter As Integer
    
    PersonCounter = PersonCounter + 1
    PersonNumber = PersonCounter
    
    If PersonNumber > 1 Then ReDim Preserve People(0 To UBound(People) + 1)
    Do
        If PeopleKeys.Exists(PersonKey) = True Then PersonKey = PersonKey + 0.1
    Loop Until PeopleKeys.Exists(PersonKey) = False
    
'    If People(UBound(People)).PersonNumber <> 0 _
'    Then ReDim Preserve People(LBound(People) To UBound(People) + 1)
'    Do
'        If PeopleKeys.Exists(PersonKey) = True Then PersonKey = PersonKey + 0.1
'    Loop Until PeopleKeys.Exists(PersonKey) = False
    PeopleKeys.Add PersonKey, People(Counter).ActiveRecords
    People(UBound(People)).PersonKey = PersonKey
    People(UBound(People)).PersonNumber = PersonNumber
    People(UBound(People)).SourceKey() = SourceKey
    People(UBound(People)).MCID = MCID
    People(UBound(People)).NameStatus() = NameStatus
    People(UBound(People)).FirstNameFull() = FirstName
    People(UBound(People)).AmbiguousFNFull = AmbiguousFN
    If IsEmpty(UBound(FirstNames)) = False _
    Then
        For NameCounter = 0 To UBound(FirstNames)
            People(UBound(People)).FirstName() = FirstNames(NameCounter)
        Next
    End If
    If IsEmpty(UBound(AmbiguousFNs)) = False _
    Then
        For NameCounter = 0 To UBound(AmbiguousFNs)
            People(UBound(People)).AmbiguousFN() = AmbiguousFNs(NameCounter)
        Next
    End If
    People(UBound(People)).LastNameFull() = LastName
    People(UBound(People)).AmbiguousLNFull = AmbiguousLN
    If IsEmpty(UBound(LastNames)) = False _
    Then
        For NameCounter = 0 To UBound(LastNames)
            People(UBound(People)).LastName() = LastNames(NameCounter)
        Next
    End If
    If IsEmpty(UBound(AmbiguousLNs)) = False _
    Then
        For NameCounter = 0 To UBound(AmbiguousLNs)
            People(UBound(People)).AmbiguousLN() = AmbiguousLNs(NameCounter)
        Next
    End If
    For NameCounter = 0 To UBound(MidInitials)
        People(UBound(People)).MidInitial() = MidInitials(NameCounter)
    Next
    People(UBound(People)).Gender = Gender
    People(UBound(People)).BirthDate() = CStr(BirthDate)
    People(UBound(People)).AmbiguousDOB() = AmbiguousDOB
    People(UBound(People)).SubscriberID() = SubscriberID
    People(UBound(People)).SSN() = SSN
    People(UBound(People)).AmbiguousSSN() = AmbiguousSSN
    People(UBound(People)).HCID() = HCID
    People(UBound(People)).HardAmbiguousFN = HardAmbiguousFN
    People(UBound(People)).HardAmbiguousLN = HardAmbiguousLN
    If NameStatus = "A" Then People(UBound(People)).ActiveRecords = True
    PeopleKeys(PersonKey) = People(UBound(People)).ActiveRecords
    People(UBound(People)).ExpiredMCID = ExpiredMCID
    
End Sub

Sub RecordPeople()

    Dim Counter As Integer
    Dim PrintCounter As Integer
    
    For Counter = 1 To UBound(People)
        Print #2,
        Print #2, "Person Key: "
        Print #2, CStr(People(Counter).PersonKey)
        Print #2,
        Print #2, "Person Number: "
        Print #2, CStr(People(Counter).PersonNumber)
        Print #2,
        Print #2, "Source Key: "
        For PrintCounter = 0 To UBound(People(Counter).SourceKey())
            Print #2, CStr(People(Counter).SourceKey(PrintCounter))
        Next
        Print #2,
        Print #2, "MCID: "
        Print #2, CStr(People(Counter).MCID)
        Print #2,
        Print #2, "Name Status: "
        For PrintCounter = 0 To UBound(People(Counter).NameStatus())
            Print #2, CStr(People(Counter).NameStatus(PrintCounter))
        Next
        Print #2,
        Print #2, "Active Records: "
        Print #2, CStr(People(Counter).ActiveRecords)
        Print #2,
        Print #2, "First Name Full: "
        For PrintCounter = 0 To UBound(People(Counter).FirstNameFull())
            Print #2, CStr(People(Counter).FirstNameFull(PrintCounter))
        Next
        Print #2,
        Print #2, "Full Ambiguous First Name: "
        Print #2, CStr(People(Counter).AmbiguousFNFull)
        Print #2,
        Print #2, "First Name: "
        For PrintCounter = 0 To UBound(People(Counter).FirstName())
            Print #2, CStr(People(Counter).FirstName(PrintCounter))
        Next
        Print #2,
        Print #2, "Ambiguous First Name: "
        For PrintCounter = 0 To UBound(People(Counter).AmbiguousFN())
            Print #2, CStr(People(Counter).AmbiguousFN(PrintCounter))
        Next
        Print #2,
        Print #2, "Last Name Full: "
        For PrintCounter = 0 To UBound(People(Counter).LastNameFull())
            Print #2, CStr(People(Counter).LastNameFull(PrintCounter))
        Next
        Print #2,
        Print #2, "Full Ambiguous Last Name: "
        Print #2, CStr(People(Counter).AmbiguousLNFull)
        Print #2,
        Print #2, "Last Name: "
        For PrintCounter = 0 To UBound(People(Counter).LastName())
            Print #2, CStr(People(Counter).LastName(PrintCounter))
        Next
        Print #2,
        Print #2, "Ambiguous Last Name: "
        For PrintCounter = 0 To UBound(People(Counter).AmbiguousLN())
            Print #2, CStr(People(Counter).AmbiguousLN(PrintCounter))
        Next
        Print #2,
        Print #2, "Middle Initial: "
        For PrintCounter = 0 To UBound(People(Counter).MidInitial())
            Print #2, CStr(People(Counter).MidInitial(PrintCounter))
        Next
        Print #2,
        Print #2, "Date of Birth: "
        For PrintCounter = 0 To UBound(People(Counter).BirthDate())
            Print #2, CStr(People(Counter).BirthDate(PrintCounter))
        Next
        Print #2,
        Print #2, "Ambiguous DOB: "
        For PrintCounter = 0 To UBound(People(Counter).AmbiguousDOB())
            Print #2, CStr(People(Counter).AmbiguousDOB(PrintCounter))
        Next
        Print #2,
        Print #2, "Gender: "
        For PrintCounter = 0 To UBound(People(Counter).Gender())
            Print #2, CStr(People(Counter).Gender(PrintCounter))
        Next
        Print #2,
        Print #2, "Subscriber ID: "
        For PrintCounter = 0 To UBound(People(Counter).SubscriberID())
            Print #2, CStr(People(Counter).SubscriberID(PrintCounter))
        Next
        Print #2,
        Print #2, "Social Security Number: "
        For PrintCounter = 0 To UBound(People(Counter).SSN())
            Print #2, CStr(People(Counter).SSN(PrintCounter))
        Next
        Print #2,
        Print #2, "Ambiguous SSN: "
        For PrintCounter = 0 To UBound(People(Counter).AmbiguousSSN())
            Print #2, CStr(People(Counter).AmbiguousSSN(PrintCounter))
        Next
        Print #2,
        Print #2, "Health Care ID: "
        For PrintCounter = 0 To UBound(People(Counter).HCID())
            Print #2, CStr(People(Counter).HCID(PrintCounter))
        Next
        Print #2,
        Print #2, "Hard Ambiguous FN: "
        Print #2, CStr(People(Counter).HardAmbiguousFN)
        Print #2,
        Print #2, "Hard Ambiguous LN: "
        Print #2, CStr(People(Counter).HardAmbiguousLN)
        Print #2,
        Print #2, "Expired MCID: "
        Print #2, CStr(People(Counter).ExpiredMCID)
    Next
    Print #2,
    Print #2,
    
End Sub

Sub ReCheck(FalseNegativeCheck As Boolean)

    Dim PersonCounter As Integer, PersonCounterAgain As Integer, HighCounter As Integer, NameCounter As Integer, DateCounter As Integer
    
    FNDiffersMCID = False
    LNDiffersMCID = False
    MIDiffersMCID = True
    DOBDiffersMCID = False

    For PersonCounter = UBound(People) - 1 To 1 Step -1
        For PersonCounterAgain = UBound(People) To PersonCounter + 1 Step -1

            If CInt(People(PersonCounter).PersonKey) <> CInt(People(PersonCounterAgain).PersonKey) _
            And People(PersonCounter).ExpiredMCID = False And People(PersonCounterAgain).ExpiredMCID = False _
            Then
                
                FNDiffers = False
                LNDiffers = False
                MIDiffers = True
                DOBDiffers = False
                AmbiguousFirst = False
                AmbiguousLast = False
                
                ReDim FirstNames(0 To UBound(People(PersonCounter).FirstName))
                ReDim AmbiguousFNs(LBound(People(PersonCounter).AmbiguousFN) To UBound(People(PersonCounter).AmbiguousFN))
                For NameCounter = 0 To UBound(People(PersonCounter).FirstName)
                    FirstNames(NameCounter) = People(PersonCounter).FirstName(NameCounter)
                    AmbiguousFNs(NameCounter) = People(PersonCounter).AmbiguousFN(NameCounter)
                Next NameCounter
                AmbiguousFN = AmbiguousFNCheck(FirstNames, AmbiguousFNs)
                FirstName = People(PersonCounter).FirstNameFull(0)
                SourceKey = People(PersonCounter).SourceKey(0)
                FNDiffers = FirstNameCheck(FirstNames, AmbiguousFNs, PersonCounterAgain, FalseNegativeCheck)
'                If FalseNegativeReview = True And FNDiffers = True _
'                Then
'                    For NameCounter = 0 To UBound(People(PersonCounter).FirstName)
'                        If UBound(DifferentNames) > 0 _
'                        Then ReDim Preserve DifferentNames(LBound(DifferentNames) To UBound(DifferentNames) + 1)
'                        DifferentNames(UBound(DifferentNames)) = People(PersonCounter).FirstName(NameCounter)
'                    Next NameCounter
'                End If
                
                If MarriageRule = True _
                Then
                    If IsInArray(vbNullString, People(PersonCounter).LastNameFull, "Exact") = True _
                    Or IsInArray(vbNullString, People(PersonCounterAgain).LastNameFull, "Exact") = True _
                    Then LNDiffers = True
                    If (People(PersonCounter).AmbiguousLNFull = True Or People(PersonCounterAgain).AmbiguousLNFull = True) _
                    Then
                        AmbiguousLast = True
                        LNDiffers = True
                        If (IsInEachArray(LastNames, People(PersonCounter).LastName, "LeftInside", "Either", True, "LeftEqualsRight", , , , , 1) = True _
                        Or IsInArray(LastName, People(PersonCounter).LastNameFull, "Exact") = True) _
                        And (AmbiguousLN = True And People(PersonCounter).AmbiguousLNFull = True) _
                        Then
                            LNDiffers = False
                            If AmbiguousLN = True And People(PersonCounter).AmbiguousLNFull = True _
                            And IsInArray(SourceKey, People(PersonCounter).SourceKey, "Exact") = True _
                            Then AmbiguousLast = False
                        End If
                    End If
                Else
                    ReDim LastNames(0 To UBound(People(PersonCounter).LastName))
                    ReDim AmbiguousLNs(0 To UBound(People(PersonCounter).AmbiguousLN))
                    For NameCounter = 0 To UBound(People(PersonCounter).LastName)
                        LastNames(NameCounter) = People(PersonCounter).LastName(NameCounter)
                        AmbiguousLNs(NameCounter) = People(PersonCounter).AmbiguousLN(NameCounter)
                    Next
                    AmbiguousLN = AmbiguousLNCheck(LastNames, AmbiguousLNs)
                    LastName = People(PersonCounter).FirstNameFull(0)
                    LNDiffers = LastNameCheck(LastNames, AmbiguousLNs, PersonCounterAgain)
                End If
                
                For DateCounter = 0 To UBound(People(PersonCounter).BirthDate)
                    DOBDiffers = BirthDateCheck(CDate(People(PersonCounter).BirthDate(DateCounter)), PersonCounterAgain)
                    If DOBDiffers = True _
                    Then Exit For
                Next
    
                If IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "LeftFoundInRight") = True _
                Or IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "RightFoundInLeft") = True _
                Or IsInArray(vbNullString, People(PersonCounter).MidInitial, "Exact", , True) = True _
                Or IsInArray(vbNullString, People(PersonCounterAgain).MidInitial, "Exact", , True) = True _
                Then
                    If (PeopleKeys.Exists(CDec(CInt(People(PersonCounter).PersonKey)) + 0.2) = True _
                    Or PeopleKeys.Exists(CDec(CInt(People(PersonCounterAgain).PersonKey)) + 0.2) = True) _
                    Then
                        If (IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "LeftFoundInRight") = True _
                        And IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "RightFoundInLeft") = True) _
                        Or (IsInArray(vbNullString, People(PersonCounter).MidInitial, "Exact", , True) = True _
                        And PeopleKeys.Exists(CDec(CInt(People(PersonCounterAgain).PersonKey)) + 0.2) = False _
                        And PeopleKeys.Exists(CDec(CInt(People(PersonCounter).PersonKey)) + 0.2) = False) _
                        Or (IsInArray(vbNullString, People(PersonCounterAgain).MidInitial, "Exact", , True) = True _
                        And PeopleKeys.Exists(CDec(CInt(People(PersonCounter).PersonKey)) + 0.2) = False _
                        And PeopleKeys.Exists(CDec(CInt(People(PersonCounterAgain).PersonKey)) + 0.2) = False) _
                        Then MIDiffers = False
                    Else
                        MIDiffers = False
                        If People(PersonCounter).MCID <> People(PersonCounterAgain).MCID Then MIMatch = True
                    End If
                End If
    
                If FNDiffers = True _
                Then FNDiffersMCID = True
                If LNDiffers = True _
                Then LNDiffersMCID = True
                If MIDiffers = False _
                Then MIDiffersMCID = False
                If DOBDiffers = True _
                Then DOBDiffersMCID = True
                If AmbiguousFirst = True _
                Then AmbiguousFNMCID = True
                If AmbiguousLast = True _
                Then AmbiguousLNMCID = True
    
                If FNDiffers = False _
                And LNDiffers = False _
                And MIDiffers = False _
                And DOBDiffers = False _
                And (AmbiguousFirst = False _
                Or (AmbiguousFirst = True _
                And IsInEachArray(People(PersonCounter).SourceKey, People(PersonCounterAgain).SourceKey, "Exact") = True)) _
                And (AmbiguousLast = False _
                Or (AmbiguousLast = True _
                And IsInEachArray(People(PersonCounter).SourceKey, People(PersonCounterAgain).SourceKey, "Exact") = True)) _
                And AmbiguousBirth = False _
                And People(PersonCounter).ExpiredMCID = People(PersonCounterAgain).ExpiredMCID _
                Then Call CombinePeople(PersonCounter, PersonCounterAgain, StartRow, EndRow)
                
            End If
            
        Next
    Next

    MidInitialCounter = 0
    If UBound(People) > 1 _
    Then
        For PersonCounter = 1 To UBound(People) - 1
            For PersonCounterAgain = PersonCounter + 1 To UBound(People)
                If IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "LeftFoundInRight") = False _
                And IsInEachArray(People(PersonCounter).MidInitial, People(PersonCounterAgain).MidInitial, "Left", , True, "RightFoundInLeft") = False _
                And IsInArray(vbNullString, People(PersonCounter).MidInitial, "Exact", , True) = False _
                And IsInArray(vbNullString, People(PersonCounterAgain).MidInitial, "Exact", , True) = False _
                And People(PersonCounter).ExpiredMCID = False And People(PersonCounterAgain).ExpiredMCID = False _
                Then
                    MidInitialCounter = MidInitialCounter + 1
                    Exit For
                End If
            Next
        Next
        If MidInitialCounter > 0 _
        Then MIDiffersMCID = True
    End If
 
End Sub

Sub FalseNegativeComments(ByVal TaskIsFP As Boolean, ByVal TaskIsPO As Boolean, ByVal TaskStartRow As Integer, ByVal TaskEndRow As Integer)

    Dim RowCounter As Integer
    Dim RowCounterAgain As Integer
    Dim HighCounter As Integer
    Dim MatchNumber As Integer
    Dim ActivePeople As Integer
    Dim ExpiredMCIDs As Integer
    Dim MultipleMCIDFlag As String
    Dim MatchingPatternBuild As New StringBuilder
    Dim MatchingPattern As String
    Dim FNComment As String
    Dim DOBComment As String
    Dim LNComment As String
    Dim MIComment As String
    Dim SSNComment As String
    Dim FNCommentSingle As String
    Dim DOBCommentSingle As String
    Dim LNCommentSingle As String
    Dim MICommentSingle As String

    HighCounter = 0
    MatchNumber = 0
    ActivePeople = 0
    ExpiredMCIDs = 0
    MultipleMCIDFlag = vbNullString
    DOBComment = vbNullString
    LNComment = vbNullString
    MIComment = vbNullString
    SSNComment = vbNullString
    MatchingPatternBuild.Class_Initialize

    Call FalsePositiveReview(TaskStartRow, TaskEndRow, TaskStartRow, TaskEndRow, True)
    
    If FNLNSwapped = True _
    Then
        FNComment = "Transposed(Swapped) FN & LN"
        LNComment = vbNullString
    ElseIf FNNickname = True _
    Then
        FNComment = "FN_Nickname"
    ElseIf FNTypo = True _
    Then
        FNComment = "FN_Typo"
    ElseIf FNMatch = True _
    Then
        FNComment = "FN_Match"
    Else
        FNComment = "Different_FN"
    End If
    If AmbiguousDOBTask = True _
    Then
        DOBComment = "DOB_Anonymous"
    Else
        If DOBTypo = True _
        Then
            DOBComment = "DOB_Typo"
        ElseIf DOBMatch = True _
        Then
            DOBComment = "DOB_Match"
        Else
            DOBComment = "FAMILY-DOB-LT-14"
        End If
    End If
    If LNTypo = True _
    Then
        LNComment = "LN_Changed_After_Marriage"
    ElseIf LNMarriage = True _
    Then
        LNComment = "LN_Changed_After_Marriage"
    ElseIf LNMatch = True _
    Then
        LNComment = "LN_Match"
    Else
        LNComment = "Different_LN"
    End If
    If MIMissing = True _
    Then
        MIComment = "NO_MI"
    ElseIf MIMatch = True _
    Then
        MIComment = "Same_MI"
    Else
        MIComment = "Different_MI"
    End If
    If SSNMissing = True _
    Then
        SSNComment = "NO_SSN"
    ElseIf SSNMatch = True _
    Then
        SSNComment = "Same_SSN"
    Else
        SSNComment = "Different_SSN"
    End If
    
    For PersonCounter = 1 To UBound(People)
        If People(PersonCounter).ActiveRecords = True _
        Then ActivePeople = ActivePeople + 1
        If People(PersonCounter).ExpiredMCID = True _
        Then
            ExpiredMCIDs = ExpiredMCIDs + 1
            ExpiredMCIDsCount = ExpiredMCIDsCount + 1
        End If
    Next
    
    If ActivePeople = 1 _
    And ExpiredMCIDs = 0 _
    Then
        MatchingPatternBuild.Append (FNComment)
        MatchingPatternBuild.Append (" + ")
        MatchingPatternBuild.Append (DOBComment)
        MatchingPatternBuild.Append (" + ")
        MatchingPatternBuild.Append (LNComment)
        If LNComment <> vbNullString Then MatchingPatternBuild.Append (" + ")
        If MIComment = "Same_MI" Or MIComment = "NO_MI" Then MIComment = "Same/NO_MI"
        MatchingPatternBuild.Append (MIComment)
        MatchingPatternBuild.Append (" + ")
        MatchingPatternBuild.Append (SSNComment)
        MatchingPatternBuild.Append (" + ID_Cross_Match")
        MatchingPattern = MatchingPatternBuild.ToString
        For RowCounterAgain = TaskStartRow To TaskEndRow
            If FalseNegativeAnalysis = True _
            Then
                Cells(RowCounterAgain, IsFNAnalystCol).Value2 = "Y"
                Cells(RowCounterAgain, MatchPatternCol).Value2 = MatchingPattern
            ElseIf FalseNegativeReview = True _
            Then
                If Cells(RowCounterAgain, IsFNAnalystCol).Value2 <> "Y" _
                Then
                    Cells(RowCounterAgain, IsFNReviewerCol).Value2 = "Y"
                    Cells(RowCounterAgain, MatchPatternReviewerCol).Value2 = MatchingPattern
                End If
            End If
            Cells(RowCounterAgain, MCIDPairFlagCol).Value2 = "1"
        Next
    ElseIf ActivePeople > 1 _
    And ExpiredMCIDs < ActivePeople - 1 _
    Then
        HighCounter = Cells(TaskStartRow, FNSequenceCol).Value2
        For RowCounter = TaskStartRow + 1 To TaskEndRow
            If Cells(RowCounter, FNSequenceCol).Value2 > HighCounter _
            And Cells(RowCounter, FNSequenceCol).Value2 <> "0" _
            Then
                HighCounter = Cells(RowCounter, FNSequenceCol).Value2
            ElseIf Cells(RowCounter, FNSequenceCol).Value2 <= HighCounter _
            And Cells(RowCounter, MCIDCol).Value2 <> Cells(RowCounter - 1, MCIDCol).Value2 _
            And Cells(RowCounter, CommentsCol).Value2 <> "expired MCID" _
            And Cells(RowCounter - 1, CommentsCol).Value2 <> "expired MCID" _
            Then
                MatchNumber = Cells(RowCounter, FNSequenceCol).Value2
                If FNComment = "Different_FN" _
                Then
                    FNCommentSingle = "FN_Match"
                Else
                    FNCommentSingle = FNComment
                End If
                If DOBComment = "FAMILY-DOB-LT-14" _
                Then
                    FNCommentSingle = "DOB_Match"
                Else
                    FNCommentSingle = DOBComment
                End If
                If LNComment = "Different_LN" _
                Then
                    FNCommentSingle = "LN_Match"
                Else
                    FNCommentSingle = LNComment
                End If
                If MIComment = "Different_MI" _
                Then
                    FNCommentSingle = "Same/NO_MI"
                Else
                    FNCommentSingle = MIComment
                End If
                MatchingPatternBuild.Append (FNCommentSingle)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (DOBCommentSingle)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (LNCommentSingle)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (MICommentSingle)
                MatchingPatternBuild.Append (" + ID_Cross_Match")
                MatchingPattern = MatchingPatternBuild.ToString
                For RowCounterAgain = TaskStartRow To TaskEndRow
                    If Cells(RowCounterAgain, FNSequenceCol).Value2 = MatchNumber _
                    Then
                        If FalseNegativeAnalysis = True _
                        Then
                            Cells(RowCounterAgain, IsFNAnalystCol).Value2 = "Y"
                        ElseIf FalseNegativeReview = True _
                        Then
                            If Cells(RowCounterAgain, IsFNAnalystCol).Value2 = "N" _
                            Then
                                Cells(RowCounterAgain, IsFNReviewerCol).Value2 = "Y"
                            End If
                        End If
                        Cells(RowCounterAgain, MCIDPairFlagCol).Value2 = "1"
                        If TaskIsFP = True Or TaskIsPO = True _
                        Then
                            MultipleMCIDFlag = "S FALSE POSITIVE"
                        Else
                            MultipleMCIDFlag = "Single"
                        End If
                    End If
                Next RowCounterAgain
            End If
        Next RowCounter
        For RowCounter = TaskStartRow To TaskEndRow
            Cells(RowCounter, MultipleMCIDFlagCol).Value2 = MultipleMCIDFlag
        Next RowCounter
        If MultipleMCIDFlag = vbNullString _
        Then
            MatchingPatternBuild.Class_Initialize
            If AmbiguousFNMCID = True Or AmbiguousLNMCID = True _
            Then
                MatchingPatternBuild.Append ("FN_Anonymous")
                Select Case DOBComment
                    Case Is = "FAMILY-DOB-LT-14"
                        DOBComment = "DOB_MISMATCH"
                    Case Is = "DOB_Match"
                        DOBComment = "DOB_MATCH"
                End Select
                If MIComment = "Same_MI" _
                Then
                    If SSNComment = "Same_SSN" Then SSNComment = "SSN_Match"
                ElseIf MIComment = "NO_MI" _
                Then
                    If SSNComment = "Same_SSN" Then SSNComment = "SSN Match"
                End If
                If DOBComment = "DOB_MISMATCH" _
                Then
                    MIComment = "NO_MI"
                    SSNComment = "NO_SSN"
                End If
                If SSNComment = "Different_SSN" Then SSNComment = "NO_SSN"
            ElseIf AmbiguousDOBMCID = True _
            Then
                MatchingPatternBuild.Append ("DOB_Anonymous")
            ElseIf DOBDiffersMCID = True _
            Then
                MatchingPatternBuild.Append ("FAMILY-DOB-LT-14")
                Select Case FNComment
                    Case Is = "FN_Match"
                        FNComment = "SAME_FN"
                    Case Is = "FN_Typo"
                        FNComment = "Similar_FN"
                    Case Is = "FN_Nickname"
                        FNComment = "Similar_FN"
                    Case Is = "Different_FN"
                        FNComment = "DIFFRENT_FN"
                End Select
                Select Case LNComment
                    Case Is = "LN_Match"
                        LNComment = "SAME_LN"
                    Case Is = "LN_Changed_After_Marriage"
                        LNComment = "SAME_LN"
                    Case Is = "Different_LN"
                        LNComment = "Others + NO_SSN"
                End Select
                Select Case MIComment
                    Case Is = "Same_MI"
                        MIComment = "SAME_MI"
                    Case Is = "NO_MI"
                        MIComment = "no_MI"
                End Select
                If FNComment = "Similar_FN" Or FNComment = "DIFFRENT_FN" Then MIComment = vbNullString
            ElseIf LNDiffersMCID = True _
            Then
                MatchingPatternBuild.Append ("Non-FAMILY")
'                Select Case FNComment
'                    Case Is = "FN_Match"
                        FNComment = "Same_FN"
'                    Case Is = "FN_Nickname"
'                        FNComment = "Same_FN"
'                    Case Is = "FN_Typo"
'                        FNComment = "Same_FN"
'                End Select
                Select Case LNComment
                    Case Is = "LN_Changed_after_Marriage"
                        LNComment = "Same_LN"
                    Case Is = "LN_Match"
                        LNComment = "Same_LN"
                End Select
                Select Case DOBComment
                    Case Is = "FAMILY-DOB-LT-14"
                        DOBComment = "Different_DOB"
                    Case Is = "DOB_Match"
                        DOBComment = "Same_DOB"
                End Select
            ElseIf TwinsGenderDiffers = True _
            Then
                MatchingPatternBuild.Append ("TWINS DIFFERENT_FN_WITH_DIFFERENT_GENDER")
            Else
                MatchingPatternBuild.Append ("TWINS DIFFERENT_FN_WITH_SAME_GENDER")
            End If
            MatchingPattern = MatchingPatternBuild.ToString
            If MatchingPattern = "FN_Anonymous" _
            Then
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (DOBComment)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (MIComment)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (SSNComment)
            ElseIf MatchingPattern = "DOB_Anonymous" _
            Then
                MatchingPatternBuild.Append (" ")
                MatchingPatternBuild.Append (FNComment)
            ElseIf MatchingPattern = "FAMILY-DOB-LT-14" _
            Then
                If LNComment = "Others + NO_SSN" _
                Then
                    MatchingPatternBuild.Append (" ")
                    MatchingPatternBuild.Append (LNComment)
                Else
                    MatchingPatternBuild.Append (" ")
                    MatchingPatternBuild.Append (FNComment)
                    MatchingPatternBuild.Append (" + ")
                    MatchingPatternBuild.Append (LNComment)
                    If MIComment <> vbNullString _
                    Then
                        MatchingPatternBuild.Append (" + ")
                        MatchingPatternBuild.Append (MIComment)
                    End If
                    If SSNComment = "Different_SSN" _
                    Then
                        MatchingPatternBuild.Append (" + ")
                        MatchingPatternBuild.Append (SSNComment)
                    End If
                End If
            ElseIf MatchingPattern = "Non-FAMILY" _
            Then
                MatchingPatternBuild.Append (" ")
                MatchingPatternBuild.Append (FNComment)
                MatchingPatternBuild.Append (" + ")
                MatchingPatternBuild.Append (LNComment)
                If LNComment = "Same_LN" _
                Then
                    MatchingPatternBuild.Append (" + ")
                    MatchingPatternBuild.Append (MIComment)
                    MatchingPatternBuild.Append (" + ")
                    MatchingPatternBuild.Append (DOBComment)
                End If
            End If
            MatchingPattern = MatchingPatternBuild.ToString
            For RowCounterAgain = TaskStartRow To TaskEndRow
                If FalseNegativeAnalysis = True _
                Then
                    Cells(RowCounterAgain, IsFNAnalystCol).Value2 = "N"
                    Cells(RowCounterAgain, MatchPatternCol).Value2 = MatchingPatternBuild.ToString
                ElseIf FalseNegativeReview = True _
                Then
                    If Cells(RowCounterAgain, IsFNAnalystCol).Value2 <> "N" _
                    Then
                        Cells(RowCounterAgain, IsFNReviewerCol).Value2 = "N"
                        Cells(RowCounterAgain, MatchPatternReviewerCol).Value2 = MatchingPatternBuild.ToString
                    End If
                End If
            Next
        End If
    End If

End Sub
