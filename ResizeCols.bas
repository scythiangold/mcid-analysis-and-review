Option Explicit
 
Sub ResizeCols()

    Dim ReviewCol As Integer, CommentsCol As Integer, FPSequence1Col As Integer, FNSequenceCol As Integer
    Dim IsFNAnalystCol As Integer, MatchPatternCol As Integer, IsFNReviewerCol As Integer, MatchPatternReviewerCol As Integer
    Dim IsPOCol As Integer, MCIDPairFlagCol As Integer, MultipleMCIDFlagCol As Integer, FPSequence2Col As Integer
    Dim TaskIDCol As Integer, MCIDCol As Integer, SourceKeyCol As Integer, NameStatusCol As Integer
    Dim FirstNameCol As Integer, LastNameCol As Integer, MidInitialCol As Integer, BirthDateCol As Integer
    Dim GenderCol As Integer, SubscriberIDCol As Integer, SSNCol As Integer, HCIDCol As Integer
    Dim AddressCol As Integer, CityCol As Integer, StateCol As Integer, ZipCodeCol As Integer
    Dim ColCounter As Integer, FinalRow As Integer
    
    FinalRow = 2
    
    For ColCounter = 1 To 78
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEW")) <> 0 _
        And Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEWER")) = 0 _
        Then ReviewCol = ColCounter
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "COMMENT")) <> 0 _
        Then CommentsCol = ColCounter
        If (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        And FPSequence1Col = 0 _
        Then
            FPSequence1Col = ColCounter
        ElseIf FPSequence1Col <> 0 _
        And (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        Then
            FPSequence2Col = ColCounter
        End If
        If (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FN_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FN SEQ")) <> 0) _
        Then FNSequenceCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_FN_ANALYST" Then IsFNAnalystCol = ColCounter
        If (InStr(Cells(1, ColCounter).Value2, "MATCH") <> 0 And InStr(Cells(1, ColCounter).Value2, "PATTERN") <> 0 _
        And InStr(Cells(1, ColCounter).Value2, "REVIEWER") = 0) _
        Or InStr(Cells(1, ColCounter).Value2, "DENSE") <> 0 Then MatchPatternCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_FN_REVIEWER" Then IsFNReviewerCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MATCHING_PATTERN_REVIEWER" Then MatchPatternReviewerCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_PO" Then IsPOCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MCID_PAIR_FLAG" Then MCIDPairFlagCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MULTIPLE_MCID_FLAG" Then MultipleMCIDFlagCol = ColCounter
        If Cells(1, ColCounter).Value2 = "TASK_ID" Then TaskIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MCID" Then MCIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SRC_KEY" Or Cells(1, ColCounter).Value2 = "MEMIDNUM" _
        Then SourceKeyCol = ColCounter
        If Cells(1, ColCounter).Value2 = "NAME_STATUS" Or Cells(1, ColCounter).Value2 = "RECSTAT" Then NameStatusCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "FIRST") <> 0 And InStr(Cells(1, ColCounter).Value2, "DENSE") = 0 _
        Then FirstNameCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "LAST") <> 0 Then LastNameCol = ColCounter
        If InStr(Cells(1, ColCounter).Value2, "MIDDLE") <> 0 Or InStr(Cells(1, ColCounter).Value2, "INITIAL") <> 0 _
        Then MidInitialCol = ColCounter
        If Cells(1, ColCounter).Value2 = "GENDER" Or Cells(1, ColCounter).Value2 = "SEX" Then GenderCol = ColCounter
        If Cells(1, ColCounter).Value2 = "DOB" Then BirthDateCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SUBSCRBID" Or Cells(1, ColCounter).Value2 = "SBSCRBR_ID" Then SubscriberIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "SSN" Or Cells(1, ColCounter).Value2 = "TAXID" Then SSNCol = ColCounter
        If Cells(1, ColCounter).Value2 = "HCID" Then HCIDCol = ColCounter
        If Cells(1, ColCounter).Value2 = "ADDRESS_LINE1" Then AddressCol = ColCounter
        If Cells(1, ColCounter).Value2 = "CITY" Then CityCol = ColCounter
        If Cells(1, ColCounter).Value2 = "STATE" Then StateCol = ColCounter
        If Cells(1, ColCounter).Value2 = "ZIP_CD" Then ZipCodeCol = ColCounter
    Next
    
    Do Until Cells(FinalRow + 1, MCIDCol).Value2 = vbNullString
        FinalRow = FinalRow + 1
    Loop
    
    With Application.ActiveWorkbook.ActiveSheet

        ActiveWindow.Zoom = 100
        ActiveSheet.Select
        FinalRow = Cells(Rows.Count, MCIDCol).End(xlUp).Row
        Rows(2).Insert
        Columns("A:AS").EntireColumn.Hidden = True
        Cells(2, ReviewCol).Value2 = "FP+PO"
        Cells(2, FPSequence1Col).Value2 = "10"
        Cells(2, FNSequenceCol).Value2 = "0"
        Cells(2, IsFNAnalystCol).Value2 = "N"
        Cells(2, IsFNReviewerCol).Value2 = "N"
        Cells(2, IsPOCol).Value2 = "FP+PO"
        Cells(2, MCIDPairFlagCol).Value2 = "0"
        Cells(2, MultipleMCIDFlagCol).Value2 = "S FALSE POSITIVE"
        Cells(2, FPSequence2Col).Font.Size = 11
        Cells(2, FPSequence2Col).Value2 = "10"
        Cells(2, TaskIDCol).Value2 = "10000000"
        Cells(2, NameStatusCol).Value2 = "A"
        Cells(2, MidInitialCol).Value2 = "NA"
        Cells(2, GenderCol).Value2 = "M"
        Cells(2, StateCol).Value2 = "WA"
        Range(Cells(2, ReviewCol), Cells(FinalRow + 1, ReviewCol)).Columns.AutoFit
        Range(Cells(2, CommentsCol), Cells(FinalRow + 1, CommentsCol)).Columns.AutoFit
        Range(Cells(2, FPSequence1Col), Cells(FinalRow + 1, FPSequence1Col)).Columns.AutoFit
        Range(Cells(2, FNSequenceCol), Cells(FinalRow + 1, FNSequenceCol)).Columns.AutoFit
        Range(Cells(2, IsFNAnalystCol), Cells(FinalRow + 1, IsFNAnalystCol)).Columns.AutoFit
        Range(Cells(2, MatchPatternCol), Cells(FinalRow + 1, MatchPatternCol)).Columns.AutoFit
        Range(Cells(2, IsFNReviewerCol), Cells(FinalRow + 1, IsFNReviewerCol)).Columns.AutoFit
        Range(Cells(2, MatchPatternReviewerCol), Cells(FinalRow + 1, MatchPatternReviewerCol)).Columns.AutoFit
        Range(Cells(2, IsPOCol), Cells(FinalRow + 1, IsPOCol)).Columns.AutoFit
        Range(Cells(2, MCIDPairFlagCol), Cells(FinalRow + 1, MCIDPairFlagCol)).Columns.AutoFit
        Range(Cells(2, MultipleMCIDFlagCol), Cells(FinalRow + 1, MultipleMCIDFlagCol)).Columns.AutoFit
        Range(Cells(2, FPSequence2Col), Cells(FinalRow + 1, FPSequence2Col)).Columns.AutoFit
        Range(Cells(2, TaskIDCol), Cells(FinalRow + 1, TaskIDCol)).Columns.AutoFit
        Range(Cells(2, MCIDCol), Cells(FinalRow + 1, MCIDCol)).Columns.AutoFit
        Range(Cells(2, SourceKeyCol), Cells(FinalRow + 1, SourceKeyCol)).Columns.AutoFit
        Range(Cells(2, NameStatusCol), Cells(FinalRow + 1, NameStatusCol)).Columns.AutoFit
        Range(Cells(2, FirstNameCol), Cells(FinalRow + 1, FirstNameCol)).Columns.AutoFit
        Range(Cells(2, LastNameCol), Cells(FinalRow + 1, LastNameCol)).Columns.AutoFit
        Range(Cells(2, MidInitialCol), Cells(FinalRow + 1, MidInitialCol)).Columns.AutoFit
        Range(Cells(2, GenderCol), Cells(FinalRow + 1, GenderCol)).Columns.AutoFit
        Range(Cells(2, BirthDateCol), Cells(FinalRow + 1, BirthDateCol)).Columns.AutoFit
        Range(Cells(2, SSNCol), Cells(FinalRow + 1, SSNCol)).Columns.AutoFit
        Range(Cells(2, SubscriberIDCol), Cells(FinalRow + 1, SubscriberIDCol)).Columns.AutoFit
        Range(Cells(2, HCIDCol), Cells(FinalRow + 1, HCIDCol)).Columns.AutoFit
        Range(Cells(2, AddressCol), Cells(FinalRow + 1, AddressCol)).Columns.AutoFit
        Range(Cells(2, CityCol), Cells(FinalRow + 1, CityCol)).Columns.AutoFit
        Range(Cells(2, StateCol), Cells(FinalRow + 1, StateCol)).Columns.AutoFit
        Range(Cells(2, ZipCodeCol), Cells(FinalRow + 1, ZipCodeCol)).Columns.AutoFit
        Rows(2).Delete
        
    End With

End Sub
