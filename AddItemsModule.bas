Attribute VB_Name = "AddItemsModule"
Option Explicit
Option Private Module

Sub AddItems()

    Dim HypocorismEntry() As String
    Dim MCIDCounter As Integer

    Call ReadFiletoArray("Hard Ambiguous Names 1.txt", HardAmbiguousNames1)
    Call ReadFiletoArray("Hard Ambiguous Names 2.txt", HardAmbiguousNames2)
    Call ReadFiletoArray("Soft Ambiguous Names 1.txt", SoftAmbiguousNames1)
    Call ReadFiletoArray("Soft Ambiguous Names 2.txt", SoftAmbiguousNames2)
    Call ReadFiletoArray("Short Names.txt", ShortNames)
    Call ReadFiletoArray("Diminutive Short Names.txt", DiminutiveShortNames)
    Call ReadFiletoArray("Ambiguous Exclusions.txt", AmbiguousExclusions)
    Call ReadFiletoArray("Chinese Family Names.txt", ChineseFamilyNames)
    Call ReadFiletoArrayDate("Ambiguous DOBs.txt", AmbiguousDOBs)
    
    If ExpiredMCIDCheck = True And ExpiredMCIDCheckManualOverride = True _
    Then
        Call ReadFiletoArray("Expired MCIDs.txt", CurrentValidMCIDs)
        For MCIDCounter = LBound(CurrentValidMCIDs) To UBound(CurrentValidMCIDs)
            CurrentValidMCIDs(MCIDCounter) = Replace(CurrentValidMCIDs(MCIDCounter), ",", "")
        Next MCIDCounter
    End If
    
    Hypocorisms.Add "Adelaide", "Heidi"
    Hypocorisms.Add "Alexander", "Alec, Sandy, Sasha"
    Hypocorisms.Add "Alexandra", "Sandra, Sandy, Sacha"
    Hypocorisms.Add "Alexandria", "Sandra, Sandy"
    Hypocorisms.Add "Alicia", "Cya"
    Hypocorisms.Add "Anneliese", "Lisa"
    Hypocorisms.Add "Ann", "Nancy"
    Hypocorisms.Add "Anne", "Nancy"
    Hypocorisms.Add "Antoinette", "Toni, Tony"
    Hypocorisms.Add "Barbara", "Babs"
    Hypocorisms.Add "Bridget", "Bee"
    Hypocorisms.Add "Catherine", "Kate, Katy, Katie"
    Hypocorisms.Add "Charles", "Chuck"
    Hypocorisms.Add "Cynthia", "Cindy"
    Hypocorisms.Add "Deborah", "Debra"
    Hypocorisms.Add "Dorothy", "Dot, Dottie, Dotty"
    Hypocorisms.Add "Dorothea", "Dot, Dottie, Dotty"
    Hypocorisms.Add "Edwin", "Ned, Ted, Teddy"
    Hypocorisms.Add "Edgar", "Ned, Ted, Teddy"
    Hypocorisms.Add "Edwina", "Ned, Ted, Teddy"
    Hypocorisms.Add "Edward", "Ned, Ted, Teddy"
    Hypocorisms.Add "Edmund", "Ned, Ted, Teddy"
    Hypocorisms.Add "Eleanor", "Nell, Nellie"
    Hypocorisms.Add "Elizabeth", "Elisa, Bess, Bessie, Libby"
    Hypocorisms.Add "Florence", "Flossie"
    Hypocorisms.Add "Geoffrey", "Jeff"
    Hypocorisms.Add "Gillian", "Jill"
    Hypocorisms.Add "Henry", "Harry, Hal, Hank"
    Hypocorisms.Add "Hubert", "Hugh"
    Hypocorisms.Add "James", "Jim, Jimbo, Jimmy"
    Hypocorisms.Add "Jane", "Jenny"
    Hypocorisms.Add "Jason", "Jace"
    Hypocorisms.Add "Jeremiah", "Jezz, Jermo"
    Hypocorisms.Add "Jeremy", "Gerry, Jezz, Jermo"
    Hypocorisms.Add "Jerome", "Jezz, Jermo"
    Hypocorisms.Add "Jocelyn", "Joss"
    Hypocorisms.Add "John", "Jack"
    Hypocorisms.Add "Jonathan", "Johnny"
    Hypocorisms.Add "Katherine", "Kitty"
    Hypocorisms.Add "Kathleen", "Kitty"
    Hypocorisms.Add "Kristen", "Krusty, Kitty"
    Hypocorisms.Add "Kristin", "Kitty"
    Hypocorisms.Add "Kristyn", "Kitty"
    Hypocorisms.Add "Lawrence", "Larry, Laurie"
    Hypocorisms.Add "Laurence", "Larry"
    Hypocorisms.Add "Leonard", "Lennie, Lenny"
    Hypocorisms.Add "Magdalene", "Maddie"
    Hypocorisms.Add "Magdalena", "Maddie"
    Hypocorisms.Add "Margaret", "Madge, Maggie, Megan, Meg, Meggie, Peg, Peggy, Molly"
    Hypocorisms.Add "Marjorie", "Madge, Maggie, Margie, Megan, Meg, Meggie, Peg, Peggy, Molly"
    Hypocorisms.Add "Mary", "Molly"
    Hypocorisms.Add "Megan", "Peg, Peggy"
    Hypocorisms.Add "Morrissey", "Moz"
    Hypocorisms.Add "Patricia", "Trish"
    Hypocorisms.Add "Patrick", "Paddy"
    Hypocorisms.Add "Priscilla", "Prill"
    Hypocorisms.Add "Reginald", "Rex"
    Hypocorisms.Add "Richard", "Dick, Dickie"
    Hypocorisms.Add "Robert", "Bob, Bobby, Robin"
    Hypocorisms.Add "Roberta", "Bobbie"
    Hypocorisms.Add "Roger", "Hodge"
    Hypocorisms.Add "Sarah", "Sadie, Sally"
    Hypocorisms.Add "Seymour", "Sy"
    Hypocorisms.Add "Stephanie", "Fanny, Stevie"
    Hypocorisms.Add "Stephen", "Steve, Stevie"
    Hypocorisms.Add "Thaddeus", "Tad"
    Hypocorisms.Add "Theodore", "Ted, Teddy, Tad"
    Hypocorisms.Add "Theresa", "Tess, Tessa, Terri, Terry, Teri"
    Hypocorisms.Add "Teresa", "Tess, Tessa"
    Hypocorisms.Add "Thomas", "Tom, Tommy"
    Hypocorisms.Add "Virginia", "Ginger"
    Hypocorisms.Add "William", "Bill, Billy, Wm"
    Hypocorisms.Add "Beatrice", "Trixie"
    Hypocorisms.Add "Cecilia", "Celia, Cissie, Cissy"
    Hypocorisms.Add "Chester", "Chet"
    Hypocorisms.Add "Christopher", "Kit"
    Hypocorisms.Add "Genevieve", "Jean, Jenny"
    Hypocorisms.Add "Gerard", "Jerry"
    Hypocorisms.Add "Gerret", "Gary"
    Hypocorisms.Add "Harold", "Hal"
    Hypocorisms.Add "Isaac", "Ike"
    Hypocorisms.Add "Johnathan", "Jon"
    Hypocorisms.Add "Kaitlyn", "Katie"
    Hypocorisms.Add "Katherin", "Kitty"
    Hypocorisms.Add "Kathryn", "Kitty"
    Hypocorisms.Add "Kristopher", "Kit"
    Hypocorisms.Add "Letitia", "Tish"
    Hypocorisms.Add "Lewis", "Lou"
    Hypocorisms.Add "Marguerite", "Margot, Margaux"
    Hypocorisms.Add "Martha", "Mardi"
    Hypocorisms.Add "Melissa", "Missy"
    Hypocorisms.Add "Misheal", "Shelly"
    Hypocorisms.Add "Reuben", "Rube"
    Hypocorisms.Add "Sylvester", "Sly"
    Hypocorisms.Add "Darren", "Darin"
    Hypocorisms.Add "Susan", "Suzanna, Suzanne"
    Hypocorisms.Add "Zachary", "Zackery"
    Hypocorisms.Add "Alejandra", "Alex"
    Hypocorisms.Add "Alejandr", "Alex"
    Hypocorisms.Add "Alejandro", "Alex"
    Hypocorisms.Add "Gerardo", "Jerry"
    Hypocorisms.Add "Miguel", "Mike"
    Hypocorisms.Add "Monica", "Monique"
    Hypocorisms.Add "Raymond", "Raymundo"
    Hypocorisms.Add "Sylvestre", "Sly"
    Hypocorisms.Add "Sharon", "Sherri"
    Hypocorisms.Add "Theadore", "Ted"
    Hypocorisms.Add "Mohammed", "Md"
    
End Sub

Sub ReadFiletoArray(Filename As String, Array1() As Variant)

    Dim Counter As Integer
    Dim LineText As String

    Counter = 1
    ReDim Array1(1 To 1)
    Open FilePath & Filename For Input As 1
    While Not EOF(1)
        If Counter > 1 Then ReDim Preserve Array1(LBound(Array1) To UBound(Array1) + 1)
        Line Input #1, LineText
        Array1(Counter) = LineText
        Counter = Counter + 1
    Wend
    Close #1

End Sub

Sub ReadFiletoArrayDate(Filename As String, Array1() As Date)

    Dim Counter As Integer
    Dim LineText As String

    Counter = 1
    ReDim Array1(1 To 1)
    Open FilePath & Filename For Input As 1
    While Not EOF(1)
        If Counter > 1 Then ReDim Preserve Array1(LBound(Array1) To UBound(Array1) + 1)
        Line Input #1, LineText
        Array1(Counter) = CDate(LineText)
        Counter = Counter + 1
    Wend
    Close #1

End Sub
