VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Person"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option ClassModule
Private pPersonKey As Double
Private pPersonNumber As Integer
Private pSourceKey() As Variant
Private pMCID As Variant
Private pNameStatus() As Variant
Private pFirstNameFull() As Variant
Private pFirstName() As Variant
Private pLastNameFull() As Variant
Private pLastName() As Variant
Private pMidInitial() As Variant
Private pGender() As Variant
Private pBirthDate() As Variant
Private pstrBirthDate() As Variant
Private pBirthDateMonth() As Variant
Private pBirthDateDay() As Variant
Private pBirthDateYear() As Variant
Private pAmbiguousDOB() As Variant
Private pSubscriberID() As Variant
Private pSSN() As Variant
Private pHCID() As Variant
Private pAmbiguousSSN() As Variant
Private pAmbiguousFN() As Variant
Private pAmbiguousLN() As Variant
Private pActiveRecords As Boolean
Private pAmbiguousFNFull As Boolean
Private pAmbiguousLNFull As Boolean
Private pHardAmbiguousFN As Boolean
Private pHardAmbiguousLN As Boolean
Private pExpiredMCID As Boolean

Public Property Get PersonKey() As Double

    PersonKey = pPersonKey
    
End Property

Public Property Let PersonKey(Value As Double)

    pPersonKey = Value
    
End Property

Public Property Get PersonNumber() As Integer

    PersonNumber = pPersonNumber
    
End Property

Public Property Let PersonNumber(Value As Integer)

    pPersonNumber = Value
    
End Property

Public Property Get SourceKey() As Variant

    SourceKey = pSourceKey
    
End Property

Public Property Let SourceKey(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pSourceKey) <> 0 Then
    
        upperLimit = UBound(pSourceKey) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pSourceKey(upperLimit) As Variant
    pSourceKey(upperLimit) = Value
    
End Property

Public Property Get MCID() As Variant

    MCID = pMCID
    
End Property

Public Property Let MCID(Value As Variant)

    pMCID = Value
    
End Property

Public Property Get NameStatus() As Variant

    NameStatus = pNameStatus
    
End Property

Public Property Let NameStatus(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pNameStatus) <> 0 Then
    
        upperLimit = UBound(pNameStatus) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pNameStatus(upperLimit) As Variant
    pNameStatus(upperLimit) = Value
    
End Property

Public Property Get ActiveRecords() As Boolean

    ActiveRecords = pActiveRecords
    
End Property

Public Property Let ActiveRecords(Value As Boolean)

    pActiveRecords = Value
    
End Property

Public Property Get FirstNameFull() As Variant

    FirstNameFull = pFirstNameFull
    
End Property

Public Property Let FirstNameFull(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pFirstNameFull) <> 0 Then
    
        upperLimit = UBound(pFirstNameFull) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pFirstNameFull(upperLimit) As Variant
    pFirstNameFull(upperLimit) = Value
    
End Property

Public Property Get FirstName() As Variant

    FirstName = pFirstName
    
End Property

Public Property Let FirstName(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pFirstName) <> 0 Then
    
        upperLimit = UBound(pFirstName) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pFirstName(upperLimit) As Variant
    pFirstName(upperLimit) = Value
    
End Property

Public Property Get LastNameFull() As Variant

    LastNameFull = pLastNameFull
    
End Property

Public Property Let LastNameFull(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pLastNameFull) <> 0 Then
    
        upperLimit = UBound(pLastNameFull) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pLastNameFull(upperLimit) As Variant
    pLastNameFull(upperLimit) = Value
    
End Property

Public Property Get LastName() As Variant

    LastName = pLastName
    
End Property

Public Property Let LastName(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pLastName) <> 0 Then
    
        upperLimit = UBound(pLastName) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pLastName(upperLimit) As Variant
    pLastName(upperLimit) = Value
    
End Property

Public Property Get MidInitial() As Variant

    MidInitial = pMidInitial
    
End Property

Public Property Let MidInitial(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pMidInitial) <> 0 Then
    
        upperLimit = UBound(pMidInitial) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pMidInitial(upperLimit) As Variant
    pMidInitial(upperLimit) = Value
    
End Property

Public Property Get Gender() As Variant

    Gender = pGender
    
End Property

Public Property Let Gender(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pGender) <> 0 Then
    
        upperLimit = UBound(pGender) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pGender(upperLimit) As Variant
    pGender(upperLimit) = Value
    
End Property

Public Property Get BirthDate() As Variant

    BirthDate = pBirthDate
    
End Property

Public Property Let BirthDate(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pBirthDate) <> 0 Then
    
        upperLimit = UBound(pBirthDate) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pBirthDate(upperLimit) As Variant
    pBirthDate(upperLimit) = Value
    
End Property

Public Property Get strBirthDate() As Variant

    strBirthDate = pstrBirthDate
    
End Property

Public Property Let strBirthDate(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pstrBirthDate) <> 0 Then
    
        upperLimit = UBound(pstrBirthDate) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pstrBirthDate(upperLimit) As Variant
    pstrBirthDate(upperLimit) = Value
    
End Property

Public Property Get BirthDateMonth() As Variant

    BirthDateMonth = pBirthDateMonth
    
End Property

Public Property Let BirthDateMonth(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pBirthDateMonth) <> 0 Then
    
        upperLimit = UBound(pBirthDateMonth) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pBirthDateMonth(upperLimit) As Variant
    pBirthDateMonth(upperLimit) = Value
    
End Property

Public Property Get BirthDateDay() As Variant

    BirthDateDay = pBirthDateDay
    
End Property

Public Property Let BirthDateDay(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pBirthDateDay) <> 0 Then
    
        upperLimit = UBound(pBirthDateDay) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pBirthDateDay(upperLimit) As Variant
    pBirthDateDay(upperLimit) = Value
    
End Property

Public Property Get BirthDateYear() As Variant

    BirthDateYear = pBirthDateYear
    
End Property

Public Property Let BirthDateYear(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pBirthDateYear) <> 0 Then
    
        upperLimit = UBound(pBirthDateYear) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pBirthDateYear(upperLimit) As Variant
    pBirthDateYear(upperLimit) = Value
    
End Property

Public Property Get AmbiguousDOB() As Variant

    AmbiguousDOB = pAmbiguousDOB
    
End Property

Public Property Let AmbiguousDOB(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pAmbiguousDOB) <> 0 Then
    
        upperLimit = UBound(pAmbiguousDOB) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pAmbiguousDOB(upperLimit) As Variant
    pAmbiguousDOB(upperLimit) = Value
    
End Property

Public Property Get SubscriberID() As Variant

    SubscriberID = pSubscriberID
    
End Property

Public Property Let SubscriberID(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pSubscriberID) <> 0 Then
    
        upperLimit = UBound(pSubscriberID) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pSubscriberID(upperLimit) As Variant
    pSubscriberID(upperLimit) = Value
    
End Property

Public Property Get SSN() As Variant

    SSN = pSSN
    
End Property

Public Property Let SSN(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pSSN) <> 0 Then
    
        upperLimit = UBound(pSSN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pSSN(upperLimit) As Variant
    pSSN(upperLimit) = Value
    
End Property

Public Property Get AmbiguousSSN() As Variant

    AmbiguousSSN = pAmbiguousSSN
    
End Property

Public Property Let AmbiguousSSN(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pAmbiguousSSN) <> 0 Then
    
        upperLimit = UBound(pAmbiguousSSN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pAmbiguousSSN(upperLimit) As Variant
    pAmbiguousSSN(upperLimit) = Value
    
End Property

Public Property Get HCID() As Variant

    HCID = pHCID
    
End Property

Public Property Let HCID(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pHCID) <> 0 Then
    
        upperLimit = UBound(pHCID) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pHCID(upperLimit) As Variant
    pHCID(upperLimit) = Value
    
End Property

Public Property Get AmbiguousFN() As Variant

    AmbiguousFN = pAmbiguousFN
    
End Property

Public Property Let AmbiguousFN(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pAmbiguousFN) <> 0 Then
    
        upperLimit = UBound(pAmbiguousFN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pAmbiguousFN(upperLimit) As Variant
    pAmbiguousFN(upperLimit) = Value
    
End Property

Public Property Get AmbiguousLN() As Variant

    AmbiguousLN = pAmbiguousLN
    
End Property

Public Property Let AmbiguousLN(Value As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pAmbiguousLN) <> 0 Then
    
        upperLimit = UBound(pAmbiguousLN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pAmbiguousLN(upperLimit) As Variant
    pAmbiguousLN(upperLimit) = Value
    
End Property

Public Property Get AmbiguousFNFull() As Boolean

    AmbiguousFNFull = pAmbiguousFNFull
    
End Property

Public Property Let AmbiguousFNFull(Value As Boolean)

    pAmbiguousFNFull = Value
    
End Property

Public Property Get AmbiguousLNFull() As Boolean

    AmbiguousLNFull = pAmbiguousLNFull
    
End Property

Public Property Let AmbiguousLNFull(Value As Boolean)

    pAmbiguousLNFull = Value
    
End Property

Public Property Get HardAmbiguousFN() As Boolean

    HardAmbiguousFN = pHardAmbiguousFN
    
End Property

Public Property Let HardAmbiguousFN(Value As Boolean)

    pHardAmbiguousFN = Value
    
End Property

Public Property Get HardAmbiguousLN() As Boolean

    HardAmbiguousLN = pHardAmbiguousLN
    
End Property

Public Property Let HardAmbiguousLN(Value As Boolean)

    pHardAmbiguousLN = Value
    
End Property

Public Property Get ExpiredMCID() As Boolean

    ExpiredMCID = pExpiredMCID
    
End Property

Public Property Let ExpiredMCID(Value As Boolean)

    pExpiredMCID = Value
    
End Property

Public Function CheckArrayInitialized() As Boolean

    CheckArrayInitialized = False
    If Not Not pSourceKey _
    Then CheckArrayInitialized = True

End Function
