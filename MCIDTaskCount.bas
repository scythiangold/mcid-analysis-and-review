Attribute VB_Name = "MCIDTaskCount"
Option Explicit

Sub MCIDSplitTasks()

    Dim RowCounter As Integer, ColCounter As Integer, TaskIDCol As Integer
    Dim ReviewCol As Integer, CommentsCol As Integer, FPSequence1Col As Integer, FNSequenceCol As Integer
    Dim IsFNAnalystCol As Integer, MatchPatternCol As Integer, IsFNReviewerCol As Integer, MatchPatternReviewerCol As Integer
    Dim IsPOCol As Integer, MCIDPairFlagCol As Integer, MultipleMCIDFlagCol As Integer, FPSequence2Col As Integer
    Dim TrimPosition As Integer
    Dim LastRow As Long, FinalRow As Long
    Dim FilePath As String, Filename As String, FileDate As String, FilenameModified As String, ComboFilename As String, FPFilename As String
    Dim TaskID As String, PreviousTaskID As String, FalsePositive As String
    Dim FilenameSplit() As String
    Const Row = 2

    Application.Calculation = xlCalculationManual
    Application.CutCopyMode = False
    Application.DisplayAlerts = False
    Application.EnableEvents = True
    Application.EnableCancelKey = xlInterrupt
    Application.ScreenUpdating = False

    For ColCounter = 1 To 15
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEW")) <> 0 _
        And Trim(InStr(UCase(Cells(1, ColCounter).Value2), "REVIEWER")) = 0 _
        Then ReviewCol = ColCounter
        If Trim(InStr(UCase(Cells(1, ColCounter).Value2), "COMMENT")) <> 0 _
        Then CommentsCol = ColCounter
        If (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        And FPSequence1Col = 0 _
        Then
            FPSequence1Col = ColCounter
        ElseIf FPSequence1Col <> 0 _
        And (Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP_SEQ")) <> 0 _
        Or Trim(InStr(UCase(Cells(1, ColCounter).Value2), "FP SEQ")) <> 0) _
        Then
            FPSequence2Col = ColCounter
        End If
        If (InStr(Cells(1, ColCounter).Value2, "MATCH") <> 0 And InStr(Cells(1, ColCounter).Value2, "PATTERN") <> 0 _
        And InStr(Cells(1, ColCounter).Value2, "REVIEWER") = 0) _
        Or InStr(Cells(1, ColCounter).Value2, "DENSE") <> 0 Then MatchPatternCol = ColCounter
        If Cells(1, ColCounter).Value2 = "FN_SEQUENCE" Or Cells(1, ColCounter).Value2 = "FN SEQUENCE" _
        Then FNSequenceCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_FN_ANALYST" Then IsFNAnalystCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_FN_REVIEWER" Then IsFNReviewerCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MATCHING_PATTERN_REVIEWER" Then MatchPatternReviewerCol = ColCounter
        If Cells(1, ColCounter).Value2 = "IS_PO" Then IsPOCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MCID_PAIR_FLAG" Then MCIDPairFlagCol = ColCounter
        If Cells(1, ColCounter).Value2 = "MULTIPLE_MCID_FLAG" Then MultipleMCIDFlagCol = ColCounter
        If Cells(1, ColCounter).Value2 = "TASK_ID" Then TaskIDCol = ColCounter
    Next
    
    With Application.ActiveWorkbook.ActiveSheet
    
        For RowCounter = Row To FinalRow
            If Cells(RowCounter, 3).Value2 <> vbNullString _
            And Cells(RowCounter, 4).Value2 <> vbNullString _
            Then
                Cells(RowCounter, 1).Value2 = Cells(RowCounter, 3).Value2
                Cells(RowCounter, 2).Value2 = Cells(RowCounter, 4).Value2
            End If
            For ColCounter = 1 To 45
                If IsNumeric(Cells(RowCounter, ColCounter).Value2) = True _
                Then Cells(RowCounter, ColCounter).Errors(3).Ignore = True
            Next
        Next
        
        ActiveSheet.AutoFilterMode = False
        ActiveSheet.Rows("1:1").Select
        
    End With
    
    FilePath = Workbooks("MCID Analysis and Review.xlsm").Sheets("Sheet1").Range("B3").Value2
    Filename = ActiveWorkbook.Name
    FileDate = Mid(Filename, 10, 8)
    FilenameSplit() = Split(Filename, "_")
    TrimPosition = Len(Filename) - (Len(FilenameSplit(UBound(FilenameSplit))) + 1 + Len(FilenameSplit(UBound(FilenameSplit) - 1)))
    FilenameModified = Left(Filename, TrimPosition)
    ComboFilename = FilenameModified & "COMBO.xlsx"
    FPFilename = "FN_ID_AGP_" & FileDate & "_" & FileDate & "_Tasks_" & Format(Date, "yyyymmdd") & ".xlsx"

    Workbooks.Add.SaveAs Filename:=FilePath & ComboFilename
    Workbooks.Add.SaveAs Filename:=FilePath & FPFilename
    
    Workbooks(ComboFilename).Sheets("Sheet1").Name = "AnalystTaskDetails"
    Workbooks(FPFilename).Sheets("Sheet1").Name = "Consolidated"
    
    Workbooks(Filename).Sheets("AnalystTaskDetails").Rows(1).Copy Workbooks(ComboFilename).Sheets("AnalystTaskDetails").Rows(1)
    Workbooks(Filename).Sheets("AnalystTaskDetails").Rows(1).Copy Workbooks(FPFilename).Sheets("Consolidated").Rows(LastRow + 1)
    
    Workbooks(FPFilename).Sheets.Add(After:=Sheets("Consolidated")).Name = "Sheet1"
    With Workbooks(FPFilename).Sheets("Sheet1")
        Cells(1, 1).Font.Bold = True
        Cells(1, 1).Font.Color = RGB(255, 255, 255)
        Cells(1, 1).Font.Name = "Arial"
        Cells(1, 1).Font.Size = 10
        Cells(1, 1).Interior.Color = RGB(0, 0, 0)
        Cells(1, 1).Value2 = "TASK_ID"
    End With
    
    RowCounter = Row
    PreviousTaskID = vbNullString
    Workbooks(FPFilename).Sheets("Sheet1").Cells(1, 2).Value2 = 0
    
    Do
        FalsePositive = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsPOCol).Value2
        If Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsFNReviewerCol).Value2 <> vbNullString _
        And Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, MatchPatternReviewerCol).Value2 <> vbNullString _
        And (FalsePositive <> "FP" And FalsePositive <> "PO" And FalsePositive <> "FP+PO") _
        Then
            Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsFNAnalystCol).Value2 = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsFNReviewerCol).Value2
            Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, MatchPatternCol).Value2 = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, MatchPatternReviewerCol).Value2
            Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsFNReviewerCol).Value2 = vbNullString
            Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, MatchPatternReviewerCol).Value2 = vbNullString
            
        End If
        TaskID = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, TaskIDCol).Value2
        PreviousTaskID = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, TaskIDCol).Value2
        FalsePositive = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsPOCol).Value2
        If TaskID <> PreviousTaskID And FalsePositive <> vbNullString _
        Then
            LastRow = Workbooks(FPFilename).Sheets("Sheet1").Cells(Rows.Count, 1).End(xlUp).Row
            Workbooks(FPFilename).Sheets("Sheet1").Cells(LastRow + 1, 1).Value2 = TaskID
            Workbooks(FPFilename).Sheets("Sheet1").Cells(1, 2).Value2 = Workbooks(FPFilename).Sheets("Sheet1").Cells(1, 2).Value2 + 1
        End If
        If Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, TaskIDCol).Value2 = vbNullString Then Exit Do
        PreviousTaskID = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, TaskIDCol).Value2
        FalsePositive = Workbooks(Filename).Sheets("AnalystTaskDetails").Cells(RowCounter, IsPOCol).Value2
        If FalsePositive = "FP" Or FalsePositive = "PO" Or FalsePositive = "FP+PO" _
        Then
            LastRow = Workbooks(FPFilename).Sheets("Consolidated").Cells(Rows.Count, 1).End(xlUp).Row
            Workbooks(Filename).Sheets("AnalystTaskDetails").Rows(RowCounter).Copy Workbooks(FPFilename).Sheets("Consolidated").Rows(LastRow + 1)
        Else
            LastRow = Workbooks(ComboFilename).Sheets("AnalystTaskDetails").Cells(Rows.Count, 1).End(xlUp).Row
            Workbooks(Filename).Sheets("AnalystTaskDetails").Rows(RowCounter).Copy Workbooks(ComboFilename).Sheets("AnalystTaskDetails").Rows(LastRow + 1)
        End If
        RowCounter = RowCounter + 1
    Loop
    
    Workbooks(FPFilename).Sheets("Consolidated").Columns("A:D").Insert
    Workbooks(FPFilename).Sheets("Consolidated").Range("A1:D1").Font.Bold = True
    Workbooks(FPFilename).Sheets("Consolidated").Range("A1:D1").Font.Color = RGB(255, 255, 255)
    Workbooks(FPFilename).Sheets("Consolidated").Range("A1:D1").Font.Name = "Arial"
    Workbooks(FPFilename).Sheets("Consolidated").Range("A1:D1").Font.Size = 10
    Workbooks(FPFilename).Sheets("Consolidated").Range("A1:D1").Interior.Color = RGB(0, 0, 0)
    Workbooks(FPFilename).Sheets("Consolidated").Cells(1, 1).Value2 = "REVIEW"
    Workbooks(FPFilename).Sheets("Consolidated").Cells(1, 2).Value2 = "COMMENTS"
    Workbooks(FPFilename).Sheets("Consolidated").Cells(1, 3).Value2 = "FP SEQUENCE"h
    
    Workbooks(ComboFilename).Save
    Workbooks(FPFilename).Save
    
    Workbooks(ComboFilename).Close
    Workbooks(FPFilename).Close
    
    Application.ScreenUpdating = True
    MsgBox "Done"

End Sub
