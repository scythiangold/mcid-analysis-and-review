Attribute VB_Name = "IsInArrayModule"
Option Explicit

Function IsInArray(ByVal ValueToSearchFor As Variant, ByRef ArrayToFindIn As Variant, _
Optional ByVal CompareType As Variant, Optional ByVal Direction As Variant, Optional ByVal Only As Boolean, _
Optional ByVal SearchValueAmbiguous As Variant, Optional ByRef FindArrayAmbiguous As Variant, Optional ByRef ArrayNotToFindIn As Variant, _
Optional ByVal Spread As Integer, Optional ByVal LengthLimit As Integer) As Boolean

    Dim ArrayCounter As Integer, ArrayCounterAgain As Integer, ArrayCounterNext As Integer
    
    If IsMissing(CompareType) = True _
    Then CompareType = 0
    If IsMissing(Direction) = True _
    Then Direction = 0
    If IsMissing(Only) = True _
    Then Only = False
    IsInArray = False
    If Only = True _
    Then IsInArray = True
    If IsMissing(SearchValueAmbiguous) = False _
    Then
        If SearchValueAmbiguous = False _
        Then Exit Function
    End If
    If IsMissing(ArrayNotToFindIn) = False _
    Then
        For ArrayCounterAgain = LBound(ArrayNotToFindIn) To UBound(ArrayNotToFindIn)
            If ValueToSearchFor = ArrayNotToFindIn(ArrayCounterAgain) _
            Then Exit Function
        Next
    End If
    For ArrayCounter = LBound(ArrayToFindIn) To UBound(ArrayToFindIn)
        If Only = True _
        Then
            IsInArray = IsInArrayLevelOnly(ValueToSearchFor, ArrayToFindIn(ArrayCounter), CompareType, Direction, ArrayCounter, FindArrayAmbiguous, ArrayNotToFindIn, _
            Spread, LengthLimit)
            If IsInArray = False _
            Then Exit Function
        Else
            IsInArray = IsInArrayLevel(ValueToSearchFor, ArrayToFindIn(ArrayCounter), CompareType, Direction, ArrayCounter, FindArrayAmbiguous, ArrayNotToFindIn, _
            Spread, LengthLimit)
            If IsMissing(ArrayNotToFindIn) = False _
            Then
                For ArrayCounterAgain = LBound(ArrayNotToFindIn) To UBound(ArrayNotToFindIn)
                    If ArrayToFindIn(ArrayCounter) = ArrayNotToFindIn(ArrayCounterAgain) _
                    Then
                        IsInArray = False
                        Exit Function
                    End If
                Next
            End If
            If IsInArray = True _
            Then Exit Function
        End If
    Next

End Function

Function IsInArrayLevel(ByVal ValueToSearchFor As Variant, ByVal ValueToFind As Variant, _
ByVal CompareType As Variant, ByVal Direction As Variant, ByVal ArrayCounter As Integer, _
Optional ByRef FindArrayAmbiguous As Variant, Optional ByRef ArrayNotToFindIn As Variant, _
Optional ByVal Spread As Integer, Optional ByVal LengthLimit As Integer) As Boolean

    If LengthLimit <> 0 _
    Then
        If Len(ValueToSearchFor) < LengthLimit Or Len(ValueToFind) < LengthLimit _
        Then Exit Function
    End If
    If IsMissing(FindArrayAmbiguous) = False _
    Then
        If FindArrayAmbiguous(ArrayCounter) = True _
        Then Exit Function
    End If
    Select Case CompareType
        Case Is = 0, Is = "Exact"
            If IsDate(ValueToSearchFor) = True And IsDate(ValueToFind) = True _
            Then
                If CDate(ValueToSearchFor) = CDate(ValueToFind) _
                Then IsInArrayLevel = True
            ElseIf VarType(ValueToSearchFor) = vbBoolean And VarType(ValueToFind) = vbBoolean _
            Then
                If CBool(ValueToSearchFor) = CBool(ValueToFind) _
                Then IsInArrayLevel = True
            Else
                If ValueToSearchFor = ValueToFind _
                Then IsInArrayLevel = True
            End If
        Case Is = 1, Is = "Left"
            If Left(ValueToSearchFor, 1) = Left(ValueToFind, 1) _
            Then IsInArrayLevel = True
        Case Is = 2, Is = "Inside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    Then IsInArrayLevel = True
                Case Is = 1, Is = "RightInLeft"
                    If InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    Then IsInArrayLevel = True
                Case Is = 2, Is = "Either"
                    If InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    Or InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    Then IsInArrayLevel = True
                Case Is = 3, Is = "Each"
                    If InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    And InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    Then IsInArrayLevel = True
            End Select
        Case Is = 3, Is = "LeftInside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    Then IsInArrayLevel = True
                Case Is = 1, Is = "RightInLeft"
                    If ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevel = True
                Case Is = 2, Is = "Either"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    Or ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevel = True
                Case Is = 3, Is = "Each"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    And ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevel = True
            End Select
        Case Is = 4, Is = "Length"
            If IsNumeric(ValueToSearchFor) = False _
            Then ValueToSearchFor = Len(ValueToSearchFor)
            If IsNumeric(ValueToFind) = False _
            Then ValueToFind = Len(ValueToFind)
            Select Case Direction
                Case Is = 0, Is = "LeftLessThanRight"
                    If ValueToSearchFor <= ValueToFind + Spread _
                    Then IsInArrayLevel = True
                Case Is = 1, Is = "RightLessThanLeft"
                    If ValueToSearchFor >= ValueToFind - Spread _
                    Then IsInArrayLevel = True
                Case Is = 2, Is = "Either"
                    If ValueToSearchFor <= ValueToFind + Spread _
                    Or ValueToSearchFor >= ValueToFind - Spread _
                    Then IsInArrayLevel = True
                Case Is = 3, Is = "Each"
                    If ValueToSearchFor <= ValueToFind + Spread _
                    And ValueToSearchFor >= ValueToFind - Spread _
                    Then IsInArrayLevel = True
            End Select
    End Select

End Function

Function IsInArrayLevelOnly(ByVal ValueToSearchFor As Variant, ByVal ValueToFind As Variant, _
ByVal CompareType As Variant, ByVal Direction As Variant, ByVal ArrayCounter As Integer, _
Optional ByRef FindArrayAmbiguous As Variant, Optional ByRef ArrayNotToFindIn As Variant, _
Optional ByVal Spread As Integer, Optional ByVal LengthLimit As Integer) As Boolean

    IsInArrayLevelOnly = True
    If LengthLimit <> 0 _
    Then
        If Len(ValueToSearchFor) < LengthLimit Or Len(ValueToFind) < LengthLimit _
        Then Exit Function
    End If
    If IsMissing(FindArrayAmbiguous) = False _
    Then
        If FindArrayAmbiguous(ArrayCounter) = True _
        Then Exit Function
    End If
    Select Case CompareType
        Case Is = 0, Is = "Exact"
            If ValueToSearchFor <> ValueToFind _
            Then IsInArrayLevelOnly = False
        Case Is = 1, Is = "Left"
            If Left(ValueToSearchFor, 1) <> Left(ValueToFind, 1) _
            Then IsInArrayLevelOnly = False
        Case Is = 2, Is = "Inside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If InStr(ValueToSearchFor, ValueToFind) = 0 _
                    Then IsInArrayLevelOnly = False
                Case Is = 1, Is = "RightInLeft"
                    If InStr(ValueToFind, ValueToSearchFor) = 0 _
                    Then IsInArrayLevelOnly = False
                Case Is = 2, Is = "Either"
                    If InStr(ValueToSearchFor, ValueToFind) = 0 _
                    Or InStr(ValueToFind, ValueToSearchFor) = 0 _
                    Then IsInArrayLevelOnly = False
                Case Is = 3, Is = "Each"
                    If InStr(ValueToSearchFor, ValueToFind) = 0 _
                    And InStr(ValueToFind, ValueToSearchFor) = 0 _
                    Then IsInArrayLevelOnly = False
            End Select
        Case Is = 3, Is = "LeftInside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If ValueToSearchFor <> Left(ValueToFind, Len(ValueToSearchFor)) _
                    Then IsInArrayLevelOnly = False
                Case Is = 1, Is = "RightInLeft"
                    If ValueToFind <> Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevelOnly = False
                Case Is = 2, Is = "Either"
                    If ValueToSearchFor <> Left(ValueToFind, Len(ValueToSearchFor)) _
                    Or ValueToFind <> Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevelOnly = False
                Case Is = 3, Is = "Each"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    And ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInArrayLevelOnly = False
            End Select
    End Select

End Function

Function IsInEachArray(ByRef ArrayToSearchIn As Variant, ByRef ArrayToFindIn As Variant, _
Optional ByVal CompareType As Variant, Optional ByVal Direction As Variant, Optional ByVal Complete As Boolean, _
Optional ByVal SubsetToFind As Variant, Optional ByRef SearchArrayAmbiguous As Variant, Optional ByRef FindArrayAmbiguous As Variant, _
Optional ByRef ArrayNotToFindIn As Variant, Optional ByVal Spread As Integer, Optional ByVal LengthLimit As Integer) As Boolean

    Dim ArrayCounter As Integer, ArrayCounterAgain As Integer, ArrayCounterNext As Integer
    Dim ArrayDeleteCounter As Integer, ArrayCounterLast As Integer, ArrayCounterAgainLast As Integer
    
    If IsMissing(CompareType) = True _
    Then CompareType = 0
    If IsMissing(Direction) = True _
    Then Direction = 0
    If IsMissing(Complete) = True _
    Then Complete = False
    If IsMissing(SubsetToFind) = True _
    Then SubsetToFind = 0
'    If IsMissing(Spread) = True _
'    Then Spread = 0
    IsInEachArray = False
    If Complete = True _
    Then IsInEachArray = True
    ArrayCounterLast = LBound(ArrayToSearchIn)
    ArrayCounterAgainLast = LBound(ArrayToFindIn)
    If Complete = True _
    Then
        For ArrayCounter = UBound(ArrayToSearchIn) To LBound(ArrayToSearchIn) Step -1
            For ArrayCounterAgain = UBound(ArrayToFindIn) To LBound(ArrayToFindIn) Step -1
                    Select Case SubsetToFind
                        Case Is = 0, Is = "LeftIncludesRight"
                            If ArrayCounter <> ArrayCounterLast Or ArrayCounter = 0 _
                            Then
                                IsInEachArray = IsInEachArrayLevelComplete(ArrayToSearchIn(ArrayCounter), ArrayToFindIn, CompareType, Direction, ArrayDeleteCounter, _
                                ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, LengthLimit)
                                If IsInEachArray = True _
                                Then
                                    For ArrayCounterNext = ArrayDeleteCounter To UBound(ArrayToFindIn) - 1
                                        ArrayToFindIn(ArrayCounterNext) = ArrayToFindIn(ArrayCounterNext + 1)
                                    Next
                                    If UBound(ArrayToFindIn) = 0 _
                                    Then Exit Function
                                    ReDim Preserve ArrayToFindIn(UBound(ArrayToFindIn) - 1)
                                    Exit For
                                Else
                                    Exit Function
                                End If
                            End If
                        Case Is = 1, Is = "RightIncludesLeft"
                            If ArrayCounterAgain <> ArrayCounterAgainLast Or ArrayCounterAgain = 0 _
                            Then
                                IsInEachArray = IsInEachArrayLevelComplete(ArrayToFindIn(ArrayCounterAgain), ArrayToSearchIn, CompareType, Direction, ArrayDeleteCounter, _
                                ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, LengthLimit)
                                If IsInEachArray = True _
                                Then
                                    For ArrayCounterNext = ArrayDeleteCounter To UBound(ArrayToSearchIn) - 1
                                        ArrayToSearchIn(ArrayCounterNext) = ArrayToSearchIn(ArrayCounterNext + 1)
                                    Next
                                    If UBound(ArrayToSearchIn) = 0 _
                                    Then Exit Function
                                    ReDim Preserve ArrayToSearchIn(UBound(ArrayToSearchIn) - 1)
                                    Exit For
                                Else
                                    Exit Function
                                End If
                            End If
                        Case Is = 2, Is = "LeftEqualsRight"
                            If ArrayCounter <> ArrayCounterLast Or ArrayCounter = 0 _
                            Then
                                IsInEachArray = IsInEachArrayLevelComplete(ArrayToSearchIn(ArrayCounter), ArrayToFindIn, CompareType, Direction, ArrayDeleteCounter, _
                                ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, LengthLimit)
                                If IsInEachArray = True _
                                Then
                                    If UBound(ArrayToSearchIn) = 0 _
                                    Then
                                        If UBound(ArrayToFindIn) = 0 _
                                        Then Exit Function
                                    End If
                                    If UBound(ArrayToFindIn) > 0 _
                                    Then
                                        For ArrayCounterNext = ArrayDeleteCounter To UBound(ArrayToFindIn) - 1
                                            ArrayToFindIn(ArrayCounterNext) = ArrayToFindIn(ArrayCounterNext + 1)
                                        Next
                                        ReDim Preserve ArrayToFindIn(UBound(ArrayToFindIn) - 1)
                                    End If
                                End If
                            End If
                        Case Is = 3, Is = "LeftFoundInRight"
                            IsInEachArray = IsInEachArrayLevelComplete(ArrayToSearchIn(ArrayCounter), ArrayToFindIn, CompareType, Direction, ArrayDeleteCounter, _
                            ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, LengthLimit)
                            If IsInEachArray = False _
                            Then
                                Exit Function
                            End If
                        Case Is = 4, Is = "RightFoundInLeft"
                            IsInEachArray = IsInEachArrayLevelComplete(ArrayToFindIn(ArrayCounterAgain), ArrayToSearchIn, CompareType, Direction, ArrayDeleteCounter, _
                            ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, LengthLimit)
                            If IsInEachArray = False _
                            Then
                                Exit Function
                            End If
                        End Select
                        ArrayCounterAgainLast = ArrayCounterAgain
                    Next
                    ArrayCounterLast = ArrayCounter
                Next
            Else
                For ArrayCounter = LBound(ArrayToSearchIn) To UBound(ArrayToSearchIn)
                    For ArrayCounterAgain = LBound(ArrayToFindIn) To UBound(ArrayToFindIn)
                        IsInEachArray = IsInEachArrayLevel(ArrayToSearchIn(ArrayCounter), ArrayToFindIn(ArrayCounterAgain), CompareType, Direction, _
                        ArrayCounter, ArrayCounterAgain, SearchArrayAmbiguous, FindArrayAmbiguous, ArrayNotToFindIn, Spread, LengthLimit)
                        If IsInEachArray = True _
                        Then
                            If IsMissing(ArrayNotToFindIn) = False _
                            Then
                                For ArrayCounterNext = LBound(ArrayNotToFindIn) To UBound(ArrayNotToFindIn)
                                    If CompareType = "Left" _
                                    Then
                                        If Left(ArrayToSearchIn(ArrayCounter), 1) = Left(ArrayNotToFindIn(ArrayCounterAgain), 1) _
                                        Then
                                            IsInEachArray = False
                                            Exit For
                                        End If
                                    ElseIf CompareType = "Exact" _
                                    Then
                                        If ArrayToSearchIn(ArrayCounter) = ArrayNotToFindIn(ArrayCounterAgain) _
                                        Then
                                            IsInEachArray = False
                                            Exit For
                                        End If
                                    End If
                                    If ArrayCounterNext = UBound(ArrayNotToFindIn) _
                                    Then Exit Function
                                Next
                            Else
                                Exit Function
                            End If
                        End If
                    Next
                Next
            End If
    
    End Function
    
Function IsInEachArrayLevel(ByVal ValueToSearchFor As Variant, ByRef ValueToFind As Variant, _
ByVal CompareType As Variant, ByVal Direction As Variant, _
ByVal ArrayCounter As Integer, ByVal ArrayCounterAgain As Integer, _
Optional ByRef SearchArrayAmbiguous As Variant, Optional ByRef FindArrayAmbiguous As Variant, _
Optional ByRef ArrayNotToFindIn As Variant, Optional ByVal Spread As Integer, Optional ByVal LengthLimit As Integer) As Boolean

    If IsMissing(LengthLimit) = False _
    Then
        If Len(ValueToSearchFor) < LengthLimit Or Len(ValueToFind) < LengthLimit _
        Then Exit Function
    End If
    If IsMissing(SearchArrayAmbiguous) = False _
    Then
        If SearchArrayAmbiguous(ArrayCounter) = True _
        Then Exit Function
    End If
    If IsMissing(FindArrayAmbiguous) = False _
    Then
        If FindArrayAmbiguous(ArrayCounterAgain) = True _
        Then Exit Function
    End If
    Select Case CompareType
        Case Is = 0, Is = "Exact"
            If IsDate(ValueToSearchFor) = True And IsDate(ValueToFind) = True _
            Then
                If CDate(ValueToSearchFor) = CDate(ValueToFind) _
                Then IsInEachArrayLevel = True
            ElseIf VarType(ValueToSearchFor) = vbBoolean And VarType(ValueToFind) = vbBoolean _
            Then
                If CBool(ValueToSearchFor) = CBool(ValueToFind) _
                Then IsInEachArrayLevel = True
            Else
                If ValueToSearchFor = ValueToFind _
                Then IsInEachArrayLevel = True
            End If
        Case Is = 1, Is = "Left"
            If Left(ValueToSearchFor, 1) = Left(ValueToFind, 1) _
            Then IsInEachArrayLevel = True
        Case Is = 2, Is = "Inside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    Then IsInEachArrayLevel = True
                Case Is = 1, Is = "RightInLeft"
                    If InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    Then IsInEachArrayLevel = True
                Case Is = 2, Is = "Either"
                    If InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    Or InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    Then IsInEachArrayLevel = True
                Case Is = 3, Is = "Each"
                    If InStr(ValueToSearchFor, ValueToFind) <> 0 _
                    And InStr(ValueToFind, ValueToSearchFor) <> 0 _
                    Then IsInEachArrayLevel = True
            End Select
        Case Is = 3, Is = "LeftInside"
            Select Case Direction
                Case Is = 0, Is = "LeftInRight"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    Then IsInEachArrayLevel = True
                Case Is = 1, Is = "RightInLeft"
                    If ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInEachArrayLevel = True
                Case Is = 2, Is = "Either"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    Or ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInEachArrayLevel = True
                Case Is = 3, Is = "Each"
                    If ValueToSearchFor = Left(ValueToFind, Len(ValueToSearchFor)) _
                    And ValueToFind = Left(ValueToSearchFor, Len(ValueToFind)) _
                    Then IsInEachArrayLevel = True
            End Select
    End Select

End Function

Function IsInEachArrayLevelComplete(ByVal ValueToSearchFor As Variant, ByRef ArrayToFindIn As Variant, _
ByVal CompareType As Variant, ByVal Direction As Variant, _
ByVal ArrayDeleteCounter As Integer, ByVal ArrayCounter As Integer, ByVal ArrayCounterAgain As Integer, _
Optional ByRef SearchArrayAmbiguous As Variant, Optional ByRef FindArrayAmbiguous As Variant, _
Optional ByRef ArrayNotToFindIn As Variant, Optional ByVal LengthLimit As Integer) As Boolean

    Dim BestMatch As Integer

    BestMatch = 0
    If LengthLimit <> 0 _
    Then
        If Len(ValueToSearchFor) < LengthLimit Or Len(ArrayToFindIn(ArrayCounterAgain)) < LengthLimit _
        Then Exit Function
    End If
    If IsMissing(SearchArrayAmbiguous) = False _
    Then
        If SearchArrayAmbiguous(ArrayCounter) = True _
        Then Exit Function
    End If
    If IsMissing(FindArrayAmbiguous) = False _
    Then
        If FindArrayAmbiguous(ArrayCounterAgain) = True _
        Then Exit Function
    End If
    IsInEachArrayLevelComplete = False
    For ArrayDeleteCounter = LBound(ArrayToFindIn) To UBound(ArrayToFindIn)
        Select Case CompareType
            Case Is = 0, Is = "Exact"
                If ValueToSearchFor = ArrayToFindIn(ArrayDeleteCounter) _
                Then IsInEachArrayLevelComplete = True
            Case Is = 1, Is = "Left"
                If Left(ValueToSearchFor, 1) = Left(ArrayToFindIn(ArrayDeleteCounter), 1) _
                Then IsInEachArrayLevelComplete = True
            Case Is = 2, Is = "Inside"
                Select Case Direction
                    Case Is = 0, Is = "LeftInRight"
                        If InStr(ArrayToFindIn(ArrayDeleteCounter), ValueToSearchFor) <> 0 _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 1, Is = "RightInLeft"
                        If InStr(ValueToSearchFor, ArrayToFindIn(ArrayDeleteCounter)) <> 0 _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 2, Is = "Either"
                        If InStr(ArrayToFindIn(ArrayDeleteCounter), ValueToSearchFor) <> 0 _
                        Or InStr(ValueToSearchFor, ArrayToFindIn(ArrayDeleteCounter)) <> 0 _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 3, Is = "Each"
                        If InStr(ArrayToFindIn(ArrayDeleteCounter), ValueToSearchFor) <> 0 _
                        And InStr(ValueToSearchFor, ArrayToFindIn(ArrayDeleteCounter)) <> 0 _
                        Then IsInEachArrayLevelComplete = True
                End Select
            Case Is = 3, Is = "LeftInside"
                Select Case Direction
                    Case Is = 0, Is = "LeftInRight"
                        If ValueToSearchFor = Left(ArrayToFindIn(ArrayDeleteCounter), Len(ValueToSearchFor)) _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 1, Is = "RightInLeft"
                        If ArrayToFindIn(ArrayDeleteCounter) = Left(ValueToSearchFor, Len(ArrayToFindIn(ArrayDeleteCounter))) _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 2, Is = "Either"
                        If ValueToSearchFor = Left(ArrayToFindIn(ArrayDeleteCounter), Len(ValueToSearchFor)) _
                        Or ArrayToFindIn(ArrayDeleteCounter) = Left(ValueToSearchFor, Len(ArrayToFindIn(ArrayDeleteCounter))) _
                        Then IsInEachArrayLevelComplete = True
                    Case Is = 3, Is = "Each"
                        If ValueToSearchFor = Left(ArrayToFindIn(ArrayDeleteCounter), Len(ValueToSearchFor)) _
                        And ArrayToFindIn(ArrayDeleteCounter) = Left(ValueToSearchFor, Len(ArrayToFindIn(ArrayDeleteCounter))) _
                        Then IsInEachArrayLevelComplete = True
                End Select
'                If Len(ArrayToFindIn(ArrayDeleteCounter)) < Len(ArrayToFindIn(BestMatch)) _
'                Then BestMatch = ArrayDeleteCounter
'                If IsInEachArrayLevelComplete = True _
'                Then
'                    If BestMatch <> 0 _
'                    Then
'                        If Len(ArrayToFindIn(ArrayDeleteCounter)) < Len(ArrayToFindIn(BestMatch)) _
'                        Then BestMatch = ArrayDeleteCounter
'                    Else
'                        BestMatch = ArrayDeleteCounter
'                    End If
'                End If
        End Select
        If IsInEachArrayLevelComplete = True _
        Then Exit Function
    Next
'    ArrayDeleteCounter = BestMatch

End Function
