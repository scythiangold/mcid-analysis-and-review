Attribute VB_Name = "CopyFNReview"
Option Explicit

Sub CopyFNReview()

    Dim RowCounter As Integer
    Const Row = 2
    
    RowCounter = Row
    Do Until Cells(RowCounter, 1).Value2 = vbNullString
        If Cells(RowCounter, 3).Value2 <> vbNullString _
        And Cells(RowCounter, 4).Value2 <> vbNullString _
        Then
            Cells(RowCounter, 1).Value2 = Cells(RowCounter, 3).Value2
            Cells(RowCounter, 2).Value2 = Cells(RowCounter, 4).Value2
        End If
        RowCounter = RowCounter + 1
    Loop

End Sub
