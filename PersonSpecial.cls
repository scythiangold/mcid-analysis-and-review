Option ClassModule
Private pSSN() As Variant
Private pAmbiguousSSN() As Variant
Private pSSNCount() As Variant

Public Property Get SSN() As Variant

    SSN = pSSN
    
End Property

Public Property Let SSN(Value2 As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pSSN) <> 0 Then
    
        upperLimit = UBound(pSSN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pSSN(upperLimit) As Variant
    pSSN(upperLimit) = Value2
    
End Property

Public Property Get AmbiguousSSN() As Variant

    AmbiguousSSN = pAmbiguousSSN
    
End Property

Public Property Let AmbiguousSSN(Value2 As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pAmbiguousSSN) <> 0 Then
    
        upperLimit = UBound(pAmbiguousSSN) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pAmbiguousSSN(upperLimit) As Variant
    pAmbiguousSSN(upperLimit) = Value2
    
End Property

Public Property Get SSNCount() As Variant

    SSNCount = pSSNCount
    
End Property

Public Property Let SSNCount(Value2 As Variant)

    Dim upperLimit As Long
    Dim arrExists As Boolean
    Dim Element As Variant
    
    If (Not Not pSSNCount) <> 0 Then
    
        upperLimit = UBound(pSSNCount) + 1
        
    Else
    
        upperLimit = 0
        
    End If
    
    ReDim Preserve pSSNCount(upperLimit) As Variant
    pSSNCount(upperLimit) = Value2
    
End Property

Public Sub IncreaseSSNCount(SSNCounter As Integer)

    pSSNCount(SSNCounter) = pSSNCount(SSNCounter) + 1
    
End Sub

Public Function CheckArrayInitialized() As Boolean

    CheckArrayInitialized = False
    If Not Not pSSN _
    Then CheckArrayInitialized = True

End Function
