Option Explicit

Sub CreateExpiredMCIDQuery()
    
    Dim MCIDCol As Integer
    Dim RowCounter As Integer, ColCounter As Integer
    Dim FilePath As String
    Dim MCIDs() As String
    Dim QueryText As String
    Dim ExpiredMCIDQuery As New StringBuilder
    Const InitialRow = 2
    
    FilePath = Workbooks("MCID Analysis and Review.xlsm").Sheets("Sheet1").Range("B3").Value2
    RowCounter = InitialRow
    ReDim MCIDs(0 To 0)
    
    For ColCounter = 1 To 78
        If Cells(1, ColCounter).Value2 = "MCID" Then MCIDCol = ColCounter
    Next ColCounter
    
    Do While Cells(RowCounter, MCIDCol).Value2 <> vbNullString
        If Cells(RowCounter, MCIDCol).Value2 <> Cells(RowCounter - 1, MCIDCol).Value2 _
        Then
            If MCIDs(UBound(MCIDs)) <> vbNullString _
            Then ReDim Preserve MCIDs(LBound(MCIDs) To UBound(MCIDs) + 1)
            MCIDs(UBound(MCIDs)) = Cells(RowCounter, MCIDCol).Value2
        End If
        RowCounter = RowCounter + 1
    Loop

    ExpiredMCIDQuery.Append ("SELECT DISTINCT curentrecno FROM mbr_mdm116.mpi_entlink_cm WHERE curentrecno IN (")
    For Counter = LBound(MCIDs) To UBound(MCIDs)
        ExpiredMCIDQuery.Append (MCIDs(Counter))
        If Counter < UBound(MCIDs) _
        Then ExpiredMCIDQuery.Append (", ")
    Next Counter
    ExpiredMCIDQuery.Append (")")
    QueryText = ExpiredMCIDQuery.ToString
    
    Debug.Print QueryText
    
    Open FilePath & "Expired MCID Query.txt" For Output As #3
    Print #3, QueryText
    Close #3

End Sub
